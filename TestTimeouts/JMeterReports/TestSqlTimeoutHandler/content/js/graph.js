/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 2.0, "minX": 0.0, "maxY": 7205.0, "series": [{"data": [[0.0, 2.0], [0.1, 29.0], [0.2, 38.0], [0.3, 44.0], [0.4, 49.0], [0.5, 52.0], [0.6, 55.0], [0.7, 57.0], [0.8, 59.0], [0.9, 60.0], [1.0, 62.0], [1.1, 63.0], [1.2, 64.0], [1.3, 65.0], [1.4, 65.0], [1.5, 66.0], [1.6, 67.0], [1.7, 68.0], [1.8, 69.0], [1.9, 69.0], [2.0, 70.0], [2.1, 71.0], [2.2, 71.0], [2.3, 72.0], [2.4, 73.0], [2.5, 73.0], [2.6, 74.0], [2.7, 74.0], [2.8, 75.0], [2.9, 76.0], [3.0, 76.0], [3.1, 77.0], [3.2, 77.0], [3.3, 78.0], [3.4, 78.0], [3.5, 79.0], [3.6, 79.0], [3.7, 80.0], [3.8, 80.0], [3.9, 80.0], [4.0, 81.0], [4.1, 81.0], [4.2, 82.0], [4.3, 82.0], [4.4, 83.0], [4.5, 83.0], [4.6, 83.0], [4.7, 84.0], [4.8, 84.0], [4.9, 85.0], [5.0, 85.0], [5.1, 85.0], [5.2, 86.0], [5.3, 86.0], [5.4, 86.0], [5.5, 87.0], [5.6, 87.0], [5.7, 87.0], [5.8, 87.0], [5.9, 88.0], [6.0, 88.0], [6.1, 88.0], [6.2, 89.0], [6.3, 89.0], [6.4, 89.0], [6.5, 89.0], [6.6, 90.0], [6.7, 90.0], [6.8, 90.0], [6.9, 90.0], [7.0, 91.0], [7.1, 91.0], [7.2, 91.0], [7.3, 91.0], [7.4, 91.0], [7.5, 92.0], [7.6, 92.0], [7.7, 92.0], [7.8, 92.0], [7.9, 92.0], [8.0, 93.0], [8.1, 93.0], [8.2, 93.0], [8.3, 93.0], [8.4, 93.0], [8.5, 94.0], [8.6, 94.0], [8.7, 94.0], [8.8, 94.0], [8.9, 94.0], [9.0, 94.0], [9.1, 95.0], [9.2, 95.0], [9.3, 95.0], [9.4, 95.0], [9.5, 95.0], [9.6, 95.0], [9.7, 96.0], [9.8, 96.0], [9.9, 96.0], [10.0, 96.0], [10.1, 96.0], [10.2, 96.0], [10.3, 96.0], [10.4, 97.0], [10.5, 97.0], [10.6, 97.0], [10.7, 97.0], [10.8, 97.0], [10.9, 97.0], [11.0, 98.0], [11.1, 98.0], [11.2, 98.0], [11.3, 98.0], [11.4, 98.0], [11.5, 98.0], [11.6, 98.0], [11.7, 99.0], [11.8, 99.0], [11.9, 99.0], [12.0, 99.0], [12.1, 99.0], [12.2, 99.0], [12.3, 100.0], [12.4, 100.0], [12.5, 100.0], [12.6, 100.0], [12.7, 100.0], [12.8, 100.0], [12.9, 100.0], [13.0, 101.0], [13.1, 101.0], [13.2, 101.0], [13.3, 101.0], [13.4, 101.0], [13.5, 101.0], [13.6, 102.0], [13.7, 102.0], [13.8, 102.0], [13.9, 102.0], [14.0, 102.0], [14.1, 102.0], [14.2, 102.0], [14.3, 103.0], [14.4, 103.0], [14.5, 103.0], [14.6, 103.0], [14.7, 103.0], [14.8, 103.0], [14.9, 104.0], [15.0, 104.0], [15.1, 104.0], [15.2, 104.0], [15.3, 104.0], [15.4, 104.0], [15.5, 104.0], [15.6, 105.0], [15.7, 105.0], [15.8, 105.0], [15.9, 105.0], [16.0, 105.0], [16.1, 105.0], [16.2, 105.0], [16.3, 106.0], [16.4, 106.0], [16.5, 106.0], [16.6, 106.0], [16.7, 106.0], [16.8, 106.0], [16.9, 107.0], [17.0, 107.0], [17.1, 107.0], [17.2, 107.0], [17.3, 107.0], [17.4, 107.0], [17.5, 107.0], [17.6, 108.0], [17.7, 108.0], [17.8, 108.0], [17.9, 108.0], [18.0, 108.0], [18.1, 108.0], [18.2, 108.0], [18.3, 109.0], [18.4, 109.0], [18.5, 109.0], [18.6, 109.0], [18.7, 109.0], [18.8, 109.0], [18.9, 109.0], [19.0, 109.0], [19.1, 110.0], [19.2, 110.0], [19.3, 110.0], [19.4, 110.0], [19.5, 110.0], [19.6, 110.0], [19.7, 110.0], [19.8, 110.0], [19.9, 111.0], [20.0, 111.0], [20.1, 111.0], [20.2, 111.0], [20.3, 111.0], [20.4, 111.0], [20.5, 111.0], [20.6, 112.0], [20.7, 112.0], [20.8, 112.0], [20.9, 112.0], [21.0, 112.0], [21.1, 112.0], [21.2, 112.0], [21.3, 112.0], [21.4, 113.0], [21.5, 113.0], [21.6, 113.0], [21.7, 113.0], [21.8, 113.0], [21.9, 113.0], [22.0, 113.0], [22.1, 114.0], [22.2, 114.0], [22.3, 114.0], [22.4, 114.0], [22.5, 114.0], [22.6, 114.0], [22.7, 114.0], [22.8, 114.0], [22.9, 115.0], [23.0, 115.0], [23.1, 115.0], [23.2, 115.0], [23.3, 115.0], [23.4, 115.0], [23.5, 115.0], [23.6, 115.0], [23.7, 116.0], [23.8, 116.0], [23.9, 116.0], [24.0, 116.0], [24.1, 116.0], [24.2, 116.0], [24.3, 116.0], [24.4, 116.0], [24.5, 117.0], [24.6, 117.0], [24.7, 117.0], [24.8, 117.0], [24.9, 117.0], [25.0, 117.0], [25.1, 117.0], [25.2, 117.0], [25.3, 118.0], [25.4, 118.0], [25.5, 118.0], [25.6, 118.0], [25.7, 118.0], [25.8, 118.0], [25.9, 118.0], [26.0, 118.0], [26.1, 119.0], [26.2, 119.0], [26.3, 119.0], [26.4, 119.0], [26.5, 119.0], [26.6, 119.0], [26.7, 119.0], [26.8, 119.0], [26.9, 119.0], [27.0, 120.0], [27.1, 120.0], [27.2, 120.0], [27.3, 120.0], [27.4, 120.0], [27.5, 120.0], [27.6, 120.0], [27.7, 120.0], [27.8, 120.0], [27.9, 121.0], [28.0, 121.0], [28.1, 121.0], [28.2, 121.0], [28.3, 121.0], [28.4, 121.0], [28.5, 121.0], [28.6, 121.0], [28.7, 121.0], [28.8, 121.0], [28.9, 122.0], [29.0, 122.0], [29.1, 122.0], [29.2, 122.0], [29.3, 122.0], [29.4, 122.0], [29.5, 122.0], [29.6, 122.0], [29.7, 122.0], [29.8, 122.0], [29.9, 123.0], [30.0, 123.0], [30.1, 123.0], [30.2, 123.0], [30.3, 123.0], [30.4, 123.0], [30.5, 123.0], [30.6, 123.0], [30.7, 123.0], [30.8, 123.0], [30.9, 123.0], [31.0, 124.0], [31.1, 124.0], [31.2, 124.0], [31.3, 124.0], [31.4, 124.0], [31.5, 124.0], [31.6, 124.0], [31.7, 124.0], [31.8, 124.0], [31.9, 124.0], [32.0, 124.0], [32.1, 125.0], [32.2, 125.0], [32.3, 125.0], [32.4, 125.0], [32.5, 125.0], [32.6, 125.0], [32.7, 125.0], [32.8, 125.0], [32.9, 125.0], [33.0, 125.0], [33.1, 125.0], [33.2, 126.0], [33.3, 126.0], [33.4, 126.0], [33.5, 126.0], [33.6, 126.0], [33.7, 126.0], [33.8, 126.0], [33.9, 126.0], [34.0, 126.0], [34.1, 126.0], [34.2, 126.0], [34.3, 127.0], [34.4, 127.0], [34.5, 127.0], [34.6, 127.0], [34.7, 127.0], [34.8, 127.0], [34.9, 127.0], [35.0, 127.0], [35.1, 127.0], [35.2, 127.0], [35.3, 127.0], [35.4, 128.0], [35.5, 128.0], [35.6, 128.0], [35.7, 128.0], [35.8, 128.0], [35.9, 128.0], [36.0, 128.0], [36.1, 128.0], [36.2, 128.0], [36.3, 128.0], [36.4, 129.0], [36.5, 129.0], [36.6, 129.0], [36.7, 129.0], [36.8, 129.0], [36.9, 129.0], [37.0, 129.0], [37.1, 129.0], [37.2, 129.0], [37.3, 129.0], [37.4, 130.0], [37.5, 130.0], [37.6, 130.0], [37.7, 130.0], [37.8, 130.0], [37.9, 130.0], [38.0, 130.0], [38.1, 130.0], [38.2, 130.0], [38.3, 130.0], [38.4, 131.0], [38.5, 131.0], [38.6, 131.0], [38.7, 131.0], [38.8, 131.0], [38.9, 131.0], [39.0, 131.0], [39.1, 131.0], [39.2, 131.0], [39.3, 132.0], [39.4, 132.0], [39.5, 132.0], [39.6, 132.0], [39.7, 132.0], [39.8, 132.0], [39.9, 132.0], [40.0, 132.0], [40.1, 132.0], [40.2, 133.0], [40.3, 133.0], [40.4, 133.0], [40.5, 133.0], [40.6, 133.0], [40.7, 133.0], [40.8, 133.0], [40.9, 133.0], [41.0, 133.0], [41.1, 134.0], [41.2, 134.0], [41.3, 134.0], [41.4, 134.0], [41.5, 134.0], [41.6, 134.0], [41.7, 134.0], [41.8, 134.0], [41.9, 134.0], [42.0, 135.0], [42.1, 135.0], [42.2, 135.0], [42.3, 135.0], [42.4, 135.0], [42.5, 135.0], [42.6, 135.0], [42.7, 135.0], [42.8, 136.0], [42.9, 136.0], [43.0, 136.0], [43.1, 136.0], [43.2, 136.0], [43.3, 136.0], [43.4, 136.0], [43.5, 136.0], [43.6, 136.0], [43.7, 137.0], [43.8, 137.0], [43.9, 137.0], [44.0, 137.0], [44.1, 137.0], [44.2, 137.0], [44.3, 137.0], [44.4, 137.0], [44.5, 138.0], [44.6, 138.0], [44.7, 138.0], [44.8, 138.0], [44.9, 138.0], [45.0, 138.0], [45.1, 138.0], [45.2, 138.0], [45.3, 139.0], [45.4, 139.0], [45.5, 139.0], [45.6, 139.0], [45.7, 139.0], [45.8, 139.0], [45.9, 139.0], [46.0, 139.0], [46.1, 139.0], [46.2, 140.0], [46.3, 140.0], [46.4, 140.0], [46.5, 140.0], [46.6, 140.0], [46.7, 140.0], [46.8, 140.0], [46.9, 140.0], [47.0, 141.0], [47.1, 141.0], [47.2, 141.0], [47.3, 141.0], [47.4, 141.0], [47.5, 141.0], [47.6, 141.0], [47.7, 141.0], [47.8, 142.0], [47.9, 142.0], [48.0, 142.0], [48.1, 142.0], [48.2, 142.0], [48.3, 142.0], [48.4, 142.0], [48.5, 142.0], [48.6, 143.0], [48.7, 143.0], [48.8, 143.0], [48.9, 143.0], [49.0, 143.0], [49.1, 143.0], [49.2, 143.0], [49.3, 143.0], [49.4, 144.0], [49.5, 144.0], [49.6, 144.0], [49.7, 144.0], [49.8, 144.0], [49.9, 144.0], [50.0, 144.0], [50.1, 144.0], [50.2, 145.0], [50.3, 145.0], [50.4, 145.0], [50.5, 145.0], [50.6, 145.0], [50.7, 145.0], [50.8, 145.0], [50.9, 145.0], [51.0, 146.0], [51.1, 146.0], [51.2, 146.0], [51.3, 146.0], [51.4, 146.0], [51.5, 146.0], [51.6, 146.0], [51.7, 146.0], [51.8, 147.0], [51.9, 147.0], [52.0, 147.0], [52.1, 147.0], [52.2, 147.0], [52.3, 147.0], [52.4, 147.0], [52.5, 148.0], [52.6, 148.0], [52.7, 148.0], [52.8, 148.0], [52.9, 148.0], [53.0, 148.0], [53.1, 148.0], [53.2, 148.0], [53.3, 149.0], [53.4, 149.0], [53.5, 149.0], [53.6, 149.0], [53.7, 149.0], [53.8, 149.0], [53.9, 149.0], [54.0, 150.0], [54.1, 150.0], [54.2, 150.0], [54.3, 150.0], [54.4, 150.0], [54.5, 150.0], [54.6, 150.0], [54.7, 151.0], [54.8, 151.0], [54.9, 151.0], [55.0, 151.0], [55.1, 151.0], [55.2, 151.0], [55.3, 151.0], [55.4, 151.0], [55.5, 152.0], [55.6, 152.0], [55.7, 152.0], [55.8, 152.0], [55.9, 152.0], [56.0, 152.0], [56.1, 152.0], [56.2, 153.0], [56.3, 153.0], [56.4, 153.0], [56.5, 153.0], [56.6, 153.0], [56.7, 153.0], [56.8, 153.0], [56.9, 153.0], [57.0, 154.0], [57.1, 154.0], [57.2, 154.0], [57.3, 154.0], [57.4, 154.0], [57.5, 154.0], [57.6, 154.0], [57.7, 155.0], [57.8, 155.0], [57.9, 155.0], [58.0, 155.0], [58.1, 155.0], [58.2, 155.0], [58.3, 155.0], [58.4, 155.0], [58.5, 156.0], [58.6, 156.0], [58.7, 156.0], [58.8, 156.0], [58.9, 156.0], [59.0, 156.0], [59.1, 156.0], [59.2, 156.0], [59.3, 157.0], [59.4, 157.0], [59.5, 157.0], [59.6, 157.0], [59.7, 157.0], [59.8, 157.0], [59.9, 157.0], [60.0, 158.0], [60.1, 158.0], [60.2, 158.0], [60.3, 158.0], [60.4, 158.0], [60.5, 158.0], [60.6, 158.0], [60.7, 159.0], [60.8, 159.0], [60.9, 159.0], [61.0, 159.0], [61.1, 159.0], [61.2, 159.0], [61.3, 159.0], [61.4, 160.0], [61.5, 160.0], [61.6, 160.0], [61.7, 160.0], [61.8, 160.0], [61.9, 160.0], [62.0, 160.0], [62.1, 161.0], [62.2, 161.0], [62.3, 161.0], [62.4, 161.0], [62.5, 161.0], [62.6, 161.0], [62.7, 162.0], [62.8, 162.0], [62.9, 162.0], [63.0, 162.0], [63.1, 162.0], [63.2, 162.0], [63.3, 163.0], [63.4, 163.0], [63.5, 163.0], [63.6, 163.0], [63.7, 163.0], [63.8, 163.0], [63.9, 164.0], [64.0, 164.0], [64.1, 164.0], [64.2, 164.0], [64.3, 164.0], [64.4, 165.0], [64.5, 165.0], [64.6, 165.0], [64.7, 165.0], [64.8, 165.0], [64.9, 165.0], [65.0, 166.0], [65.1, 166.0], [65.2, 166.0], [65.3, 166.0], [65.4, 166.0], [65.5, 167.0], [65.6, 167.0], [65.7, 167.0], [65.8, 167.0], [65.9, 167.0], [66.0, 168.0], [66.1, 168.0], [66.2, 168.0], [66.3, 168.0], [66.4, 168.0], [66.5, 169.0], [66.6, 169.0], [66.7, 169.0], [66.8, 169.0], [66.9, 169.0], [67.0, 170.0], [67.1, 170.0], [67.2, 170.0], [67.3, 170.0], [67.4, 170.0], [67.5, 171.0], [67.6, 171.0], [67.7, 171.0], [67.8, 171.0], [67.9, 171.0], [68.0, 172.0], [68.1, 172.0], [68.2, 172.0], [68.3, 172.0], [68.4, 173.0], [68.5, 173.0], [68.6, 173.0], [68.7, 173.0], [68.8, 173.0], [68.9, 174.0], [69.0, 174.0], [69.1, 174.0], [69.2, 174.0], [69.3, 174.0], [69.4, 175.0], [69.5, 175.0], [69.6, 175.0], [69.7, 175.0], [69.8, 176.0], [69.9, 176.0], [70.0, 176.0], [70.1, 176.0], [70.2, 176.0], [70.3, 177.0], [70.4, 177.0], [70.5, 177.0], [70.6, 177.0], [70.7, 178.0], [70.8, 178.0], [70.9, 178.0], [71.0, 178.0], [71.1, 179.0], [71.2, 179.0], [71.3, 179.0], [71.4, 179.0], [71.5, 179.0], [71.6, 180.0], [71.7, 180.0], [71.8, 180.0], [71.9, 180.0], [72.0, 181.0], [72.1, 181.0], [72.2, 181.0], [72.3, 181.0], [72.4, 182.0], [72.5, 182.0], [72.6, 182.0], [72.7, 182.0], [72.8, 183.0], [72.9, 183.0], [73.0, 183.0], [73.1, 183.0], [73.2, 184.0], [73.3, 184.0], [73.4, 184.0], [73.5, 184.0], [73.6, 185.0], [73.7, 185.0], [73.8, 185.0], [73.9, 185.0], [74.0, 186.0], [74.1, 186.0], [74.2, 186.0], [74.3, 186.0], [74.4, 187.0], [74.5, 187.0], [74.6, 187.0], [74.7, 187.0], [74.8, 188.0], [74.9, 188.0], [75.0, 188.0], [75.1, 189.0], [75.2, 189.0], [75.3, 189.0], [75.4, 189.0], [75.5, 190.0], [75.6, 190.0], [75.7, 190.0], [75.8, 190.0], [75.9, 191.0], [76.0, 191.0], [76.1, 191.0], [76.2, 192.0], [76.3, 192.0], [76.4, 192.0], [76.5, 192.0], [76.6, 193.0], [76.7, 193.0], [76.8, 193.0], [76.9, 194.0], [77.0, 194.0], [77.1, 194.0], [77.2, 195.0], [77.3, 195.0], [77.4, 195.0], [77.5, 196.0], [77.6, 196.0], [77.7, 196.0], [77.8, 197.0], [77.9, 197.0], [78.0, 197.0], [78.1, 198.0], [78.2, 198.0], [78.3, 199.0], [78.4, 199.0], [78.5, 199.0], [78.6, 200.0], [78.7, 200.0], [78.8, 201.0], [78.9, 201.0], [79.0, 201.0], [79.1, 202.0], [79.2, 202.0], [79.3, 203.0], [79.4, 203.0], [79.5, 203.0], [79.6, 204.0], [79.7, 204.0], [79.8, 205.0], [79.9, 205.0], [80.0, 205.0], [80.1, 206.0], [80.2, 206.0], [80.3, 207.0], [80.4, 207.0], [80.5, 208.0], [80.6, 208.0], [80.7, 209.0], [80.8, 209.0], [80.9, 209.0], [81.0, 210.0], [81.1, 210.0], [81.2, 211.0], [81.3, 211.0], [81.4, 212.0], [81.5, 213.0], [81.6, 213.0], [81.7, 214.0], [81.8, 214.0], [81.9, 215.0], [82.0, 215.0], [82.1, 216.0], [82.2, 216.0], [82.3, 217.0], [82.4, 217.0], [82.5, 218.0], [82.6, 218.0], [82.7, 219.0], [82.8, 220.0], [82.9, 220.0], [83.0, 221.0], [83.1, 221.0], [83.2, 222.0], [83.3, 222.0], [83.4, 223.0], [83.5, 224.0], [83.6, 224.0], [83.7, 225.0], [83.8, 226.0], [83.9, 226.0], [84.0, 227.0], [84.1, 228.0], [84.2, 228.0], [84.3, 229.0], [84.4, 230.0], [84.5, 230.0], [84.6, 231.0], [84.7, 232.0], [84.8, 233.0], [84.9, 233.0], [85.0, 234.0], [85.1, 235.0], [85.2, 236.0], [85.3, 236.0], [85.4, 237.0], [85.5, 238.0], [85.6, 239.0], [85.7, 240.0], [85.8, 241.0], [85.9, 242.0], [86.0, 243.0], [86.1, 244.0], [86.2, 245.0], [86.3, 246.0], [86.4, 247.0], [86.5, 248.0], [86.6, 249.0], [86.7, 250.0], [86.8, 251.0], [86.9, 252.0], [87.0, 253.0], [87.1, 254.0], [87.2, 256.0], [87.3, 257.0], [87.4, 258.0], [87.5, 260.0], [87.6, 261.0], [87.7, 262.0], [87.8, 264.0], [87.9, 265.0], [88.0, 267.0], [88.1, 268.0], [88.2, 270.0], [88.3, 272.0], [88.4, 274.0], [88.5, 276.0], [88.6, 278.0], [88.7, 280.0], [88.8, 282.0], [88.9, 284.0], [89.0, 287.0], [89.1, 289.0], [89.2, 292.0], [89.3, 294.0], [89.4, 297.0], [89.5, 300.0], [89.6, 303.0], [89.7, 306.0], [89.8, 310.0], [89.9, 313.0], [90.0, 317.0], [90.1, 321.0], [90.2, 326.0], [90.3, 331.0], [90.4, 336.0], [90.5, 343.0], [90.6, 351.0], [90.7, 360.0], [90.8, 370.0], [90.9, 385.0], [91.0, 409.0], [91.1, 440.0], [91.2, 513.0], [91.3, 714.0], [91.4, 2005.0], [91.5, 2055.0], [91.6, 2106.0], [91.7, 2127.0], [91.8, 2160.0], [91.9, 2206.0], [92.0, 2408.0], [92.1, 2663.0], [92.2, 3039.0], [92.3, 3114.0], [92.4, 4013.0], [92.5, 4091.0], [92.6, 4114.0], [92.7, 4127.0], [92.8, 4140.0], [92.9, 4151.0], [93.0, 4162.0], [93.1, 4171.0], [93.2, 4180.0], [93.3, 4189.0], [93.4, 4198.0], [93.5, 4209.0], [93.6, 4220.0], [93.7, 4229.0], [93.8, 4239.0], [93.9, 4249.0], [94.0, 4257.0], [94.1, 4266.0], [94.2, 4275.0], [94.3, 4284.0], [94.4, 4292.0], [94.5, 4300.0], [94.6, 4309.0], [94.7, 4315.0], [94.8, 4321.0], [94.9, 4327.0], [95.0, 4333.0], [95.1, 4338.0], [95.2, 4344.0], [95.3, 4349.0], [95.4, 4354.0], [95.5, 4359.0], [95.6, 4364.0], [95.7, 4369.0], [95.8, 4373.0], [95.9, 4378.0], [96.0, 4383.0], [96.1, 4389.0], [96.2, 4394.0], [96.3, 4399.0], [96.4, 4404.0], [96.5, 4409.0], [96.6, 4413.0], [96.7, 4418.0], [96.8, 4423.0], [96.9, 4428.0], [97.0, 4433.0], [97.1, 4437.0], [97.2, 4442.0], [97.3, 4447.0], [97.4, 4452.0], [97.5, 4456.0], [97.6, 4461.0], [97.7, 4466.0], [97.8, 4471.0], [97.9, 4477.0], [98.0, 4483.0], [98.1, 4489.0], [98.2, 4496.0], [98.3, 4504.0], [98.4, 4511.0], [98.5, 4519.0], [98.6, 4528.0], [98.7, 4538.0], [98.8, 4547.0], [98.9, 4557.0], [99.0, 4569.0], [99.1, 4584.0], [99.2, 4602.0], [99.3, 4624.0], [99.4, 4649.0], [99.5, 4689.0], [99.6, 4748.0], [99.7, 4812.0], [99.8, 4919.0], [99.9, 5179.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "maxY": 678270.0, "series": [{"data": [[0.0, 125635.0], [600.0, 419.0], [700.0, 360.0], [800.0, 251.0], [900.0, 3.0], [1000.0, 84.0], [1100.0, 277.0], [1900.0, 133.0], [2000.0, 1929.0], [2100.0, 3103.0], [2300.0, 333.0], [2200.0, 827.0], [2400.0, 452.0], [2500.0, 97.0], [2600.0, 800.0], [2700.0, 349.0], [2800.0, 106.0], [2900.0, 83.0], [3000.0, 865.0], [3100.0, 1211.0], [3200.0, 82.0], [3300.0, 3.0], [4000.0, 1341.0], [4300.0, 18544.0], [4200.0, 11028.0], [4100.0, 9088.0], [4600.0, 3401.0], [4500.0, 9641.0], [4400.0, 19882.0], [4700.0, 1708.0], [4800.0, 1021.0], [4900.0, 745.0], [5000.0, 189.0], [5100.0, 249.0], [5200.0, 64.0], [5300.0, 103.0], [5400.0, 110.0], [5500.0, 121.0], [5600.0, 165.0], [5700.0, 20.0], [5800.0, 54.0], [5900.0, 141.0], [6000.0, 61.0], [6100.0, 79.0], [6300.0, 48.0], [6200.0, 6.0], [6500.0, 8.0], [6400.0, 21.0], [7100.0, 1.0], [7200.0, 1.0], [100.0, 678270.0], [200.0, 112054.0], [300.0, 15066.0], [400.0, 2259.0], [500.0, 623.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 7200.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 2004.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 933297.0, "series": [{"data": [[0.0, 933297.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 2004.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 85827.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 2386.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 1000.0, "minX": 1.67637996E12, "maxY": 1000.0, "series": [{"data": [[1.67638008E12, 1000.0], [1.67638026E12, 1000.0], [1.67637996E12, 1000.0], [1.67638044E12, 1000.0], [1.67638014E12, 1000.0], [1.67638032E12, 1000.0], [1.67638002E12, 1000.0], [1.6763802E12, 1000.0], [1.67638038E12, 1000.0]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638044E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 497.41782721095336, "minX": 1000.0, "maxY": 497.41782721095336, "series": [{"data": [[1000.0, 497.41782721095336]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[1000.0, 497.41782721095336]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 1000.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 165586.8, "minX": 1.67637996E12, "maxY": 664009.6, "series": [{"data": [[1.67638008E12, 460153.6], [1.67638026E12, 656417.8], [1.67637996E12, 378487.2], [1.67638044E12, 534381.6], [1.67638014E12, 594624.2], [1.67638032E12, 564507.4], [1.67638002E12, 528148.6], [1.6763802E12, 664009.6], [1.67638038E12, 509247.6]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.67638008E12, 221335.46666666667], [1.67638026E12, 320561.06666666665], [1.67637996E12, 165586.8], [1.67638044E12, 255020.4], [1.67638014E12, 292064.5333333333], [1.67638032E12, 266072.6666666667], [1.67638002E12, 235540.66666666666], [1.6763802E12, 320622.26666666666], [1.67638038E12, 243161.2]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638044E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 417.5004525398792, "minX": 1.67637996E12, "maxY": 605.3546206783618, "series": [{"data": [[1.67638008E12, 605.3546206783618], [1.67638026E12, 417.5004525398792], [1.67637996E12, 525.06425471917], [1.67638044E12, 444.7258441546867], [1.67638014E12, 468.7132601744619], [1.67638032E12, 512.5886356859854], [1.67638002E12, 580.9425395756132], [1.6763802E12, 430.1118196407209], [1.67638038E12, 566.6772001454004]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638044E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 417.50034647584164, "minX": 1.67637996E12, "maxY": 605.3544261019193, "series": [{"data": [[1.67638008E12, 605.3544261019193], [1.67638026E12, 417.50034647584164], [1.67637996E12, 525.0619550189515], [1.67638044E12, 444.7256752793161], [1.67638014E12, 468.71312047930724], [1.67638032E12, 512.5884823444309], [1.67638002E12, 580.9423182408719], [1.6763802E12, 430.11163583149454], [1.67638038E12, 566.6769298171877]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638044E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.002658164311316208, "minX": 1.67637996E12, "maxY": 0.2606326913336901, "series": [{"data": [[1.67638008E12, 0.003584302801900729], [1.67638026E12, 0.0027152392804616074], [1.67637996E12, 0.2606326913336901], [1.67638044E12, 0.0030308686416197525], [1.67638014E12, 0.0030810542327631457], [1.67638032E12, 0.0030497934148314932], [1.67638002E12, 0.0035221094163499166], [1.6763802E12, 0.002658164311316208], [1.67638038E12, 0.0034583368289568452]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638044E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 2.0, "minX": 1.67637996E12, "maxY": 6583.0, "series": [{"data": [[1.67638008E12, 6524.0], [1.67638026E12, 4886.0], [1.67637996E12, 5489.0], [1.67638044E12, 4936.0], [1.67638014E12, 6583.0], [1.67638032E12, 6563.0], [1.67638002E12, 5969.0], [1.6763802E12, 6430.0], [1.67638038E12, 5616.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.67638008E12, 3125.0], [1.67638026E12, 217.0], [1.67637996E12, 300.0], [1.67638044E12, 224.0], [1.67638014E12, 226.0], [1.67638032E12, 2681.0], [1.67638002E12, 4141.0], [1.6763802E12, 249.0], [1.67638038E12, 2075.9000000000015]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.67638008E12, 4521.990000000002], [1.67638026E12, 4518.990000000002], [1.67637996E12, 4481.0], [1.67638044E12, 4616.0], [1.67638014E12, 4436.0], [1.67638032E12, 4485.0], [1.67638002E12, 4598.0], [1.6763802E12, 4458.0], [1.67638038E12, 4596.990000000002]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.67638008E12, 4271.0], [1.67638026E12, 4275.950000000001], [1.67637996E12, 4323.0], [1.67638044E12, 4447.0], [1.67638014E12, 4256.0], [1.67638032E12, 4232.950000000001], [1.67638002E12, 4311.0], [1.6763802E12, 4326.950000000001], [1.67638038E12, 4354.950000000001]], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [[1.67638008E12, 3.0], [1.67638026E12, 11.0], [1.67637996E12, 5.0], [1.67638044E12, 25.0], [1.67638014E12, 6.0], [1.67638032E12, 2.0], [1.67638002E12, 4.0], [1.6763802E12, 26.0], [1.67638038E12, 6.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.67638008E12, 151.0], [1.67638026E12, 130.0], [1.67637996E12, 154.0], [1.67638044E12, 142.0], [1.67638014E12, 135.0], [1.67638032E12, 166.0], [1.67638002E12, 156.0], [1.6763802E12, 130.0], [1.67638038E12, 152.0]], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638044E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 94.0, "minX": 1.0, "maxY": 7116.0, "series": [{"data": [[3.0, 4208.0], [24.0, 4163.5], [27.0, 4174.0], [42.0, 4162.0], [46.0, 4143.0], [50.0, 4152.0], [81.0, 4253.0], [117.0, 4041.0], [122.0, 2414.0], [137.0, 4240.5], [164.0, 4195.0], [176.0, 4173.0], [201.0, 4191.0], [207.0, 2254.0], [216.0, 4165.0], [234.0, 4137.0], [238.0, 4163.0], [244.0, 4154.5], [251.0, 3256.0], [255.0, 4216.0], [249.0, 4041.0], [261.0, 4156.0], [256.0, 4126.0], [293.0, 3104.0], [296.0, 4184.5], [302.0, 4129.0], [307.0, 4218.0], [315.0, 4127.0], [331.0, 4151.0], [338.0, 4171.0], [343.0, 4222.0], [360.0, 4146.0], [364.0, 4173.0], [366.0, 4123.0], [362.0, 3121.0], [372.0, 3121.5], [397.0, 4183.0], [409.0, 4153.0], [418.0, 4207.0], [438.0, 4163.0], [563.0, 4183.0], [553.0, 180.0], [544.0, 220.5], [582.0, 2350.0], [649.0, 159.0], [644.0, 140.0], [699.0, 453.0], [705.0, 2160.0], [748.0, 2008.0], [921.0, 329.0], [915.0, 128.0], [930.0, 149.0], [985.0, 3028.0], [1000.0, 269.0], [1083.0, 338.0], [1060.0, 659.0], [1065.0, 158.0], [1098.0, 326.5], [1089.0, 221.0], [1133.0, 2187.0], [1126.0, 150.0], [1136.0, 2118.0], [1131.0, 143.0], [1146.0, 165.5], [1104.0, 108.0], [1168.0, 152.0], [1188.0, 143.0], [1210.0, 112.0], [1184.0, 151.5], [1305.0, 440.0], [1309.0, 170.0], [1391.0, 157.0], [1366.0, 187.0], [1440.0, 251.0], [1448.0, 173.0], [1464.0, 167.5], [1474.0, 210.0], [1479.0, 143.0], [1483.0, 202.0], [1580.0, 160.0], [1551.0, 170.0], [1585.0, 135.0], [1634.0, 124.0], [1624.0, 157.0], [1717.0, 130.0], [1694.0, 217.0], [1724.0, 209.0], [1686.0, 239.0], [1706.0, 193.0], [1688.0, 181.0], [1698.0, 119.0], [1705.0, 131.0], [1703.0, 194.0], [1701.0, 147.0], [1716.0, 159.5], [1665.0, 208.0], [1750.0, 193.0], [1781.0, 196.0], [1761.0, 151.0], [1780.0, 184.0], [1743.0, 184.0], [1777.0, 160.0], [1802.0, 178.0], [1843.0, 174.0], [1810.0, 132.0], [1808.0, 199.0], [1799.0, 158.0], [1825.0, 185.0], [1834.0, 169.0], [1842.0, 189.0], [1805.0, 189.0], [1869.0, 199.0], [1918.0, 146.0], [1898.0, 134.0], [1883.0, 192.0], [1880.0, 158.0], [1912.0, 177.0], [1873.0, 159.0], [1867.0, 176.0], [1927.0, 173.0], [1941.0, 173.0], [1942.0, 175.0], [1938.0, 180.5], [1953.0, 148.0], [1957.0, 141.0], [1962.0, 140.0], [1977.0, 128.0], [1981.0, 177.0], [1952.0, 166.0], [1970.0, 173.0], [1939.0, 161.0], [1935.0, 141.0], [1933.0, 148.0], [1921.0, 169.0], [1943.0, 175.0], [1992.0, 151.0], [2005.0, 136.0], [2026.0, 146.0], [2021.0, 163.0], [2047.0, 147.0], [2040.0, 147.0], [2037.0, 150.0], [2039.0, 148.0], [2036.0, 148.0], [1996.0, 166.0], [1998.0, 164.0], [1985.0, 177.0], [2011.0, 145.0], [2009.0, 137.0], [2043.0, 153.0], [2007.0, 170.0], [2029.0, 156.0], [2027.0, 143.0], [2172.0, 161.0], [2158.0, 137.5], [2174.0, 140.0], [2170.0, 165.0], [2156.0, 155.0], [2144.0, 146.0], [2070.0, 142.0], [2116.0, 126.0], [2132.0, 160.0], [2124.0, 161.0], [2120.0, 152.0], [2142.0, 156.0], [2138.0, 132.0], [2164.0, 157.0], [2162.0, 159.0], [2088.0, 149.0], [2102.0, 170.0], [2066.0, 164.0], [2054.0, 170.0], [2068.0, 161.0], [2096.0, 166.0], [2092.0, 159.0], [2284.0, 146.0], [2282.0, 149.0], [2280.0, 142.0], [2290.0, 137.0], [2298.0, 145.0], [2302.0, 155.0], [2228.0, 153.0], [2224.0, 153.0], [2226.0, 140.0], [2178.0, 155.0], [2236.0, 148.0], [2244.0, 140.0], [2192.0, 159.0], [2194.0, 144.0], [2198.0, 141.0], [2182.0, 147.0], [2180.0, 130.0], [2278.0, 134.0], [2276.0, 138.0], [2272.0, 142.0], [2266.0, 156.0], [2268.0, 137.0], [2270.0, 135.0], [2264.0, 138.0], [2262.0, 138.0], [2258.0, 143.5], [2252.0, 172.0], [2256.0, 145.0], [2254.0, 135.0], [2214.0, 159.0], [2216.0, 148.0], [2210.0, 132.0], [2402.0, 147.0], [2308.0, 137.0], [2306.0, 134.0], [2310.0, 154.0], [2322.0, 148.0], [2312.0, 133.0], [2332.0, 155.0], [2328.0, 98.0], [2334.0, 152.0], [2354.0, 138.0], [2366.0, 128.0], [2364.0, 149.0], [2360.0, 137.0], [2352.0, 128.0], [2350.0, 151.0], [2340.0, 146.0], [2342.0, 145.0], [2380.0, 150.0], [2428.0, 112.0], [2430.0, 130.0], [2378.0, 150.0], [2376.0, 131.0], [2390.0, 131.0], [2388.0, 117.0], [2382.0, 116.0], [2384.0, 128.0], [2404.0, 161.0], [2412.0, 133.0], [2424.0, 109.0], [2418.0, 128.0], [2344.0, 160.0], [2396.0, 136.0], [2488.0, 161.0], [2474.0, 131.0], [2464.0, 129.0], [2490.0, 145.0], [2484.0, 136.0], [2482.0, 126.5], [2480.0, 129.0], [2476.0, 135.0], [2538.0, 127.0], [2546.0, 128.0], [2444.0, 155.0], [2446.0, 126.0], [2460.0, 140.0], [2454.0, 153.0], [2500.0, 144.0], [2512.0, 128.0], [2504.0, 106.0], [2502.0, 132.0], [2518.0, 136.0], [2516.0, 139.0], [2434.0, 112.0], [2442.0, 138.0], [2432.0, 150.0], [2566.0, 123.0], [2638.0, 128.0], [2634.0, 119.0], [2560.0, 135.0], [2576.0, 122.0], [2620.0, 119.0], [2616.0, 132.0], [2594.0, 131.0], [2608.0, 121.0], [2584.0, 125.0], [2588.0, 150.0], [2656.0, 131.0], [2666.0, 128.0], [2676.0, 117.0], [2664.0, 117.0], [2650.0, 132.0], [2654.0, 129.0], [2580.0, 112.0], [2578.0, 126.0], [2644.0, 116.0], [2690.0, 108.0], [2726.0, 126.0], [2720.0, 128.0], [2724.0, 117.0], [2788.0, 128.0], [2804.0, 124.0], [2698.0, 127.0], [2780.0, 123.0], [2730.0, 126.0], [2756.0, 125.0], [2834.0, 106.0], [2836.0, 116.0], [2828.0, 108.0], [2922.0, 123.0], [2864.0, 115.0], [2884.0, 122.0], [2856.0, 126.0], [3010.0, 112.0], [2956.0, 116.0], [2167.0, 137.0], [2165.0, 194.0], [2173.0, 146.0], [2119.0, 137.0], [2127.0, 154.0], [2171.0, 162.0], [2169.0, 141.0], [2077.0, 144.0], [2139.0, 156.0], [2133.0, 162.0], [2161.0, 130.0], [2163.0, 159.0], [2093.0, 160.0], [2053.0, 168.0], [2065.0, 153.0], [2099.0, 161.0], [2107.0, 165.0], [2097.0, 148.0], [2095.0, 153.0], [2295.0, 122.0], [2293.0, 151.0], [2297.0, 148.0], [2301.0, 132.0], [2223.0, 168.0], [2233.0, 164.0], [2225.0, 133.0], [2235.0, 161.0], [2237.0, 125.0], [2197.0, 139.0], [2199.0, 149.0], [2273.0, 134.0], [2281.0, 159.0], [2201.0, 152.0], [2203.0, 151.0], [2265.0, 159.0], [2263.0, 155.0], [2261.0, 153.0], [2259.0, 148.0], [2249.0, 145.0], [2247.0, 153.0], [2245.0, 152.0], [2253.0, 142.0], [2257.0, 155.0], [2217.0, 156.0], [2209.0, 160.0], [2211.0, 143.0], [2319.0, 145.0], [2315.0, 131.5], [2313.0, 133.0], [2317.0, 127.0], [2333.0, 122.0], [2327.0, 147.0], [2367.0, 138.0], [2361.0, 132.0], [2349.0, 154.0], [2347.0, 131.0], [2345.0, 145.0], [2351.0, 125.0], [2353.0, 114.0], [2425.0, 140.0], [2339.0, 128.0], [2337.0, 152.0], [2343.0, 144.0], [2375.0, 151.0], [2431.0, 136.0], [2379.0, 156.0], [2377.0, 129.0], [2427.0, 132.0], [2395.0, 123.0], [2389.0, 156.0], [2403.0, 141.0], [2413.0, 94.0], [2421.0, 132.0], [2397.0, 146.0], [2493.0, 139.0], [2443.0, 141.0], [2471.0, 150.0], [2473.0, 124.0], [2475.0, 120.0], [2487.0, 127.0], [2479.0, 130.0], [2457.0, 151.0], [2463.0, 146.0], [2555.0, 146.0], [2551.0, 116.0], [2501.0, 141.0], [2549.0, 146.0], [2521.0, 142.0], [2519.0, 136.0], [2495.0, 118.0], [2513.0, 147.0], [2435.0, 140.0], [2437.0, 135.0], [2433.0, 118.0], [2681.0, 126.0], [2667.0, 124.0], [2625.0, 122.0], [2631.0, 133.0], [2687.0, 121.0], [2577.0, 142.0], [2567.0, 127.0], [2563.0, 135.0], [2609.0, 118.0], [2615.0, 136.0], [2595.0, 137.0], [2603.0, 120.0], [2579.0, 122.0], [2641.0, 141.0], [2649.0, 127.0], [2643.0, 144.0], [2745.0, 132.0], [2721.0, 129.0], [2813.0, 125.0], [2797.0, 121.0], [2715.0, 122.0], [2703.0, 136.0], [2753.0, 109.0], [2691.0, 133.0], [2693.0, 99.0], [2733.0, 119.0], [2727.0, 123.0], [2739.0, 122.0], [2935.0, 112.0], [2881.0, 117.0], [2817.0, 119.0], [1.0, 4131.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[137.0, 4586.0], [164.0, 4571.0], [207.0, 4541.0], [251.0, 5329.0], [256.0, 5407.5], [302.0, 5297.0], [343.0, 4681.0], [438.0, 4531.0], [563.0, 4775.5], [921.0, 5613.0], [1065.0, 4650.0], [1309.0, 4815.5], [1440.0, 5424.0], [1474.0, 7116.0], [1479.0, 4647.5], [1580.0, 4890.0], [1634.0, 5114.5], [1624.0, 4995.0], [1716.0, 4944.5], [1703.0, 5438.5], [1665.0, 5009.0], [1750.0, 5358.0], [1781.0, 4902.0], [1780.0, 5074.0], [1802.0, 4650.0], [1869.0, 5281.0], [1927.0, 4704.5], [1939.0, 4836.0], [1935.0, 4686.0], [1957.0, 4797.0], [1943.0, 4802.0], [2005.0, 5093.0], [2027.0, 4751.0], [2026.0, 4967.0], [1996.0, 4852.0], [2165.0, 4736.0], [2156.0, 4821.0], [2132.0, 4731.0], [2053.0, 4904.0], [2144.0, 4775.5], [2167.0, 4726.0], [2162.0, 4881.0], [2119.0, 4939.0], [2133.0, 4938.5], [2290.0, 4542.0], [2244.0, 5036.0], [2257.0, 4659.0], [2217.0, 5357.0], [2226.0, 4857.0], [2282.0, 4921.0], [2264.0, 5463.0], [2263.0, 4875.0], [2268.0, 4809.5], [2259.0, 5561.0], [2235.0, 5154.0], [2237.0, 4884.0], [2236.0, 4960.0], [2194.0, 4781.0], [2197.0, 4908.0], [2297.0, 4826.0], [2298.0, 4755.0], [2428.0, 4961.0], [2353.0, 4697.0], [2340.0, 4653.0], [2380.0, 4875.0], [2352.0, 4524.0], [2347.0, 4770.0], [2427.0, 4790.0], [2402.0, 4726.5], [2418.0, 4861.0], [2360.0, 4835.0], [2306.0, 4723.0], [2312.0, 4847.5], [2308.0, 5561.0], [2313.0, 4724.5], [2382.0, 4807.5], [2384.0, 4921.0], [2376.0, 4813.5], [2377.0, 4823.0], [2538.0, 4879.0], [2521.0, 4800.0], [2495.0, 4780.0], [2484.0, 4822.0], [2513.0, 4962.0], [2434.0, 4783.0], [2444.0, 4670.0], [2546.0, 4661.0], [2516.0, 4865.5], [2502.0, 4738.5], [2667.0, 4688.0], [2620.0, 4812.5], [2649.0, 4846.5], [2609.0, 4779.0], [2634.0, 4770.5], [2631.0, 4751.0], [2676.0, 4816.0], [2745.0, 4782.0], [2691.0, 5353.0], [2756.0, 4772.5], [2753.0, 5301.5], [2724.0, 5429.5], [2690.0, 4785.0], [2834.0, 4860.0], [2828.0, 4888.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 3010.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 94.0, "minX": 1.0, "maxY": 7116.0, "series": [{"data": [[3.0, 4208.0], [24.0, 4163.5], [27.0, 4174.0], [42.0, 4162.0], [46.0, 4143.0], [50.0, 4152.0], [81.0, 4253.0], [117.0, 4041.0], [122.0, 2414.0], [137.0, 4240.5], [164.0, 4195.0], [176.0, 4173.0], [201.0, 4191.0], [207.0, 2254.0], [216.0, 4165.0], [234.0, 4137.0], [238.0, 4163.0], [244.0, 4154.5], [251.0, 3256.0], [255.0, 4216.0], [249.0, 4041.0], [261.0, 4156.0], [256.0, 4126.0], [293.0, 3104.0], [296.0, 4184.5], [302.0, 4129.0], [307.0, 4218.0], [315.0, 4127.0], [331.0, 4151.0], [338.0, 4171.0], [343.0, 4222.0], [360.0, 4146.0], [364.0, 4173.0], [366.0, 4123.0], [362.0, 3121.0], [372.0, 3121.5], [397.0, 4183.0], [409.0, 4153.0], [418.0, 4207.0], [438.0, 4163.0], [563.0, 4183.0], [553.0, 180.0], [544.0, 220.5], [582.0, 2350.0], [649.0, 159.0], [644.0, 140.0], [699.0, 453.0], [705.0, 2160.0], [748.0, 2008.0], [921.0, 329.0], [915.0, 128.0], [930.0, 149.0], [985.0, 3028.0], [1000.0, 269.0], [1083.0, 338.0], [1060.0, 659.0], [1065.0, 158.0], [1098.0, 326.5], [1089.0, 221.0], [1133.0, 2187.0], [1126.0, 150.0], [1136.0, 2118.0], [1131.0, 143.0], [1146.0, 165.5], [1104.0, 108.0], [1168.0, 152.0], [1188.0, 143.0], [1210.0, 112.0], [1184.0, 151.5], [1305.0, 440.0], [1309.0, 170.0], [1391.0, 157.0], [1366.0, 187.0], [1440.0, 251.0], [1448.0, 173.0], [1464.0, 167.5], [1474.0, 210.0], [1479.0, 143.0], [1483.0, 202.0], [1580.0, 160.0], [1551.0, 170.0], [1585.0, 135.0], [1634.0, 124.0], [1624.0, 157.0], [1717.0, 130.0], [1694.0, 217.0], [1724.0, 209.0], [1686.0, 239.0], [1706.0, 193.0], [1688.0, 181.0], [1698.0, 119.0], [1705.0, 131.0], [1703.0, 194.0], [1701.0, 147.0], [1716.0, 159.5], [1665.0, 208.0], [1750.0, 193.0], [1781.0, 196.0], [1761.0, 151.0], [1780.0, 184.0], [1743.0, 184.0], [1777.0, 160.0], [1802.0, 178.0], [1843.0, 174.0], [1810.0, 132.0], [1808.0, 199.0], [1799.0, 158.0], [1825.0, 185.0], [1834.0, 169.0], [1842.0, 189.0], [1805.0, 189.0], [1869.0, 199.0], [1918.0, 146.0], [1898.0, 134.0], [1883.0, 192.0], [1880.0, 158.0], [1912.0, 177.0], [1873.0, 159.0], [1867.0, 176.0], [1927.0, 173.0], [1941.0, 173.0], [1942.0, 175.0], [1938.0, 180.5], [1953.0, 148.0], [1957.0, 141.0], [1962.0, 140.0], [1977.0, 128.0], [1981.0, 177.0], [1952.0, 166.0], [1970.0, 173.0], [1939.0, 161.0], [1935.0, 141.0], [1933.0, 148.0], [1921.0, 169.0], [1943.0, 175.0], [1992.0, 151.0], [2005.0, 136.0], [2026.0, 146.0], [2021.0, 163.0], [2047.0, 147.0], [2040.0, 147.0], [2037.0, 150.0], [2039.0, 148.0], [2036.0, 148.0], [1996.0, 166.0], [1998.0, 164.0], [1985.0, 177.0], [2011.0, 145.0], [2009.0, 137.0], [2043.0, 153.0], [2007.0, 170.0], [2029.0, 156.0], [2027.0, 143.0], [2172.0, 161.0], [2158.0, 137.5], [2174.0, 140.0], [2170.0, 165.0], [2156.0, 155.0], [2144.0, 146.0], [2070.0, 142.0], [2116.0, 126.0], [2132.0, 160.0], [2124.0, 161.0], [2120.0, 152.0], [2142.0, 156.0], [2138.0, 132.0], [2164.0, 157.0], [2162.0, 159.0], [2088.0, 149.0], [2102.0, 170.0], [2066.0, 164.0], [2054.0, 170.0], [2068.0, 161.0], [2096.0, 166.0], [2092.0, 159.0], [2284.0, 146.0], [2282.0, 149.0], [2280.0, 142.0], [2290.0, 137.0], [2298.0, 145.0], [2302.0, 155.0], [2228.0, 153.0], [2224.0, 153.0], [2226.0, 140.0], [2178.0, 155.0], [2236.0, 148.0], [2244.0, 140.0], [2192.0, 159.0], [2194.0, 144.0], [2198.0, 141.0], [2182.0, 147.0], [2180.0, 130.0], [2278.0, 134.0], [2276.0, 138.0], [2272.0, 142.0], [2266.0, 156.0], [2268.0, 137.0], [2270.0, 135.0], [2264.0, 138.0], [2262.0, 138.0], [2258.0, 143.5], [2252.0, 172.0], [2256.0, 145.0], [2254.0, 135.0], [2214.0, 159.0], [2216.0, 148.0], [2210.0, 132.0], [2402.0, 147.0], [2308.0, 137.0], [2306.0, 134.0], [2310.0, 154.0], [2322.0, 148.0], [2312.0, 133.0], [2332.0, 155.0], [2328.0, 98.0], [2334.0, 152.0], [2354.0, 138.0], [2366.0, 128.0], [2364.0, 149.0], [2360.0, 137.0], [2352.0, 128.0], [2350.0, 151.0], [2340.0, 146.0], [2342.0, 145.0], [2380.0, 150.0], [2428.0, 112.0], [2430.0, 130.0], [2378.0, 150.0], [2376.0, 131.0], [2390.0, 131.0], [2388.0, 117.0], [2382.0, 116.0], [2384.0, 128.0], [2404.0, 161.0], [2412.0, 133.0], [2424.0, 109.0], [2418.0, 128.0], [2344.0, 160.0], [2396.0, 136.0], [2488.0, 161.0], [2474.0, 131.0], [2464.0, 129.0], [2490.0, 145.0], [2484.0, 136.0], [2482.0, 126.5], [2480.0, 129.0], [2476.0, 135.0], [2538.0, 127.0], [2546.0, 128.0], [2444.0, 155.0], [2446.0, 126.0], [2460.0, 140.0], [2454.0, 153.0], [2500.0, 144.0], [2512.0, 128.0], [2504.0, 106.0], [2502.0, 132.0], [2518.0, 136.0], [2516.0, 139.0], [2434.0, 112.0], [2442.0, 138.0], [2432.0, 150.0], [2566.0, 123.0], [2638.0, 128.0], [2634.0, 119.0], [2560.0, 135.0], [2576.0, 122.0], [2620.0, 119.0], [2616.0, 132.0], [2594.0, 131.0], [2608.0, 121.0], [2584.0, 125.0], [2588.0, 150.0], [2656.0, 131.0], [2666.0, 128.0], [2676.0, 117.0], [2664.0, 117.0], [2650.0, 132.0], [2654.0, 129.0], [2580.0, 112.0], [2578.0, 126.0], [2644.0, 116.0], [2690.0, 108.0], [2726.0, 126.0], [2720.0, 128.0], [2724.0, 117.0], [2788.0, 128.0], [2804.0, 124.0], [2698.0, 127.0], [2780.0, 123.0], [2730.0, 126.0], [2756.0, 125.0], [2834.0, 106.0], [2836.0, 116.0], [2828.0, 108.0], [2922.0, 123.0], [2864.0, 115.0], [2884.0, 122.0], [2856.0, 126.0], [3010.0, 112.0], [2956.0, 116.0], [2167.0, 137.0], [2165.0, 194.0], [2173.0, 146.0], [2119.0, 137.0], [2127.0, 154.0], [2171.0, 162.0], [2169.0, 141.0], [2077.0, 144.0], [2139.0, 156.0], [2133.0, 162.0], [2161.0, 130.0], [2163.0, 159.0], [2093.0, 160.0], [2053.0, 168.0], [2065.0, 153.0], [2099.0, 161.0], [2107.0, 165.0], [2097.0, 148.0], [2095.0, 153.0], [2295.0, 122.0], [2293.0, 151.0], [2297.0, 148.0], [2301.0, 132.0], [2223.0, 168.0], [2233.0, 164.0], [2225.0, 133.0], [2235.0, 161.0], [2237.0, 125.0], [2197.0, 139.0], [2199.0, 149.0], [2273.0, 134.0], [2281.0, 159.0], [2201.0, 152.0], [2203.0, 151.0], [2265.0, 159.0], [2263.0, 155.0], [2261.0, 153.0], [2259.0, 148.0], [2249.0, 145.0], [2247.0, 153.0], [2245.0, 152.0], [2253.0, 142.0], [2257.0, 155.0], [2217.0, 156.0], [2209.0, 160.0], [2211.0, 143.0], [2319.0, 145.0], [2315.0, 131.5], [2313.0, 133.0], [2317.0, 127.0], [2333.0, 122.0], [2327.0, 147.0], [2367.0, 138.0], [2361.0, 132.0], [2349.0, 154.0], [2347.0, 131.0], [2345.0, 145.0], [2351.0, 125.0], [2353.0, 114.0], [2425.0, 140.0], [2339.0, 128.0], [2337.0, 152.0], [2343.0, 144.0], [2375.0, 151.0], [2431.0, 136.0], [2379.0, 156.0], [2377.0, 129.0], [2427.0, 132.0], [2395.0, 123.0], [2389.0, 156.0], [2403.0, 141.0], [2413.0, 94.0], [2421.0, 132.0], [2397.0, 146.0], [2493.0, 139.0], [2443.0, 141.0], [2471.0, 150.0], [2473.0, 124.0], [2475.0, 120.0], [2487.0, 127.0], [2479.0, 130.0], [2457.0, 151.0], [2463.0, 146.0], [2555.0, 146.0], [2551.0, 116.0], [2501.0, 141.0], [2549.0, 146.0], [2521.0, 142.0], [2519.0, 136.0], [2495.0, 118.0], [2513.0, 147.0], [2435.0, 140.0], [2437.0, 135.0], [2433.0, 118.0], [2681.0, 126.0], [2667.0, 124.0], [2625.0, 122.0], [2631.0, 133.0], [2687.0, 121.0], [2577.0, 142.0], [2567.0, 127.0], [2563.0, 135.0], [2609.0, 118.0], [2615.0, 136.0], [2595.0, 137.0], [2603.0, 120.0], [2579.0, 122.0], [2641.0, 141.0], [2649.0, 127.0], [2643.0, 144.0], [2745.0, 132.0], [2721.0, 129.0], [2813.0, 125.0], [2797.0, 121.0], [2715.0, 122.0], [2703.0, 136.0], [2753.0, 109.0], [2691.0, 133.0], [2693.0, 99.0], [2733.0, 119.0], [2727.0, 123.0], [2739.0, 122.0], [2935.0, 112.0], [2881.0, 117.0], [2817.0, 119.0], [1.0, 4131.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[137.0, 4586.0], [164.0, 4571.0], [207.0, 4541.0], [251.0, 5329.0], [256.0, 5407.5], [302.0, 5297.0], [343.0, 4681.0], [438.0, 4531.0], [563.0, 4775.5], [921.0, 5613.0], [1065.0, 4650.0], [1309.0, 4815.5], [1440.0, 5424.0], [1474.0, 7116.0], [1479.0, 4647.0], [1580.0, 4890.0], [1634.0, 5114.5], [1624.0, 4995.0], [1716.0, 4944.5], [1703.0, 5438.5], [1665.0, 5009.0], [1750.0, 5358.0], [1781.0, 4902.0], [1780.0, 5074.0], [1802.0, 4650.0], [1869.0, 5281.0], [1927.0, 4704.5], [1939.0, 4836.0], [1935.0, 4686.0], [1957.0, 4797.0], [1943.0, 4802.0], [2005.0, 5093.0], [2027.0, 4751.0], [2026.0, 4967.0], [1996.0, 4852.0], [2165.0, 4736.0], [2156.0, 4821.0], [2132.0, 4731.0], [2053.0, 4904.0], [2144.0, 4775.5], [2167.0, 4726.0], [2162.0, 4881.0], [2119.0, 4939.0], [2133.0, 4938.5], [2290.0, 4542.0], [2244.0, 5036.0], [2257.0, 4659.0], [2217.0, 5357.0], [2226.0, 4857.0], [2282.0, 4921.0], [2264.0, 5463.0], [2263.0, 4875.0], [2268.0, 4809.5], [2259.0, 5561.0], [2235.0, 5154.0], [2237.0, 4884.0], [2236.0, 4960.0], [2194.0, 4781.0], [2197.0, 4908.0], [2297.0, 4826.0], [2298.0, 4755.0], [2428.0, 4961.0], [2353.0, 4697.0], [2340.0, 4653.0], [2380.0, 4875.0], [2352.0, 4524.0], [2347.0, 4770.0], [2427.0, 4790.0], [2402.0, 4726.5], [2418.0, 4861.0], [2360.0, 4835.0], [2306.0, 4723.0], [2312.0, 4847.5], [2308.0, 5561.0], [2313.0, 4724.5], [2382.0, 4807.5], [2384.0, 4921.0], [2376.0, 4813.5], [2377.0, 4823.0], [2538.0, 4879.0], [2521.0, 4800.0], [2495.0, 4780.0], [2484.0, 4822.0], [2513.0, 4962.0], [2434.0, 4783.0], [2444.0, 4670.0], [2546.0, 4661.0], [2516.0, 4865.5], [2502.0, 4738.5], [2667.0, 4688.0], [2620.0, 4812.5], [2649.0, 4846.5], [2609.0, 4779.0], [2634.0, 4770.5], [2631.0, 4751.0], [2676.0, 4816.0], [2745.0, 4782.0], [2691.0, 5353.0], [2756.0, 4772.5], [2753.0, 5301.5], [2724.0, 5429.5], [2690.0, 4785.0], [2834.0, 4860.0], [2828.0, 4888.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 3010.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1234.2, "minX": 1.67637996E12, "maxY": 2357.516666666667, "series": [{"data": [[1.67638008E12, 1627.4666666666667], [1.67638026E12, 2357.0666666666666], [1.67637996E12, 1234.2], [1.67638044E12, 1858.4833333333333], [1.67638014E12, 2147.5333333333333], [1.67638032E12, 1956.4], [1.67638002E12, 1731.9333333333334], [1.6763802E12, 2357.516666666667], [1.67638038E12, 1787.9666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638044E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.67637996E12, "maxY": 2355.7833333333333, "series": [{"data": [[1.67638008E12, 1625.0666666666666], [1.67638026E12, 2355.7833333333333], [1.67637996E12, 1208.2666666666667], [1.67638044E12, 1871.4666666666667], [1.67638014E12, 2147.116666666667], [1.67638032E12, 1951.05], [1.67638002E12, 1720.95], [1.6763802E12, 2354.6], [1.67638038E12, 1784.5]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.67638008E12, 2.4], [1.67638026E12, 1.2833333333333334], [1.67637996E12, 9.283333333333333], [1.67638044E12, 3.683333333333333], [1.67638014E12, 0.4166666666666667], [1.67638032E12, 5.366666666666666], [1.67638002E12, 10.966666666666667], [1.6763802E12, 2.9166666666666665], [1.67638038E12, 3.45]], "isOverall": false, "label": "504", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638044E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.67637996E12, "maxY": 2355.7833333333333, "series": [{"data": [[1.67638008E12, 1625.0666666666666], [1.67638026E12, 2355.7833333333333], [1.67637996E12, 1208.2666666666667], [1.67638044E12, 1871.4666666666667], [1.67638014E12, 2147.116666666667], [1.67638032E12, 1951.05], [1.67638002E12, 1720.95], [1.6763802E12, 2354.6], [1.67638038E12, 1784.5]], "isOverall": false, "label": "HTTP Request-success", "isController": false}, {"data": [[1.67638008E12, 2.4], [1.67638026E12, 1.2833333333333334], [1.67637996E12, 9.283333333333333], [1.67638044E12, 3.683333333333333], [1.67638014E12, 0.4166666666666667], [1.67638032E12, 5.366666666666666], [1.67638002E12, 10.966666666666667], [1.6763802E12, 2.9166666666666665], [1.67638038E12, 3.45]], "isOverall": false, "label": "HTTP Request-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638044E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.67637996E12, "maxY": 2355.7833333333333, "series": [{"data": [[1.67638008E12, 1625.0666666666666], [1.67638026E12, 2355.7833333333333], [1.67637996E12, 1208.2666666666667], [1.67638044E12, 1871.4666666666667], [1.67638014E12, 2147.116666666667], [1.67638032E12, 1951.05], [1.67638002E12, 1720.95], [1.6763802E12, 2354.6], [1.67638038E12, 1784.5]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.67638008E12, 2.4], [1.67638026E12, 1.2833333333333334], [1.67637996E12, 9.283333333333333], [1.67638044E12, 3.683333333333333], [1.67638014E12, 0.4166666666666667], [1.67638032E12, 5.366666666666666], [1.67638002E12, 10.966666666666667], [1.6763802E12, 2.9166666666666665], [1.67638038E12, 3.45]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638044E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

