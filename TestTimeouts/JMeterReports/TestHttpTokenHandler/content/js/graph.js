/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 995.0, "minX": 0.0, "maxY": 2806.0, "series": [{"data": [[0.0, 995.0], [0.1, 1009.0], [0.2, 1012.0], [0.3, 1015.0], [0.4, 1017.0], [0.5, 1019.0], [0.6, 1021.0], [0.7, 1022.0], [0.8, 1023.0], [0.9, 1024.0], [1.0, 1025.0], [1.1, 1026.0], [1.2, 1027.0], [1.3, 1028.0], [1.4, 1029.0], [1.5, 1030.0], [1.6, 1030.0], [1.7, 1031.0], [1.8, 1032.0], [1.9, 1033.0], [2.0, 1034.0], [2.1, 1034.0], [2.2, 1035.0], [2.3, 1036.0], [2.4, 1036.0], [2.5, 1037.0], [2.6, 1037.0], [2.7, 1038.0], [2.8, 1038.0], [2.9, 1039.0], [3.0, 1039.0], [3.1, 1040.0], [3.2, 1040.0], [3.3, 1041.0], [3.4, 1041.0], [3.5, 1042.0], [3.6, 1042.0], [3.7, 1043.0], [3.8, 1043.0], [3.9, 1044.0], [4.0, 1044.0], [4.1, 1045.0], [4.2, 1045.0], [4.3, 1046.0], [4.4, 1046.0], [4.5, 1047.0], [4.6, 1047.0], [4.7, 1047.0], [4.8, 1048.0], [4.9, 1048.0], [5.0, 1049.0], [5.1, 1049.0], [5.2, 1049.0], [5.3, 1050.0], [5.4, 1050.0], [5.5, 1051.0], [5.6, 1051.0], [5.7, 1051.0], [5.8, 1052.0], [5.9, 1052.0], [6.0, 1052.0], [6.1, 1053.0], [6.2, 1053.0], [6.3, 1053.0], [6.4, 1054.0], [6.5, 1054.0], [6.6, 1054.0], [6.7, 1055.0], [6.8, 1055.0], [6.9, 1055.0], [7.0, 1056.0], [7.1, 1056.0], [7.2, 1056.0], [7.3, 1057.0], [7.4, 1057.0], [7.5, 1057.0], [7.6, 1058.0], [7.7, 1058.0], [7.8, 1058.0], [7.9, 1059.0], [8.0, 1059.0], [8.1, 1060.0], [8.2, 1060.0], [8.3, 1060.0], [8.4, 1061.0], [8.5, 1061.0], [8.6, 1061.0], [8.7, 1061.0], [8.8, 1062.0], [8.9, 1062.0], [9.0, 1062.0], [9.1, 1063.0], [9.2, 1063.0], [9.3, 1063.0], [9.4, 1064.0], [9.5, 1064.0], [9.6, 1064.0], [9.7, 1064.0], [9.8, 1065.0], [9.9, 1065.0], [10.0, 1065.0], [10.1, 1066.0], [10.2, 1066.0], [10.3, 1066.0], [10.4, 1066.0], [10.5, 1067.0], [10.6, 1067.0], [10.7, 1067.0], [10.8, 1068.0], [10.9, 1068.0], [11.0, 1068.0], [11.1, 1068.0], [11.2, 1069.0], [11.3, 1069.0], [11.4, 1069.0], [11.5, 1069.0], [11.6, 1070.0], [11.7, 1070.0], [11.8, 1070.0], [11.9, 1071.0], [12.0, 1071.0], [12.1, 1071.0], [12.2, 1071.0], [12.3, 1072.0], [12.4, 1072.0], [12.5, 1072.0], [12.6, 1072.0], [12.7, 1073.0], [12.8, 1073.0], [12.9, 1073.0], [13.0, 1073.0], [13.1, 1074.0], [13.2, 1074.0], [13.3, 1074.0], [13.4, 1074.0], [13.5, 1075.0], [13.6, 1075.0], [13.7, 1075.0], [13.8, 1075.0], [13.9, 1076.0], [14.0, 1076.0], [14.1, 1076.0], [14.2, 1076.0], [14.3, 1077.0], [14.4, 1077.0], [14.5, 1077.0], [14.6, 1077.0], [14.7, 1078.0], [14.8, 1078.0], [14.9, 1078.0], [15.0, 1078.0], [15.1, 1079.0], [15.2, 1079.0], [15.3, 1079.0], [15.4, 1080.0], [15.5, 1080.0], [15.6, 1080.0], [15.7, 1080.0], [15.8, 1080.0], [15.9, 1081.0], [16.0, 1081.0], [16.1, 1081.0], [16.2, 1081.0], [16.3, 1082.0], [16.4, 1082.0], [16.5, 1082.0], [16.6, 1082.0], [16.7, 1083.0], [16.8, 1083.0], [16.9, 1083.0], [17.0, 1083.0], [17.1, 1084.0], [17.2, 1084.0], [17.3, 1084.0], [17.4, 1084.0], [17.5, 1085.0], [17.6, 1085.0], [17.7, 1085.0], [17.8, 1085.0], [17.9, 1086.0], [18.0, 1086.0], [18.1, 1086.0], [18.2, 1086.0], [18.3, 1087.0], [18.4, 1087.0], [18.5, 1087.0], [18.6, 1087.0], [18.7, 1087.0], [18.8, 1088.0], [18.9, 1088.0], [19.0, 1088.0], [19.1, 1089.0], [19.2, 1089.0], [19.3, 1089.0], [19.4, 1089.0], [19.5, 1090.0], [19.6, 1090.0], [19.7, 1090.0], [19.8, 1090.0], [19.9, 1091.0], [20.0, 1091.0], [20.1, 1091.0], [20.2, 1091.0], [20.3, 1091.0], [20.4, 1092.0], [20.5, 1092.0], [20.6, 1092.0], [20.7, 1092.0], [20.8, 1093.0], [20.9, 1093.0], [21.0, 1093.0], [21.1, 1093.0], [21.2, 1094.0], [21.3, 1094.0], [21.4, 1094.0], [21.5, 1094.0], [21.6, 1094.0], [21.7, 1095.0], [21.8, 1095.0], [21.9, 1095.0], [22.0, 1095.0], [22.1, 1096.0], [22.2, 1096.0], [22.3, 1096.0], [22.4, 1096.0], [22.5, 1096.0], [22.6, 1097.0], [22.7, 1097.0], [22.8, 1097.0], [22.9, 1097.0], [23.0, 1097.0], [23.1, 1098.0], [23.2, 1098.0], [23.3, 1098.0], [23.4, 1098.0], [23.5, 1099.0], [23.6, 1099.0], [23.7, 1099.0], [23.8, 1099.0], [23.9, 1099.0], [24.0, 1100.0], [24.1, 1100.0], [24.2, 1100.0], [24.3, 1100.0], [24.4, 1101.0], [24.5, 1101.0], [24.6, 1101.0], [24.7, 1101.0], [24.8, 1101.0], [24.9, 1102.0], [25.0, 1102.0], [25.1, 1102.0], [25.2, 1102.0], [25.3, 1103.0], [25.4, 1103.0], [25.5, 1103.0], [25.6, 1103.0], [25.7, 1103.0], [25.8, 1104.0], [25.9, 1104.0], [26.0, 1104.0], [26.1, 1104.0], [26.2, 1104.0], [26.3, 1105.0], [26.4, 1105.0], [26.5, 1105.0], [26.6, 1105.0], [26.7, 1106.0], [26.8, 1106.0], [26.9, 1106.0], [27.0, 1106.0], [27.1, 1107.0], [27.2, 1107.0], [27.3, 1107.0], [27.4, 1107.0], [27.5, 1107.0], [27.6, 1108.0], [27.7, 1108.0], [27.8, 1108.0], [27.9, 1108.0], [28.0, 1109.0], [28.1, 1109.0], [28.2, 1109.0], [28.3, 1109.0], [28.4, 1109.0], [28.5, 1110.0], [28.6, 1110.0], [28.7, 1110.0], [28.8, 1110.0], [28.9, 1111.0], [29.0, 1111.0], [29.1, 1111.0], [29.2, 1111.0], [29.3, 1111.0], [29.4, 1112.0], [29.5, 1112.0], [29.6, 1112.0], [29.7, 1112.0], [29.8, 1112.0], [29.9, 1113.0], [30.0, 1113.0], [30.1, 1113.0], [30.2, 1113.0], [30.3, 1114.0], [30.4, 1114.0], [30.5, 1114.0], [30.6, 1114.0], [30.7, 1114.0], [30.8, 1115.0], [30.9, 1115.0], [31.0, 1115.0], [31.1, 1115.0], [31.2, 1116.0], [31.3, 1116.0], [31.4, 1116.0], [31.5, 1116.0], [31.6, 1116.0], [31.7, 1117.0], [31.8, 1117.0], [31.9, 1117.0], [32.0, 1117.0], [32.1, 1117.0], [32.2, 1118.0], [32.3, 1118.0], [32.4, 1118.0], [32.5, 1118.0], [32.6, 1118.0], [32.7, 1119.0], [32.8, 1119.0], [32.9, 1119.0], [33.0, 1119.0], [33.1, 1120.0], [33.2, 1120.0], [33.3, 1120.0], [33.4, 1120.0], [33.5, 1121.0], [33.6, 1121.0], [33.7, 1121.0], [33.8, 1121.0], [33.9, 1121.0], [34.0, 1122.0], [34.1, 1122.0], [34.2, 1122.0], [34.3, 1122.0], [34.4, 1123.0], [34.5, 1123.0], [34.6, 1123.0], [34.7, 1123.0], [34.8, 1124.0], [34.9, 1124.0], [35.0, 1124.0], [35.1, 1124.0], [35.2, 1124.0], [35.3, 1125.0], [35.4, 1125.0], [35.5, 1125.0], [35.6, 1125.0], [35.7, 1126.0], [35.8, 1126.0], [35.9, 1126.0], [36.0, 1126.0], [36.1, 1127.0], [36.2, 1127.0], [36.3, 1127.0], [36.4, 1127.0], [36.5, 1128.0], [36.6, 1128.0], [36.7, 1128.0], [36.8, 1128.0], [36.9, 1128.0], [37.0, 1129.0], [37.1, 1129.0], [37.2, 1129.0], [37.3, 1129.0], [37.4, 1130.0], [37.5, 1130.0], [37.6, 1130.0], [37.7, 1130.0], [37.8, 1131.0], [37.9, 1131.0], [38.0, 1131.0], [38.1, 1131.0], [38.2, 1131.0], [38.3, 1132.0], [38.4, 1132.0], [38.5, 1132.0], [38.6, 1132.0], [38.7, 1132.0], [38.8, 1133.0], [38.9, 1133.0], [39.0, 1133.0], [39.1, 1133.0], [39.2, 1133.0], [39.3, 1134.0], [39.4, 1134.0], [39.5, 1134.0], [39.6, 1134.0], [39.7, 1135.0], [39.8, 1135.0], [39.9, 1135.0], [40.0, 1135.0], [40.1, 1135.0], [40.2, 1136.0], [40.3, 1136.0], [40.4, 1136.0], [40.5, 1136.0], [40.6, 1137.0], [40.7, 1137.0], [40.8, 1137.0], [40.9, 1137.0], [41.0, 1137.0], [41.1, 1138.0], [41.2, 1138.0], [41.3, 1138.0], [41.4, 1138.0], [41.5, 1139.0], [41.6, 1139.0], [41.7, 1139.0], [41.8, 1139.0], [41.9, 1139.0], [42.0, 1140.0], [42.1, 1140.0], [42.2, 1140.0], [42.3, 1140.0], [42.4, 1141.0], [42.5, 1141.0], [42.6, 1141.0], [42.7, 1141.0], [42.8, 1141.0], [42.9, 1142.0], [43.0, 1142.0], [43.1, 1142.0], [43.2, 1142.0], [43.3, 1143.0], [43.4, 1143.0], [43.5, 1143.0], [43.6, 1143.0], [43.7, 1144.0], [43.8, 1144.0], [43.9, 1144.0], [44.0, 1144.0], [44.1, 1144.0], [44.2, 1145.0], [44.3, 1145.0], [44.4, 1145.0], [44.5, 1145.0], [44.6, 1145.0], [44.7, 1146.0], [44.8, 1146.0], [44.9, 1146.0], [45.0, 1146.0], [45.1, 1146.0], [45.2, 1147.0], [45.3, 1147.0], [45.4, 1147.0], [45.5, 1147.0], [45.6, 1148.0], [45.7, 1148.0], [45.8, 1148.0], [45.9, 1148.0], [46.0, 1148.0], [46.1, 1149.0], [46.2, 1149.0], [46.3, 1149.0], [46.4, 1149.0], [46.5, 1149.0], [46.6, 1150.0], [46.7, 1150.0], [46.8, 1150.0], [46.9, 1150.0], [47.0, 1151.0], [47.1, 1151.0], [47.2, 1151.0], [47.3, 1151.0], [47.4, 1151.0], [47.5, 1152.0], [47.6, 1152.0], [47.7, 1152.0], [47.8, 1152.0], [47.9, 1153.0], [48.0, 1153.0], [48.1, 1153.0], [48.2, 1153.0], [48.3, 1153.0], [48.4, 1154.0], [48.5, 1154.0], [48.6, 1154.0], [48.7, 1154.0], [48.8, 1154.0], [48.9, 1155.0], [49.0, 1155.0], [49.1, 1155.0], [49.2, 1155.0], [49.3, 1156.0], [49.4, 1156.0], [49.5, 1156.0], [49.6, 1156.0], [49.7, 1156.0], [49.8, 1157.0], [49.9, 1157.0], [50.0, 1157.0], [50.1, 1157.0], [50.2, 1157.0], [50.3, 1158.0], [50.4, 1158.0], [50.5, 1158.0], [50.6, 1158.0], [50.7, 1159.0], [50.8, 1159.0], [50.9, 1159.0], [51.0, 1159.0], [51.1, 1159.0], [51.2, 1160.0], [51.3, 1160.0], [51.4, 1160.0], [51.5, 1160.0], [51.6, 1161.0], [51.7, 1161.0], [51.8, 1161.0], [51.9, 1161.0], [52.0, 1162.0], [52.1, 1162.0], [52.2, 1162.0], [52.3, 1162.0], [52.4, 1162.0], [52.5, 1163.0], [52.6, 1163.0], [52.7, 1163.0], [52.8, 1163.0], [52.9, 1164.0], [53.0, 1164.0], [53.1, 1164.0], [53.2, 1164.0], [53.3, 1165.0], [53.4, 1165.0], [53.5, 1165.0], [53.6, 1165.0], [53.7, 1166.0], [53.8, 1166.0], [53.9, 1166.0], [54.0, 1167.0], [54.1, 1167.0], [54.2, 1167.0], [54.3, 1167.0], [54.4, 1168.0], [54.5, 1168.0], [54.6, 1168.0], [54.7, 1168.0], [54.8, 1169.0], [54.9, 1169.0], [55.0, 1169.0], [55.1, 1169.0], [55.2, 1170.0], [55.3, 1170.0], [55.4, 1170.0], [55.5, 1170.0], [55.6, 1171.0], [55.7, 1171.0], [55.8, 1171.0], [55.9, 1171.0], [56.0, 1172.0], [56.1, 1172.0], [56.2, 1172.0], [56.3, 1172.0], [56.4, 1173.0], [56.5, 1173.0], [56.6, 1173.0], [56.7, 1173.0], [56.8, 1174.0], [56.9, 1174.0], [57.0, 1174.0], [57.1, 1174.0], [57.2, 1175.0], [57.3, 1175.0], [57.4, 1175.0], [57.5, 1175.0], [57.6, 1176.0], [57.7, 1176.0], [57.8, 1176.0], [57.9, 1177.0], [58.0, 1177.0], [58.1, 1177.0], [58.2, 1177.0], [58.3, 1178.0], [58.4, 1178.0], [58.5, 1178.0], [58.6, 1178.0], [58.7, 1179.0], [58.8, 1179.0], [58.9, 1179.0], [59.0, 1179.0], [59.1, 1180.0], [59.2, 1180.0], [59.3, 1180.0], [59.4, 1181.0], [59.5, 1181.0], [59.6, 1181.0], [59.7, 1181.0], [59.8, 1182.0], [59.9, 1182.0], [60.0, 1182.0], [60.1, 1183.0], [60.2, 1183.0], [60.3, 1183.0], [60.4, 1183.0], [60.5, 1184.0], [60.6, 1184.0], [60.7, 1184.0], [60.8, 1185.0], [60.9, 1185.0], [61.0, 1185.0], [61.1, 1185.0], [61.2, 1186.0], [61.3, 1186.0], [61.4, 1186.0], [61.5, 1187.0], [61.6, 1187.0], [61.7, 1187.0], [61.8, 1187.0], [61.9, 1188.0], [62.0, 1188.0], [62.1, 1188.0], [62.2, 1189.0], [62.3, 1189.0], [62.4, 1189.0], [62.5, 1189.0], [62.6, 1190.0], [62.7, 1190.0], [62.8, 1190.0], [62.9, 1191.0], [63.0, 1191.0], [63.1, 1191.0], [63.2, 1192.0], [63.3, 1192.0], [63.4, 1192.0], [63.5, 1192.0], [63.6, 1193.0], [63.7, 1193.0], [63.8, 1193.0], [63.9, 1194.0], [64.0, 1194.0], [64.1, 1194.0], [64.2, 1194.0], [64.3, 1195.0], [64.4, 1195.0], [64.5, 1195.0], [64.6, 1196.0], [64.7, 1196.0], [64.8, 1196.0], [64.9, 1197.0], [65.0, 1197.0], [65.1, 1197.0], [65.2, 1198.0], [65.3, 1198.0], [65.4, 1198.0], [65.5, 1198.0], [65.6, 1199.0], [65.7, 1199.0], [65.8, 1199.0], [65.9, 1200.0], [66.0, 1200.0], [66.1, 1200.0], [66.2, 1201.0], [66.3, 1201.0], [66.4, 1201.0], [66.5, 1202.0], [66.6, 1202.0], [66.7, 1202.0], [66.8, 1202.0], [66.9, 1203.0], [67.0, 1203.0], [67.1, 1203.0], [67.2, 1204.0], [67.3, 1204.0], [67.4, 1204.0], [67.5, 1205.0], [67.6, 1205.0], [67.7, 1205.0], [67.8, 1206.0], [67.9, 1206.0], [68.0, 1206.0], [68.1, 1207.0], [68.2, 1207.0], [68.3, 1207.0], [68.4, 1208.0], [68.5, 1208.0], [68.6, 1208.0], [68.7, 1209.0], [68.8, 1209.0], [68.9, 1209.0], [69.0, 1210.0], [69.1, 1210.0], [69.2, 1210.0], [69.3, 1211.0], [69.4, 1211.0], [69.5, 1211.0], [69.6, 1212.0], [69.7, 1212.0], [69.8, 1212.0], [69.9, 1213.0], [70.0, 1213.0], [70.1, 1213.0], [70.2, 1214.0], [70.3, 1214.0], [70.4, 1215.0], [70.5, 1215.0], [70.6, 1215.0], [70.7, 1216.0], [70.8, 1216.0], [70.9, 1217.0], [71.0, 1217.0], [71.1, 1217.0], [71.2, 1218.0], [71.3, 1218.0], [71.4, 1218.0], [71.5, 1219.0], [71.6, 1219.0], [71.7, 1219.0], [71.8, 1220.0], [71.9, 1220.0], [72.0, 1221.0], [72.1, 1221.0], [72.2, 1221.0], [72.3, 1222.0], [72.4, 1222.0], [72.5, 1222.0], [72.6, 1223.0], [72.7, 1223.0], [72.8, 1224.0], [72.9, 1224.0], [73.0, 1224.0], [73.1, 1225.0], [73.2, 1225.0], [73.3, 1226.0], [73.4, 1226.0], [73.5, 1226.0], [73.6, 1227.0], [73.7, 1227.0], [73.8, 1228.0], [73.9, 1228.0], [74.0, 1228.0], [74.1, 1229.0], [74.2, 1229.0], [74.3, 1230.0], [74.4, 1230.0], [74.5, 1230.0], [74.6, 1231.0], [74.7, 1231.0], [74.8, 1232.0], [74.9, 1232.0], [75.0, 1232.0], [75.1, 1233.0], [75.2, 1233.0], [75.3, 1234.0], [75.4, 1234.0], [75.5, 1235.0], [75.6, 1235.0], [75.7, 1235.0], [75.8, 1236.0], [75.9, 1236.0], [76.0, 1237.0], [76.1, 1237.0], [76.2, 1237.0], [76.3, 1238.0], [76.4, 1238.0], [76.5, 1239.0], [76.6, 1239.0], [76.7, 1239.0], [76.8, 1240.0], [76.9, 1240.0], [77.0, 1241.0], [77.1, 1241.0], [77.2, 1241.0], [77.3, 1242.0], [77.4, 1242.0], [77.5, 1243.0], [77.6, 1243.0], [77.7, 1244.0], [77.8, 1244.0], [77.9, 1244.0], [78.0, 1245.0], [78.1, 1245.0], [78.2, 1246.0], [78.3, 1246.0], [78.4, 1247.0], [78.5, 1247.0], [78.6, 1247.0], [78.7, 1248.0], [78.8, 1248.0], [78.9, 1249.0], [79.0, 1249.0], [79.1, 1250.0], [79.2, 1250.0], [79.3, 1251.0], [79.4, 1251.0], [79.5, 1251.0], [79.6, 1252.0], [79.7, 1252.0], [79.8, 1253.0], [79.9, 1253.0], [80.0, 1254.0], [80.1, 1254.0], [80.2, 1255.0], [80.3, 1255.0], [80.4, 1256.0], [80.5, 1256.0], [80.6, 1257.0], [80.7, 1257.0], [80.8, 1258.0], [80.9, 1258.0], [81.0, 1259.0], [81.1, 1259.0], [81.2, 1260.0], [81.3, 1261.0], [81.4, 1261.0], [81.5, 1262.0], [81.6, 1262.0], [81.7, 1263.0], [81.8, 1263.0], [81.9, 1264.0], [82.0, 1265.0], [82.1, 1265.0], [82.2, 1266.0], [82.3, 1266.0], [82.4, 1267.0], [82.5, 1267.0], [82.6, 1268.0], [82.7, 1268.0], [82.8, 1269.0], [82.9, 1270.0], [83.0, 1270.0], [83.1, 1271.0], [83.2, 1272.0], [83.3, 1272.0], [83.4, 1273.0], [83.5, 1273.0], [83.6, 1274.0], [83.7, 1275.0], [83.8, 1275.0], [83.9, 1276.0], [84.0, 1277.0], [84.1, 1277.0], [84.2, 1278.0], [84.3, 1278.0], [84.4, 1279.0], [84.5, 1280.0], [84.6, 1281.0], [84.7, 1281.0], [84.8, 1282.0], [84.9, 1283.0], [85.0, 1283.0], [85.1, 1284.0], [85.2, 1285.0], [85.3, 1286.0], [85.4, 1286.0], [85.5, 1287.0], [85.6, 1288.0], [85.7, 1289.0], [85.8, 1289.0], [85.9, 1290.0], [86.0, 1291.0], [86.1, 1291.0], [86.2, 1292.0], [86.3, 1293.0], [86.4, 1294.0], [86.5, 1294.0], [86.6, 1295.0], [86.7, 1296.0], [86.8, 1296.0], [86.9, 1297.0], [87.0, 1298.0], [87.1, 1299.0], [87.2, 1300.0], [87.3, 1301.0], [87.4, 1301.0], [87.5, 1302.0], [87.6, 1303.0], [87.7, 1304.0], [87.8, 1305.0], [87.9, 1306.0], [88.0, 1307.0], [88.1, 1308.0], [88.2, 1309.0], [88.3, 1310.0], [88.4, 1311.0], [88.5, 1311.0], [88.6, 1312.0], [88.7, 1313.0], [88.8, 1314.0], [88.9, 1315.0], [89.0, 1316.0], [89.1, 1317.0], [89.2, 1318.0], [89.3, 1319.0], [89.4, 1320.0], [89.5, 1321.0], [89.6, 1322.0], [89.7, 1323.0], [89.8, 1324.0], [89.9, 1325.0], [90.0, 1326.0], [90.1, 1327.0], [90.2, 1328.0], [90.3, 1329.0], [90.4, 1330.0], [90.5, 1331.0], [90.6, 1333.0], [90.7, 1334.0], [90.8, 1335.0], [90.9, 1337.0], [91.0, 1338.0], [91.1, 1339.0], [91.2, 1341.0], [91.3, 1342.0], [91.4, 1343.0], [91.5, 1345.0], [91.6, 1346.0], [91.7, 1348.0], [91.8, 1349.0], [91.9, 1350.0], [92.0, 1351.0], [92.1, 1353.0], [92.2, 1354.0], [92.3, 1355.0], [92.4, 1357.0], [92.5, 1358.0], [92.6, 1360.0], [92.7, 1361.0], [92.8, 1363.0], [92.9, 1364.0], [93.0, 1366.0], [93.1, 1368.0], [93.2, 1370.0], [93.3, 1371.0], [93.4, 1373.0], [93.5, 1374.0], [93.6, 1376.0], [93.7, 1378.0], [93.8, 1380.0], [93.9, 1382.0], [94.0, 1384.0], [94.1, 1386.0], [94.2, 1388.0], [94.3, 1391.0], [94.4, 1393.0], [94.5, 1395.0], [94.6, 1398.0], [94.7, 1401.0], [94.8, 1403.0], [94.9, 1406.0], [95.0, 1408.0], [95.1, 1410.0], [95.2, 1413.0], [95.3, 1415.0], [95.4, 1418.0], [95.5, 1420.0], [95.6, 1423.0], [95.7, 1426.0], [95.8, 1428.0], [95.9, 1431.0], [96.0, 1434.0], [96.1, 1436.0], [96.2, 1440.0], [96.3, 1443.0], [96.4, 1446.0], [96.5, 1449.0], [96.6, 1451.0], [96.7, 1455.0], [96.8, 1457.0], [96.9, 1460.0], [97.0, 1464.0], [97.1, 1468.0], [97.2, 1472.0], [97.3, 1476.0], [97.4, 1478.0], [97.5, 1482.0], [97.6, 1486.0], [97.7, 1491.0], [97.8, 1496.0], [97.9, 1502.0], [98.0, 1508.0], [98.1, 1515.0], [98.2, 1523.0], [98.3, 1531.0], [98.4, 1538.0], [98.5, 1546.0], [98.6, 1558.0], [98.7, 1576.0], [98.8, 1603.0], [98.9, 1620.0], [99.0, 1643.0], [99.1, 1668.0], [99.2, 1704.0], [99.3, 1754.0], [99.4, 1937.0], [99.5, 2114.0], [99.6, 2192.0], [99.7, 2279.0], [99.8, 2365.0], [99.9, 2474.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 3.0, "minX": 900.0, "maxY": 138826.0, "series": [{"data": [[2100.0, 393.0], [2200.0, 393.0], [2300.0, 339.0], [2400.0, 283.0], [2500.0, 182.0], [2600.0, 77.0], [2700.0, 15.0], [2800.0, 3.0], [900.0, 33.0], [1000.0, 79357.0], [1100.0, 138826.0], [1200.0, 70729.0], [1300.0, 24778.0], [1400.0, 10644.0], [1500.0, 3039.0], [1600.0, 1342.0], [1700.0, 585.0], [1800.0, 89.0], [1900.0, 112.0], [2000.0, 219.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 2800.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 390.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 324315.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 324315.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 6733.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 390.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 1000.0, "minX": 1.67637486E12, "maxY": 1000.0, "series": [{"data": [[1.67637522E12, 1000.0], [1.67637504E12, 1000.0], [1.6763751E12, 1000.0], [1.67637492E12, 1000.0], [1.67637498E12, 1000.0], [1.67637528E12, 1000.0], [1.67637486E12, 1000.0], [1.67637516E12, 1000.0]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67637528E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1185.849102999673, "minX": 1000.0, "maxY": 1185.849102999673, "series": [{"data": [[1000.0, 1185.849102999673]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[1000.0, 1185.849102999673]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 1000.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 64856.25, "minX": 1.67637486E12, "maxY": 238658.56666666668, "series": [{"data": [[1.67637522E12, 237472.3], [1.67637504E12, 237200.83333333334], [1.6763751E12, 229638.7], [1.67637492E12, 130673.33333333333], [1.67637498E12, 168263.73333333334], [1.67637528E12, 143489.78333333333], [1.67637486E12, 146820.18333333332], [1.67637516E12, 238658.56666666668]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.67637522E12, 117787.5], [1.67637504E12, 112815.0], [1.6763751E12, 113899.5], [1.67637492E12, 64856.25], [1.67637498E12, 83513.25], [1.67637528E12, 70499.25], [1.67637486E12, 66105.0], [1.67637516E12, 116259.75]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67637528E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 1146.161967526263, "minX": 1.67637486E12, "maxY": 1282.3032334921613, "series": [{"data": [[1.67637522E12, 1146.161967526263], [1.67637504E12, 1196.1810929397634], [1.6763751E12, 1187.495575046429], [1.67637492E12, 1157.7478924544714], [1.67637498E12, 1232.2402672629855], [1.67637528E12, 1153.9091692464654], [1.67637486E12, 1282.3032334921613], [1.67637516E12, 1161.29560101411]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67637528E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 1146.161872015278, "minX": 1.67637486E12, "maxY": 1282.3020762423478, "series": [{"data": [[1.67637522E12, 1146.161872015278], [1.67637504E12, 1196.180793777422], [1.6763751E12, 1187.4954170123688], [1.67637492E12, 1157.7476496097152], [1.67637498E12, 1232.2394590079853], [1.67637528E12, 1153.9090096703123], [1.67637486E12, 1282.3020762423478], [1.67637516E12, 1161.295388128733]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67637528E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.007005864024307579, "minX": 1.67637486E12, "maxY": 2.334343090537781, "series": [{"data": [[1.67637522E12, 0.007201528175740123], [1.67637504E12, 0.007857997606701254], [1.6763751E12, 0.0071905495634308385], [1.67637492E12, 0.012870771899392902], [1.67637498E12, 0.13573295255543158], [1.67637528E12, 0.031053521845977025], [1.67637486E12, 2.334343090537781], [1.67637516E12, 0.007005864024307579]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67637528E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 995.0, "minX": 1.67637486E12, "maxY": 2806.0, "series": [{"data": [[1.67637522E12, 1602.0], [1.67637504E12, 1749.0], [1.6763751E12, 1623.0], [1.67637492E12, 1613.0], [1.67637498E12, 2806.0], [1.67637528E12, 1609.0], [1.67637486E12, 2777.0], [1.67637516E12, 1588.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.67637522E12, 1259.0], [1.67637504E12, 1344.0], [1.6763751E12, 1313.0], [1.67637492E12, 1251.0], [1.67637498E12, 1301.0], [1.67637528E12, 1295.0], [1.67637486E12, 1349.0], [1.67637516E12, 1297.0]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.67637522E12, 1375.0], [1.67637504E12, 1497.0], [1.6763751E12, 1462.0], [1.67637492E12, 1431.0], [1.67637498E12, 1474.0], [1.67637528E12, 1488.9900000000016], [1.67637486E12, 1579.0], [1.67637516E12, 1474.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.67637522E12, 1301.0], [1.67637504E12, 1405.0], [1.6763751E12, 1361.0], [1.67637492E12, 1291.0], [1.67637498E12, 1356.0], [1.67637528E12, 1357.0], [1.67637486E12, 1439.0], [1.67637516E12, 1357.0]], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [[1.67637522E12, 995.0], [1.67637504E12, 997.0], [1.6763751E12, 1004.0], [1.67637492E12, 998.0], [1.67637498E12, 996.0], [1.67637528E12, 996.0], [1.67637486E12, 1002.0], [1.67637516E12, 998.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.67637522E12, 1138.0], [1.67637504E12, 1176.0], [1.6763751E12, 1173.0], [1.67637492E12, 1137.0], [1.67637498E12, 1155.0], [1.67637528E12, 1143.0], [1.67637486E12, 1181.0], [1.67637516E12, 1150.0]], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67637528E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 1059.0, "minX": 176.0, "maxY": 2582.5, "series": [{"data": [[176.0, 1461.0], [215.0, 1245.0], [335.0, 1160.0], [518.0, 2205.0], [517.0, 2296.0], [541.0, 1296.0], [532.0, 1389.5], [560.0, 1207.0], [567.0, 1429.0], [570.0, 1195.0], [597.0, 1251.0], [590.0, 1524.0], [595.0, 1103.0], [599.0, 1197.0], [637.0, 1526.0], [618.0, 1224.0], [635.0, 1151.0], [622.0, 1366.0], [629.0, 1173.0], [646.0, 1470.5], [654.0, 1280.5], [664.0, 1296.5], [661.0, 1246.0], [642.0, 1211.5], [650.0, 1244.0], [671.0, 1312.0], [686.0, 2186.5], [695.0, 1150.0], [694.0, 1200.5], [703.0, 1122.0], [696.0, 1204.0], [691.0, 1114.0], [728.0, 1113.0], [712.0, 1208.5], [715.0, 1157.0], [719.0, 1166.0], [725.0, 1238.0], [721.0, 1141.5], [711.0, 1137.0], [734.0, 1082.0], [732.0, 1211.0], [726.0, 1098.0], [727.0, 1098.0], [708.0, 1153.0], [707.0, 1144.0], [763.0, 1106.0], [738.0, 1327.0], [737.0, 1225.0], [750.0, 1226.0], [747.0, 1200.0], [744.0, 1099.0], [746.0, 1125.0], [752.0, 1130.5], [767.0, 1227.0], [743.0, 1142.0], [766.0, 1072.0], [764.0, 1091.0], [761.0, 1190.0], [757.0, 1191.0], [755.0, 1159.0], [771.0, 1235.0], [799.0, 1180.0], [797.0, 1092.0], [796.0, 1172.0], [795.0, 1207.0], [780.0, 1161.0], [779.0, 1211.0], [778.0, 1272.0], [776.0, 1153.5], [786.0, 1195.0], [785.0, 1089.0], [787.0, 1113.0], [789.0, 1158.0], [788.0, 1136.0], [791.0, 1200.0], [784.0, 1125.5], [783.0, 1286.0], [782.0, 1108.0], [781.0, 1187.0], [773.0, 1199.0], [772.0, 1215.0], [793.0, 1274.0], [794.0, 1176.0], [792.0, 1213.0], [774.0, 1166.0], [769.0, 1155.0], [768.0, 1162.0], [827.0, 1255.0], [802.0, 1137.0], [812.0, 1191.0], [808.0, 1157.5], [810.0, 1216.0], [820.0, 1253.0], [817.0, 1262.0], [818.0, 1171.0], [819.0, 1119.0], [821.0, 1200.0], [822.0, 1115.0], [806.0, 1175.0], [826.0, 1316.0], [825.0, 1115.0], [824.0, 1202.0], [800.0, 1176.0], [801.0, 1122.0], [815.0, 1121.0], [813.0, 1193.0], [814.0, 1090.0], [831.0, 1178.5], [830.0, 1096.0], [829.0, 1146.0], [804.0, 1132.0], [803.0, 1199.0], [837.0, 1136.0], [847.0, 1150.0], [861.0, 1228.0], [849.0, 1164.0], [848.0, 1138.0], [851.0, 1227.0], [850.0, 1197.0], [856.0, 1196.0], [857.0, 1175.5], [858.0, 1173.0], [859.0, 1184.0], [860.0, 1103.5], [842.0, 1113.0], [844.0, 1135.0], [845.0, 1239.0], [846.0, 1143.0], [841.0, 1153.0], [832.0, 1245.0], [836.0, 1211.0], [852.0, 1198.0], [853.0, 1160.0], [855.0, 1137.0], [854.0, 1139.0], [838.0, 1105.5], [839.0, 1168.0], [867.0, 1176.0], [874.0, 1186.0], [872.0, 1221.0], [873.0, 1115.0], [890.0, 1109.0], [889.0, 1198.0], [892.0, 1111.0], [891.0, 1135.0], [878.0, 1216.0], [865.0, 1183.0], [866.0, 1102.0], [887.0, 1148.0], [886.0, 1155.0], [881.0, 1117.0], [883.0, 1087.0], [884.0, 1146.0], [893.0, 1145.5], [871.0, 1113.0], [868.0, 1170.5], [869.0, 1096.0], [888.0, 1163.0], [864.0, 1125.0], [879.0, 1224.0], [913.0, 1095.0], [923.0, 1153.0], [922.0, 1118.0], [926.0, 1131.0], [924.0, 1113.0], [912.0, 1069.0], [927.0, 1148.0], [919.0, 1479.0], [921.0, 1140.0], [916.0, 1121.0], [917.0, 1177.0], [903.0, 1158.0], [898.0, 1167.0], [899.0, 1139.5], [902.0, 1100.0], [915.0, 1109.0], [897.0, 1106.0], [896.0, 1125.0], [909.0, 1126.5], [910.0, 1130.0], [908.0, 1116.0], [904.0, 1256.0], [907.0, 1117.0], [905.0, 1154.5], [932.0, 1317.0], [954.0, 1145.0], [951.0, 1117.5], [949.0, 1084.0], [942.0, 1140.0], [935.0, 1156.0], [933.0, 1131.0], [952.0, 1214.0], [953.0, 1094.0], [938.0, 1160.0], [936.0, 1059.0], [941.0, 1098.0], [939.0, 1204.0], [948.0, 1135.0], [959.0, 1274.0], [958.0, 1150.0], [945.0, 1114.0], [956.0, 1109.5], [957.0, 1143.0], [928.0, 1162.5], [943.0, 1125.0], [931.0, 1134.0], [929.0, 1148.0], [976.0, 1149.0], [975.0, 1167.0], [962.0, 1145.0], [980.0, 1532.0], [981.0, 1108.0], [982.0, 1141.0], [983.0, 1163.0], [973.0, 1131.5], [974.0, 1229.0], [978.0, 1102.0], [979.0, 1130.0], [964.0, 1175.0], [986.0, 1215.0], [965.0, 1172.0], [988.0, 1220.5], [989.0, 1181.0], [969.0, 1206.5], [971.0, 1252.0], [970.0, 1134.0], [968.0, 1144.0], [972.0, 1139.0], [995.0, 1156.0], [1000.0, 1145.0], [996.0, 1167.0], [999.0, 1227.0], [992.0, 1238.0], [993.0, 1130.0], [994.0, 1268.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[567.0, 1523.5], [622.0, 1611.0], [654.0, 1500.0], [686.0, 2582.5], [176.0, 1575.0], [738.0, 1487.5], [791.0, 1503.0], [845.0, 1502.0], [836.0, 1613.0], [852.0, 1847.0], [886.0, 1552.0], [994.0, 1537.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 1000.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 1059.0, "minX": 176.0, "maxY": 2582.5, "series": [{"data": [[176.0, 1461.0], [215.0, 1245.0], [335.0, 1160.0], [518.0, 2205.0], [517.0, 2296.0], [541.0, 1296.0], [532.0, 1389.5], [560.0, 1207.0], [567.0, 1429.0], [570.0, 1195.0], [597.0, 1251.0], [590.0, 1524.0], [595.0, 1103.0], [599.0, 1197.0], [637.0, 1526.0], [618.0, 1224.0], [635.0, 1151.0], [622.0, 1366.0], [629.0, 1173.0], [646.0, 1470.5], [654.0, 1280.5], [664.0, 1296.5], [661.0, 1246.0], [642.0, 1211.5], [650.0, 1244.0], [671.0, 1312.0], [686.0, 2186.5], [695.0, 1150.0], [694.0, 1200.5], [703.0, 1122.0], [696.0, 1204.0], [691.0, 1114.0], [728.0, 1113.0], [712.0, 1208.5], [715.0, 1157.0], [719.0, 1166.0], [725.0, 1238.0], [721.0, 1141.5], [711.0, 1137.0], [734.0, 1082.0], [732.0, 1211.0], [726.0, 1098.0], [727.0, 1098.0], [708.0, 1153.0], [707.0, 1144.0], [763.0, 1106.0], [738.0, 1327.0], [737.0, 1225.0], [750.0, 1226.0], [747.0, 1200.0], [744.0, 1099.0], [746.0, 1125.0], [752.0, 1130.5], [767.0, 1227.0], [743.0, 1142.0], [766.0, 1072.0], [764.0, 1091.0], [761.0, 1190.0], [757.0, 1191.0], [755.0, 1159.0], [771.0, 1235.0], [799.0, 1180.0], [797.0, 1092.0], [796.0, 1172.0], [795.0, 1207.0], [780.0, 1161.0], [779.0, 1211.0], [778.0, 1272.0], [776.0, 1153.5], [786.0, 1195.0], [785.0, 1089.0], [787.0, 1113.0], [789.0, 1158.0], [788.0, 1136.0], [791.0, 1200.0], [784.0, 1125.5], [783.0, 1286.0], [782.0, 1108.0], [781.0, 1187.0], [773.0, 1199.0], [772.0, 1215.0], [793.0, 1274.0], [794.0, 1176.0], [792.0, 1213.0], [774.0, 1166.0], [769.0, 1155.0], [768.0, 1162.0], [827.0, 1255.0], [802.0, 1137.0], [812.0, 1191.0], [808.0, 1157.5], [810.0, 1216.0], [820.0, 1253.0], [817.0, 1262.0], [818.0, 1171.0], [819.0, 1119.0], [821.0, 1200.0], [822.0, 1115.0], [806.0, 1175.0], [826.0, 1316.0], [825.0, 1115.0], [824.0, 1202.0], [800.0, 1176.0], [801.0, 1122.0], [815.0, 1121.0], [813.0, 1193.0], [814.0, 1090.0], [831.0, 1178.5], [830.0, 1096.0], [829.0, 1146.0], [804.0, 1132.0], [803.0, 1199.0], [837.0, 1136.0], [847.0, 1150.0], [861.0, 1228.0], [849.0, 1164.0], [848.0, 1138.0], [851.0, 1227.0], [850.0, 1197.0], [856.0, 1196.0], [857.0, 1175.5], [858.0, 1173.0], [859.0, 1184.0], [860.0, 1103.5], [842.0, 1113.0], [844.0, 1135.0], [845.0, 1239.0], [846.0, 1143.0], [841.0, 1153.0], [832.0, 1245.0], [836.0, 1211.0], [852.0, 1198.0], [853.0, 1160.0], [855.0, 1137.0], [854.0, 1139.0], [838.0, 1105.5], [839.0, 1168.0], [867.0, 1176.0], [874.0, 1186.0], [872.0, 1221.0], [873.0, 1115.0], [890.0, 1109.0], [889.0, 1198.0], [892.0, 1111.0], [891.0, 1135.0], [878.0, 1216.0], [865.0, 1183.0], [866.0, 1102.0], [887.0, 1148.0], [886.0, 1155.0], [881.0, 1117.0], [883.0, 1087.0], [884.0, 1146.0], [893.0, 1145.5], [871.0, 1113.0], [868.0, 1170.5], [869.0, 1096.0], [888.0, 1163.0], [864.0, 1125.0], [879.0, 1224.0], [913.0, 1095.0], [923.0, 1153.0], [922.0, 1118.0], [926.0, 1131.0], [924.0, 1113.0], [912.0, 1069.0], [927.0, 1148.0], [919.0, 1479.0], [921.0, 1140.0], [916.0, 1121.0], [917.0, 1177.0], [903.0, 1158.0], [898.0, 1167.0], [899.0, 1139.5], [902.0, 1100.0], [915.0, 1109.0], [897.0, 1106.0], [896.0, 1125.0], [909.0, 1126.5], [910.0, 1130.0], [908.0, 1116.0], [904.0, 1256.0], [907.0, 1117.0], [905.0, 1154.5], [932.0, 1317.0], [954.0, 1145.0], [951.0, 1117.5], [949.0, 1084.0], [942.0, 1140.0], [935.0, 1156.0], [933.0, 1131.0], [952.0, 1214.0], [953.0, 1094.0], [938.0, 1160.0], [936.0, 1059.0], [941.0, 1098.0], [939.0, 1204.0], [948.0, 1135.0], [959.0, 1274.0], [958.0, 1150.0], [945.0, 1114.0], [956.0, 1109.5], [957.0, 1143.0], [928.0, 1162.5], [943.0, 1125.0], [931.0, 1134.0], [929.0, 1148.0], [976.0, 1149.0], [975.0, 1167.0], [962.0, 1145.0], [980.0, 1532.0], [981.0, 1108.0], [982.0, 1141.0], [983.0, 1163.0], [973.0, 1131.5], [974.0, 1229.0], [978.0, 1102.0], [979.0, 1130.0], [964.0, 1175.0], [986.0, 1215.0], [965.0, 1172.0], [988.0, 1220.5], [989.0, 1181.0], [969.0, 1206.5], [971.0, 1252.0], [970.0, 1134.0], [968.0, 1144.0], [972.0, 1139.0], [995.0, 1156.0], [1000.0, 1145.0], [996.0, 1167.0], [999.0, 1227.0], [992.0, 1238.0], [993.0, 1130.0], [994.0, 1268.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[567.0, 1523.5], [622.0, 1611.0], [654.0, 1500.0], [686.0, 2582.5], [176.0, 1575.0], [738.0, 1487.5], [791.0, 1503.0], [845.0, 1502.0], [836.0, 1613.0], [852.0, 1847.0], [886.0, 1552.0], [994.0, 1537.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 1000.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 463.75, "minX": 1.67637486E12, "maxY": 872.5, "series": [{"data": [[1.67637522E12, 872.5], [1.67637504E12, 835.65], [1.6763751E12, 843.7166666666667], [1.67637492E12, 463.75], [1.67637498E12, 635.2833333333333], [1.67637528E12, 505.55], [1.67637486E12, 506.3333333333333], [1.67637516E12, 861.1833333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67637528E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.67637486E12, "maxY": 872.4666666666667, "series": [{"data": [[1.67637522E12, 872.4666666666667], [1.67637504E12, 833.5], [1.6763751E12, 843.6666666666666], [1.67637492E12, 480.4166666666667], [1.67637498E12, 618.6166666666667], [1.67637528E12, 521.9], [1.67637486E12, 486.68333333333334], [1.67637516E12, 860.2166666666667]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.67637522E12, 0.03333333333333333], [1.67637504E12, 2.1666666666666665], [1.6763751E12, 0.03333333333333333], [1.67637528E12, 0.31666666666666665], [1.67637486E12, 2.9833333333333334], [1.67637516E12, 0.9666666666666667]], "isOverall": false, "label": "504", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67637528E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.67637486E12, "maxY": 872.4666666666667, "series": [{"data": [[1.67637522E12, 872.4666666666667], [1.67637504E12, 833.5], [1.6763751E12, 843.6666666666666], [1.67637492E12, 480.4166666666667], [1.67637498E12, 618.6166666666667], [1.67637528E12, 521.9], [1.67637486E12, 486.68333333333334], [1.67637516E12, 860.2166666666667]], "isOverall": false, "label": "HTTP Request-success", "isController": false}, {"data": [[1.67637522E12, 0.03333333333333333], [1.67637504E12, 2.1666666666666665], [1.6763751E12, 0.03333333333333333], [1.67637528E12, 0.31666666666666665], [1.67637486E12, 2.9833333333333334], [1.67637516E12, 0.9666666666666667]], "isOverall": false, "label": "HTTP Request-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67637528E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.67637486E12, "maxY": 872.4666666666667, "series": [{"data": [[1.67637522E12, 872.4666666666667], [1.67637504E12, 833.5], [1.6763751E12, 843.6666666666666], [1.67637492E12, 480.4166666666667], [1.67637498E12, 618.6166666666667], [1.67637528E12, 521.9], [1.67637486E12, 486.68333333333334], [1.67637516E12, 860.2166666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.67637522E12, 0.03333333333333333], [1.67637504E12, 2.1666666666666665], [1.6763751E12, 0.03333333333333333], [1.67637528E12, 0.31666666666666665], [1.67637486E12, 2.9833333333333334], [1.67637516E12, 0.9666666666666667]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67637528E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

