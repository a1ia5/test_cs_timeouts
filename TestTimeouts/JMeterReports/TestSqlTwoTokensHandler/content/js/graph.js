/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 2989.0, "minX": 0.0, "maxY": 185081.0, "series": [{"data": [[0.0, 2989.0], [0.1, 2993.0], [0.2, 2995.0], [0.3, 2996.0], [0.4, 2996.0], [0.5, 2996.0], [0.6, 2997.0], [0.7, 2997.0], [0.8, 2997.0], [0.9, 2997.0], [1.0, 2998.0], [1.1, 2998.0], [1.2, 2998.0], [1.3, 2998.0], [1.4, 2998.0], [1.5, 2998.0], [1.6, 2999.0], [1.7, 2999.0], [1.8, 2999.0], [1.9, 2999.0], [2.0, 2999.0], [2.1, 2999.0], [2.2, 2999.0], [2.3, 2999.0], [2.4, 2999.0], [2.5, 2999.0], [2.6, 2999.0], [2.7, 2999.0], [2.8, 3000.0], [2.9, 3000.0], [3.0, 3000.0], [3.1, 3000.0], [3.2, 3000.0], [3.3, 3000.0], [3.4, 3000.0], [3.5, 3000.0], [3.6, 3000.0], [3.7, 3000.0], [3.8, 3000.0], [3.9, 3000.0], [4.0, 3000.0], [4.1, 3000.0], [4.2, 3000.0], [4.3, 3000.0], [4.4, 3000.0], [4.5, 3000.0], [4.6, 3000.0], [4.7, 3000.0], [4.8, 3000.0], [4.9, 3001.0], [5.0, 3001.0], [5.1, 3001.0], [5.2, 3001.0], [5.3, 3001.0], [5.4, 3001.0], [5.5, 3001.0], [5.6, 3001.0], [5.7, 3001.0], [5.8, 3001.0], [5.9, 3001.0], [6.0, 3001.0], [6.1, 3001.0], [6.2, 3001.0], [6.3, 3001.0], [6.4, 3001.0], [6.5, 3001.0], [6.6, 3001.0], [6.7, 3001.0], [6.8, 3001.0], [6.9, 3001.0], [7.0, 3001.0], [7.1, 3001.0], [7.2, 3001.0], [7.3, 3001.0], [7.4, 3001.0], [7.5, 3001.0], [7.6, 3001.0], [7.7, 3001.0], [7.8, 3001.0], [7.9, 3001.0], [8.0, 3002.0], [8.1, 3002.0], [8.2, 3002.0], [8.3, 3002.0], [8.4, 3002.0], [8.5, 3002.0], [8.6, 3002.0], [8.7, 3002.0], [8.8, 3002.0], [8.9, 3002.0], [9.0, 3002.0], [9.1, 3002.0], [9.2, 3002.0], [9.3, 3002.0], [9.4, 3002.0], [9.5, 3002.0], [9.6, 3002.0], [9.7, 3002.0], [9.8, 3002.0], [9.9, 3002.0], [10.0, 3002.0], [10.1, 3002.0], [10.2, 3002.0], [10.3, 3002.0], [10.4, 3002.0], [10.5, 3002.0], [10.6, 3002.0], [10.7, 3002.0], [10.8, 3002.0], [10.9, 3002.0], [11.0, 3002.0], [11.1, 3002.0], [11.2, 3002.0], [11.3, 3002.0], [11.4, 3002.0], [11.5, 3002.0], [11.6, 3002.0], [11.7, 3002.0], [11.8, 3002.0], [11.9, 3002.0], [12.0, 3003.0], [12.1, 3003.0], [12.2, 3003.0], [12.3, 3003.0], [12.4, 3003.0], [12.5, 3003.0], [12.6, 3003.0], [12.7, 3003.0], [12.8, 3003.0], [12.9, 3003.0], [13.0, 3003.0], [13.1, 3003.0], [13.2, 3003.0], [13.3, 3003.0], [13.4, 3003.0], [13.5, 3003.0], [13.6, 3003.0], [13.7, 3003.0], [13.8, 3003.0], [13.9, 3003.0], [14.0, 3003.0], [14.1, 3003.0], [14.2, 3003.0], [14.3, 3003.0], [14.4, 3003.0], [14.5, 3003.0], [14.6, 3003.0], [14.7, 3003.0], [14.8, 3003.0], [14.9, 3003.0], [15.0, 3003.0], [15.1, 3003.0], [15.2, 3003.0], [15.3, 3003.0], [15.4, 3003.0], [15.5, 3003.0], [15.6, 3003.0], [15.7, 3003.0], [15.8, 3003.0], [15.9, 3003.0], [16.0, 3003.0], [16.1, 3003.0], [16.2, 3003.0], [16.3, 3004.0], [16.4, 3004.0], [16.5, 3004.0], [16.6, 3004.0], [16.7, 3004.0], [16.8, 3004.0], [16.9, 3004.0], [17.0, 3004.0], [17.1, 3004.0], [17.2, 3004.0], [17.3, 3004.0], [17.4, 3004.0], [17.5, 3004.0], [17.6, 3004.0], [17.7, 3004.0], [17.8, 3004.0], [17.9, 3004.0], [18.0, 3004.0], [18.1, 3004.0], [18.2, 3004.0], [18.3, 3004.0], [18.4, 3004.0], [18.5, 3004.0], [18.6, 3004.0], [18.7, 3004.0], [18.8, 3004.0], [18.9, 3004.0], [19.0, 3004.0], [19.1, 3004.0], [19.2, 3004.0], [19.3, 3004.0], [19.4, 3004.0], [19.5, 3004.0], [19.6, 3004.0], [19.7, 3004.0], [19.8, 3004.0], [19.9, 3004.0], [20.0, 3004.0], [20.1, 3004.0], [20.2, 3004.0], [20.3, 3004.0], [20.4, 3005.0], [20.5, 3005.0], [20.6, 3005.0], [20.7, 3005.0], [20.8, 3005.0], [20.9, 3005.0], [21.0, 3005.0], [21.1, 3005.0], [21.2, 3005.0], [21.3, 3005.0], [21.4, 3005.0], [21.5, 3005.0], [21.6, 3005.0], [21.7, 3005.0], [21.8, 3005.0], [21.9, 3005.0], [22.0, 3005.0], [22.1, 3005.0], [22.2, 3005.0], [22.3, 3005.0], [22.4, 3005.0], [22.5, 3005.0], [22.6, 3005.0], [22.7, 3005.0], [22.8, 3005.0], [22.9, 3005.0], [23.0, 3005.0], [23.1, 3005.0], [23.2, 3005.0], [23.3, 3005.0], [23.4, 3005.0], [23.5, 3005.0], [23.6, 3005.0], [23.7, 3005.0], [23.8, 3005.0], [23.9, 3005.0], [24.0, 3005.0], [24.1, 3005.0], [24.2, 3006.0], [24.3, 3006.0], [24.4, 3006.0], [24.5, 3006.0], [24.6, 3006.0], [24.7, 3006.0], [24.8, 3006.0], [24.9, 3006.0], [25.0, 3006.0], [25.1, 3006.0], [25.2, 3006.0], [25.3, 3006.0], [25.4, 3006.0], [25.5, 3006.0], [25.6, 3006.0], [25.7, 3006.0], [25.8, 3006.0], [25.9, 3006.0], [26.0, 3006.0], [26.1, 3006.0], [26.2, 3006.0], [26.3, 3006.0], [26.4, 3006.0], [26.5, 3006.0], [26.6, 3006.0], [26.7, 3006.0], [26.8, 3006.0], [26.9, 3006.0], [27.0, 3006.0], [27.1, 3006.0], [27.2, 3006.0], [27.3, 3006.0], [27.4, 3006.0], [27.5, 3006.0], [27.6, 3006.0], [27.7, 3006.0], [27.8, 3006.0], [27.9, 3006.0], [28.0, 3006.0], [28.1, 3006.0], [28.2, 3006.0], [28.3, 3007.0], [28.4, 3007.0], [28.5, 3007.0], [28.6, 3007.0], [28.7, 3007.0], [28.8, 3007.0], [28.9, 3007.0], [29.0, 3007.0], [29.1, 3007.0], [29.2, 3007.0], [29.3, 3007.0], [29.4, 3007.0], [29.5, 3007.0], [29.6, 3007.0], [29.7, 3007.0], [29.8, 3007.0], [29.9, 3007.0], [30.0, 3007.0], [30.1, 3007.0], [30.2, 3007.0], [30.3, 3007.0], [30.4, 3007.0], [30.5, 3007.0], [30.6, 3007.0], [30.7, 3007.0], [30.8, 3007.0], [30.9, 3007.0], [31.0, 3007.0], [31.1, 3007.0], [31.2, 3007.0], [31.3, 3007.0], [31.4, 3007.0], [31.5, 3007.0], [31.6, 3007.0], [31.7, 3007.0], [31.8, 3007.0], [31.9, 3007.0], [32.0, 3007.0], [32.1, 3008.0], [32.2, 3008.0], [32.3, 3008.0], [32.4, 3008.0], [32.5, 3008.0], [32.6, 3008.0], [32.7, 3008.0], [32.8, 3008.0], [32.9, 3008.0], [33.0, 3008.0], [33.1, 3008.0], [33.2, 3008.0], [33.3, 3008.0], [33.4, 3008.0], [33.5, 3008.0], [33.6, 3008.0], [33.7, 3008.0], [33.8, 3008.0], [33.9, 3008.0], [34.0, 3008.0], [34.1, 3008.0], [34.2, 3008.0], [34.3, 3008.0], [34.4, 3008.0], [34.5, 3008.0], [34.6, 3008.0], [34.7, 3008.0], [34.8, 3008.0], [34.9, 3008.0], [35.0, 3008.0], [35.1, 3008.0], [35.2, 3008.0], [35.3, 3008.0], [35.4, 3008.0], [35.5, 3008.0], [35.6, 3008.0], [35.7, 3008.0], [35.8, 3009.0], [35.9, 3009.0], [36.0, 3009.0], [36.1, 3009.0], [36.2, 3009.0], [36.3, 3009.0], [36.4, 3009.0], [36.5, 3009.0], [36.6, 3009.0], [36.7, 3009.0], [36.8, 3009.0], [36.9, 3009.0], [37.0, 3009.0], [37.1, 3009.0], [37.2, 3009.0], [37.3, 3009.0], [37.4, 3009.0], [37.5, 3009.0], [37.6, 3009.0], [37.7, 3009.0], [37.8, 3009.0], [37.9, 3009.0], [38.0, 3009.0], [38.1, 3009.0], [38.2, 3009.0], [38.3, 3009.0], [38.4, 3009.0], [38.5, 3009.0], [38.6, 3009.0], [38.7, 3009.0], [38.8, 3009.0], [38.9, 3009.0], [39.0, 3009.0], [39.1, 3009.0], [39.2, 3009.0], [39.3, 3009.0], [39.4, 3009.0], [39.5, 3009.0], [39.6, 3010.0], [39.7, 3010.0], [39.8, 3010.0], [39.9, 3010.0], [40.0, 3010.0], [40.1, 3010.0], [40.2, 3010.0], [40.3, 3010.0], [40.4, 3010.0], [40.5, 3010.0], [40.6, 3010.0], [40.7, 3010.0], [40.8, 3010.0], [40.9, 3010.0], [41.0, 3010.0], [41.1, 3010.0], [41.2, 3010.0], [41.3, 3010.0], [41.4, 3010.0], [41.5, 3010.0], [41.6, 3010.0], [41.7, 3010.0], [41.8, 3010.0], [41.9, 3010.0], [42.0, 3010.0], [42.1, 3010.0], [42.2, 3010.0], [42.3, 3010.0], [42.4, 3010.0], [42.5, 3010.0], [42.6, 3010.0], [42.7, 3010.0], [42.8, 3010.0], [42.9, 3010.0], [43.0, 3010.0], [43.1, 3010.0], [43.2, 3010.0], [43.3, 3010.0], [43.4, 3011.0], [43.5, 3011.0], [43.6, 3011.0], [43.7, 3011.0], [43.8, 3011.0], [43.9, 3011.0], [44.0, 3011.0], [44.1, 3011.0], [44.2, 3011.0], [44.3, 3011.0], [44.4, 3011.0], [44.5, 3011.0], [44.6, 3011.0], [44.7, 3011.0], [44.8, 3011.0], [44.9, 3011.0], [45.0, 3011.0], [45.1, 3011.0], [45.2, 3011.0], [45.3, 3011.0], [45.4, 3011.0], [45.5, 3011.0], [45.6, 3011.0], [45.7, 3011.0], [45.8, 3011.0], [45.9, 3011.0], [46.0, 3011.0], [46.1, 3011.0], [46.2, 3011.0], [46.3, 3011.0], [46.4, 3011.0], [46.5, 3011.0], [46.6, 3011.0], [46.7, 3011.0], [46.8, 3011.0], [46.9, 3011.0], [47.0, 3011.0], [47.1, 3011.0], [47.2, 3011.0], [47.3, 3011.0], [47.4, 3012.0], [47.5, 3012.0], [47.6, 3012.0], [47.7, 3012.0], [47.8, 3012.0], [47.9, 3012.0], [48.0, 3012.0], [48.1, 3012.0], [48.2, 3012.0], [48.3, 3012.0], [48.4, 3012.0], [48.5, 3012.0], [48.6, 3012.0], [48.7, 3012.0], [48.8, 3012.0], [48.9, 3012.0], [49.0, 3012.0], [49.1, 3012.0], [49.2, 3012.0], [49.3, 3012.0], [49.4, 3012.0], [49.5, 3012.0], [49.6, 3012.0], [49.7, 3012.0], [49.8, 3012.0], [49.9, 3012.0], [50.0, 3012.0], [50.1, 3012.0], [50.2, 3012.0], [50.3, 3012.0], [50.4, 3012.0], [50.5, 3012.0], [50.6, 3012.0], [50.7, 3012.0], [50.8, 3012.0], [50.9, 3012.0], [51.0, 3012.0], [51.1, 3012.0], [51.2, 3013.0], [51.3, 3013.0], [51.4, 3013.0], [51.5, 3013.0], [51.6, 3013.0], [51.7, 3013.0], [51.8, 3013.0], [51.9, 3013.0], [52.0, 3013.0], [52.1, 3013.0], [52.2, 3013.0], [52.3, 3013.0], [52.4, 3013.0], [52.5, 3013.0], [52.6, 3013.0], [52.7, 3013.0], [52.8, 3013.0], [52.9, 3013.0], [53.0, 3013.0], [53.1, 3013.0], [53.2, 3013.0], [53.3, 3013.0], [53.4, 3013.0], [53.5, 3013.0], [53.6, 3013.0], [53.7, 3013.0], [53.8, 3013.0], [53.9, 3013.0], [54.0, 3013.0], [54.1, 3013.0], [54.2, 3013.0], [54.3, 3013.0], [54.4, 3013.0], [54.5, 3013.0], [54.6, 3013.0], [54.7, 3013.0], [54.8, 3013.0], [54.9, 3013.0], [55.0, 3014.0], [55.1, 3014.0], [55.2, 3014.0], [55.3, 3014.0], [55.4, 3014.0], [55.5, 3014.0], [55.6, 3014.0], [55.7, 3014.0], [55.8, 3014.0], [55.9, 3014.0], [56.0, 3014.0], [56.1, 3014.0], [56.2, 3014.0], [56.3, 3014.0], [56.4, 3014.0], [56.5, 3014.0], [56.6, 3014.0], [56.7, 3014.0], [56.8, 3014.0], [56.9, 3014.0], [57.0, 3014.0], [57.1, 3014.0], [57.2, 3014.0], [57.3, 3014.0], [57.4, 3014.0], [57.5, 3014.0], [57.6, 3014.0], [57.7, 3014.0], [57.8, 3014.0], [57.9, 3014.0], [58.0, 3014.0], [58.1, 3014.0], [58.2, 3014.0], [58.3, 3014.0], [58.4, 3014.0], [58.5, 3014.0], [58.6, 3015.0], [58.7, 3015.0], [58.8, 3015.0], [58.9, 3015.0], [59.0, 3015.0], [59.1, 3015.0], [59.2, 3015.0], [59.3, 3015.0], [59.4, 3015.0], [59.5, 3015.0], [59.6, 3015.0], [59.7, 3015.0], [59.8, 3015.0], [59.9, 3015.0], [60.0, 3015.0], [60.1, 3015.0], [60.2, 3015.0], [60.3, 3015.0], [60.4, 3015.0], [60.5, 3015.0], [60.6, 3015.0], [60.7, 3015.0], [60.8, 3015.0], [60.9, 3015.0], [61.0, 3015.0], [61.1, 3015.0], [61.2, 3015.0], [61.3, 3015.0], [61.4, 3015.0], [61.5, 3015.0], [61.6, 3015.0], [61.7, 3015.0], [61.8, 3016.0], [61.9, 3016.0], [62.0, 3016.0], [62.1, 3016.0], [62.2, 3016.0], [62.3, 3016.0], [62.4, 3016.0], [62.5, 3016.0], [62.6, 3016.0], [62.7, 3016.0], [62.8, 3016.0], [62.9, 3016.0], [63.0, 3016.0], [63.1, 3016.0], [63.2, 3016.0], [63.3, 3016.0], [63.4, 3016.0], [63.5, 3016.0], [63.6, 3016.0], [63.7, 3016.0], [63.8, 3016.0], [63.9, 3016.0], [64.0, 3016.0], [64.1, 3016.0], [64.2, 3016.0], [64.3, 3016.0], [64.4, 3016.0], [64.5, 3017.0], [64.6, 3017.0], [64.7, 3017.0], [64.8, 3017.0], [64.9, 3017.0], [65.0, 3017.0], [65.1, 3017.0], [65.2, 3017.0], [65.3, 3017.0], [65.4, 3017.0], [65.5, 3017.0], [65.6, 3017.0], [65.7, 3017.0], [65.8, 3017.0], [65.9, 3017.0], [66.0, 3017.0], [66.1, 3017.0], [66.2, 3017.0], [66.3, 3017.0], [66.4, 3017.0], [66.5, 3018.0], [66.6, 3018.0], [66.7, 3018.0], [66.8, 3018.0], [66.9, 3018.0], [67.0, 3018.0], [67.1, 3018.0], [67.2, 3018.0], [67.3, 3018.0], [67.4, 3018.0], [67.5, 3018.0], [67.6, 3018.0], [67.7, 3018.0], [67.8, 3018.0], [67.9, 3019.0], [68.0, 3019.0], [68.1, 3019.0], [68.2, 3019.0], [68.3, 3019.0], [68.4, 3019.0], [68.5, 3019.0], [68.6, 3019.0], [68.7, 3019.0], [68.8, 3020.0], [68.9, 3020.0], [69.0, 3020.0], [69.1, 3020.0], [69.2, 3020.0], [69.3, 3020.0], [69.4, 3020.0], [69.5, 3021.0], [69.6, 3021.0], [69.7, 3021.0], [69.8, 3021.0], [69.9, 3021.0], [70.0, 3022.0], [70.1, 3022.0], [70.2, 3022.0], [70.3, 3023.0], [70.4, 3023.0], [70.5, 3023.0], [70.6, 3024.0], [70.7, 3024.0], [70.8, 3025.0], [70.9, 3025.0], [71.0, 3026.0], [71.1, 3026.0], [71.2, 3027.0], [71.3, 3028.0], [71.4, 3029.0], [71.5, 3030.0], [71.6, 3031.0], [71.7, 3033.0], [71.8, 3035.0], [71.9, 3037.0], [72.0, 3040.0], [72.1, 3049.0], [72.2, 3336.0], [72.3, 3484.0], [72.4, 3730.0], [72.5, 3929.0], [72.6, 4005.0], [72.7, 4134.0], [72.8, 4161.0], [72.9, 4177.0], [73.0, 4183.0], [73.1, 4200.0], [73.2, 4216.0], [73.3, 4222.0], [73.4, 4225.0], [73.5, 4228.0], [73.6, 4231.0], [73.7, 4235.0], [73.8, 4241.0], [73.9, 4245.0], [74.0, 4250.0], [74.1, 4253.0], [74.2, 4255.0], [74.3, 4258.0], [74.4, 4260.0], [74.5, 4266.0], [74.6, 4270.0], [74.7, 4272.0], [74.8, 4274.0], [74.9, 4276.0], [75.0, 4278.0], [75.1, 4280.0], [75.2, 4283.0], [75.3, 4285.0], [75.4, 4287.0], [75.5, 4288.0], [75.6, 4290.0], [75.7, 4291.0], [75.8, 4294.0], [75.9, 4296.0], [76.0, 4297.0], [76.1, 4299.0], [76.2, 4301.0], [76.3, 4302.0], [76.4, 4304.0], [76.5, 4307.0], [76.6, 4309.0], [76.7, 4311.0], [76.8, 4314.0], [76.9, 4316.0], [77.0, 4318.0], [77.1, 4319.0], [77.2, 4321.0], [77.3, 4323.0], [77.4, 4326.0], [77.5, 4329.0], [77.6, 4331.0], [77.7, 4334.0], [77.8, 4336.0], [77.9, 4339.0], [78.0, 4341.0], [78.1, 4343.0], [78.2, 4345.0], [78.3, 4347.0], [78.4, 4350.0], [78.5, 4352.0], [78.6, 4355.0], [78.7, 4359.0], [78.8, 4362.0], [78.9, 4366.0], [79.0, 4369.0], [79.1, 4373.0], [79.2, 4375.0], [79.3, 4377.0], [79.4, 4380.0], [79.5, 4382.0], [79.6, 4385.0], [79.7, 4386.0], [79.8, 4389.0], [79.9, 4392.0], [80.0, 4395.0], [80.1, 4398.0], [80.2, 4400.0], [80.3, 4403.0], [80.4, 4405.0], [80.5, 4408.0], [80.6, 4410.0], [80.7, 4412.0], [80.8, 4414.0], [80.9, 4416.0], [81.0, 4418.0], [81.1, 4420.0], [81.2, 4423.0], [81.3, 4425.0], [81.4, 4430.0], [81.5, 4432.0], [81.6, 4435.0], [81.7, 4438.0], [81.8, 4440.0], [81.9, 4443.0], [82.0, 4445.0], [82.1, 4447.0], [82.2, 4450.0], [82.3, 4453.0], [82.4, 4456.0], [82.5, 4460.0], [82.6, 4467.0], [82.7, 4473.0], [82.8, 4481.0], [82.9, 4493.0], [83.0, 4501.0], [83.1, 4504.0], [83.2, 4507.0], [83.3, 4509.0], [83.4, 4511.0], [83.5, 4513.0], [83.6, 4515.0], [83.7, 4517.0], [83.8, 4518.0], [83.9, 4519.0], [84.0, 4521.0], [84.1, 4523.0], [84.2, 4524.0], [84.3, 4526.0], [84.4, 4528.0], [84.5, 4530.0], [84.6, 4532.0], [84.7, 4533.0], [84.8, 4536.0], [84.9, 4540.0], [85.0, 4543.0], [85.1, 4546.0], [85.2, 4549.0], [85.3, 4553.0], [85.4, 4560.0], [85.5, 4563.0], [85.6, 4567.0], [85.7, 4574.0], [85.8, 4577.0], [85.9, 4581.0], [86.0, 4583.0], [86.1, 4586.0], [86.2, 4588.0], [86.3, 4590.0], [86.4, 4593.0], [86.5, 4596.0], [86.6, 4600.0], [86.7, 4604.0], [86.8, 4606.0], [86.9, 4612.0], [87.0, 4621.0], [87.1, 4626.0], [87.2, 4630.0], [87.3, 4634.0], [87.4, 4638.0], [87.5, 4641.0], [87.6, 4643.0], [87.7, 4645.0], [87.8, 4649.0], [87.9, 4653.0], [88.0, 4657.0], [88.1, 4661.0], [88.2, 4666.0], [88.3, 4669.0], [88.4, 4672.0], [88.5, 4673.0], [88.6, 4679.0], [88.7, 4683.0], [88.8, 4687.0], [88.9, 4692.0], [89.0, 4697.0], [89.1, 4701.0], [89.2, 4704.0], [89.3, 4708.0], [89.4, 4711.0], [89.5, 4713.0], [89.6, 4716.0], [89.7, 4719.0], [89.8, 4721.0], [89.9, 4723.0], [90.0, 4724.0], [90.1, 4727.0], [90.2, 4730.0], [90.3, 4737.0], [90.4, 4741.0], [90.5, 4744.0], [90.6, 4748.0], [90.7, 4752.0], [90.8, 4757.0], [90.9, 4762.0], [91.0, 4764.0], [91.1, 4766.0], [91.2, 4767.0], [91.3, 4770.0], [91.4, 4771.0], [91.5, 4773.0], [91.6, 4775.0], [91.7, 4778.0], [91.8, 4785.0], [91.9, 4788.0], [92.0, 4790.0], [92.1, 4793.0], [92.2, 4795.0], [92.3, 4797.0], [92.4, 4799.0], [92.5, 4800.0], [92.6, 4801.0], [92.7, 4802.0], [92.8, 4804.0], [92.9, 4805.0], [93.0, 4808.0], [93.1, 4810.0], [93.2, 4813.0], [93.3, 4816.0], [93.4, 4819.0], [93.5, 4823.0], [93.6, 4827.0], [93.7, 4831.0], [93.8, 4833.0], [93.9, 4836.0], [94.0, 4837.0], [94.1, 4839.0], [94.2, 4841.0], [94.3, 4842.0], [94.4, 4844.0], [94.5, 4846.0], [94.6, 4847.0], [94.7, 4849.0], [94.8, 4852.0], [94.9, 4855.0], [95.0, 4857.0], [95.1, 4859.0], [95.2, 4861.0], [95.3, 4864.0], [95.4, 4866.0], [95.5, 4869.0], [95.6, 4871.0], [95.7, 4873.0], [95.8, 4877.0], [95.9, 4881.0], [96.0, 4885.0], [96.1, 4890.0], [96.2, 4896.0], [96.3, 4903.0], [96.4, 4907.0], [96.5, 4914.0], [96.6, 4922.0], [96.7, 4925.0], [96.8, 4927.0], [96.9, 4930.0], [97.0, 4935.0], [97.1, 4939.0], [97.2, 4943.0], [97.3, 4950.0], [97.4, 4988.0], [97.5, 5202.0], [97.6, 5526.0], [97.7, 5790.0], [97.8, 6112.0], [97.9, 6481.0], [98.0, 6808.0], [98.1, 7006.0], [98.2, 7321.0], [98.3, 7629.0], [98.4, 10534.0], [98.5, 13726.0], [98.6, 20682.0], [98.7, 29477.0], [98.8, 40691.0], [98.9, 54552.0], [99.0, 67853.0], [99.1, 81703.0], [99.2, 88471.0], [99.3, 103961.0], [99.4, 110504.0], [99.5, 125978.0], [99.6, 137184.0], [99.7, 152596.0], [99.8, 164263.0], [99.9, 175276.0], [100.0, 185081.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 2900.0, "maxY": 36347.0, "series": [{"data": [[140000.0, 1.0], [165600.0, 2.0], [176800.0, 9.0], [178400.0, 2.0], [175200.0, 5.0], [160100.0, 5.0], [169700.0, 2.0], [171300.0, 3.0], [176100.0, 6.0], [69100.0, 1.0], [85100.0, 22.0], [85900.0, 14.0], [97100.0, 5.0], [98700.0, 2.0], [100300.0, 1.0], [102700.0, 1.0], [113900.0, 9.0], [125900.0, 5.0], [128300.0, 1.0], [132200.0, 5.0], [146600.0, 4.0], [164200.0, 2.0], [177000.0, 1.0], [185000.0, 2.0], [33700.0, 1.0], [35300.0, 8.0], [36500.0, 1.0], [141100.0, 8.0], [37300.0, 14.0], [40500.0, 8.0], [44100.0, 5.0], [177900.0, 4.0], [179500.0, 1.0], [46500.0, 2.0], [182700.0, 1.0], [48900.0, 1.0], [51700.0, 1.0], [54500.0, 9.0], [58500.0, 9.0], [59300.0, 3.0], [61300.0, 8.0], [62900.0, 2.0], [67400.0, 2.0], [72200.0, 7.0], [75400.0, 2.0], [86600.0, 6.0], [101800.0, 2.0], [108200.0, 1.0], [109000.0, 6.0], [121800.0, 1.0], [120200.0, 2.0], [123400.0, 6.0], [134000.0, 1.0], [150000.0, 1.0], [164400.0, 1.0], [170800.0, 1.0], [178800.0, 10.0], [182000.0, 1.0], [144500.0, 2.0], [152500.0, 5.0], [163700.0, 2.0], [2900.0, 1425.0], [184500.0, 1.0], [3000.0, 36347.0], [3100.0, 1.0], [3200.0, 1.0], [3300.0, 46.0], [3400.0, 24.0], [3500.0, 25.0], [3600.0, 20.0], [3700.0, 23.0], [3800.0, 18.0], [3900.0, 63.0], [4000.0, 44.0], [4100.0, 221.0], [4200.0, 1591.0], [4300.0, 2125.0], [4500.0, 1891.0], [4600.0, 1288.0], [4400.0, 1453.0], [4700.0, 1786.0], [4800.0, 1991.0], [75300.0, 1.0], [4900.0, 596.0], [5000.0, 16.0], [5100.0, 34.0], [81700.0, 9.0], [5200.0, 11.0], [5300.0, 15.0], [83300.0, 1.0], [5500.0, 37.0], [5400.0, 16.0], [5600.0, 12.0], [86500.0, 1.0], [5700.0, 17.0], [5800.0, 22.0], [93700.0, 3.0], [5900.0, 14.0], [6000.0, 12.0], [6100.0, 13.0], [6200.0, 17.0], [6300.0, 14.0], [6600.0, 19.0], [6400.0, 12.0], [6500.0, 15.0], [6700.0, 15.0], [6800.0, 33.0], [6900.0, 17.0], [109700.0, 13.0], [110500.0, 20.0], [7000.0, 15.0], [7100.0, 17.0], [7200.0, 18.0], [7300.0, 17.0], [7400.0, 16.0], [7500.0, 19.0], [7600.0, 17.0], [7700.0, 3.0], [7800.0, 4.0], [7900.0, 3.0], [8000.0, 2.0], [8300.0, 1.0], [8400.0, 3.0], [8600.0, 1.0], [9000.0, 3.0], [9100.0, 1.0], [9300.0, 3.0], [9700.0, 1.0], [153400.0, 1.0], [155000.0, 10.0], [10000.0, 9.0], [10200.0, 2.0], [10500.0, 12.0], [10600.0, 10.0], [10700.0, 1.0], [166200.0, 3.0], [10400.0, 3.0], [11000.0, 1.0], [11500.0, 6.0], [183800.0, 1.0], [12100.0, 2.0], [12500.0, 6.0], [12600.0, 7.0], [12800.0, 1.0], [13200.0, 1.0], [13100.0, 2.0], [13400.0, 3.0], [13700.0, 4.0], [13600.0, 1.0], [14200.0, 2.0], [14500.0, 4.0], [14600.0, 6.0], [14900.0, 3.0], [15500.0, 1.0], [16200.0, 1.0], [16300.0, 6.0], [16600.0, 1.0], [17400.0, 1.0], [18200.0, 1.0], [18800.0, 4.0], [19400.0, 1.0], [19600.0, 1.0], [20600.0, 5.0], [22200.0, 8.0], [23400.0, 1.0], [24400.0, 4.0], [25200.0, 1.0], [25400.0, 2.0], [25600.0, 1.0], [26600.0, 3.0], [27400.0, 16.0], [28400.0, 4.0], [29400.0, 4.0], [30600.0, 3.0], [31400.0, 1.0], [32400.0, 1.0], [31800.0, 6.0], [38400.0, 1.0], [42000.0, 1.0], [163900.0, 5.0], [170300.0, 2.0], [178300.0, 1.0], [173500.0, 7.0], [45600.0, 2.0], [46400.0, 9.0], [49200.0, 1.0], [52800.0, 1.0], [55600.0, 1.0], [63200.0, 5.0], [64400.0, 8.0], [69600.0, 4.0], [70400.0, 3.0], [73600.0, 1.0], [78400.0, 2.0], [95200.0, 2.0], [96800.0, 1.0], [98400.0, 4.0], [109600.0, 2.0], [110400.0, 1.0], [131200.0, 7.0], [161600.0, 1.0], [163200.0, 1.0], [158400.0, 1.0], [177600.0, 1.0], [180800.0, 1.0], [146500.0, 1.0], [140100.0, 1.0], [164100.0, 3.0], [167300.0, 3.0], [180100.0, 1.0], [176900.0, 3.0], [73500.0, 7.0], [103900.0, 5.0], [107100.0, 3.0], [119100.0, 12.0], [136200.0, 4.0], [165000.0, 3.0], [176200.0, 1.0], [34300.0, 1.0], [132300.0, 1.0], [137100.0, 7.0], [151500.0, 4.0], [164300.0, 5.0], [165900.0, 1.0], [169100.0, 4.0], [43100.0, 6.0], [47900.0, 4.0], [50300.0, 1.0], [51100.0, 3.0], [59900.0, 1.0], [67800.0, 10.0], [76600.0, 2.0], [90200.0, 10.0], [95000.0, 6.0], [100600.0, 1.0], [138000.0, 1.0], [144400.0, 6.0], [149200.0, 10.0], [160400.0, 7.0], [173200.0, 1.0], [159700.0, 1.0], [164500.0, 1.0], [172500.0, 6.0], [177300.0, 1.0], [174100.0, 3.0], [76500.0, 11.0], [100500.0, 6.0], [106100.0, 1.0], [105300.0, 9.0], [115700.0, 1.0], [119700.0, 1.0], [122100.0, 7.0], [127700.0, 11.0], [138200.0, 1.0], [147800.0, 5.0], [155800.0, 2.0], [178200.0, 2.0], [17300.0, 1.0], [18100.0, 1.0], [19700.0, 8.0], [20500.0, 6.0], [21500.0, 4.0], [22500.0, 1.0], [27700.0, 1.0], [30700.0, 1.0], [32500.0, 1.0], [134300.0, 7.0], [132700.0, 6.0], [143900.0, 1.0], [148700.0, 1.0], [39800.0, 3.0], [40600.0, 2.0], [158300.0, 12.0], [179100.0, 1.0], [47800.0, 2.0], [49000.0, 3.0], [51000.0, 4.0], [51800.0, 1.0], [57400.0, 2.0], [63000.0, 5.0], [66000.0, 1.0], [79600.0, 1.0], [88400.0, 4.0], [91600.0, 5.0], [117200.0, 1.0], [125200.0, 2.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 185000.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 14448.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 37890.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 37890.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 14448.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 1000.0, "minX": 1.6765449E12, "maxY": 1000.0, "series": [{"data": [[1.67654502E12, 1000.0], [1.67654514E12, 1000.0], [1.67654496E12, 1000.0], [1.67654508E12, 1000.0], [1.6765449E12, 1000.0]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67654514E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 4850.59809316363, "minX": 1000.0, "maxY": 4850.59809316363, "series": [{"data": [[1000.0, 4850.59809316363]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[1000.0, 4850.59809316363]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 1000.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 8286.716666666667, "minX": 1.6765449E12, "maxY": 921636.3333333334, "series": [{"data": [[1.67654502E12, 67472.0], [1.67654514E12, 921636.3333333334], [1.67654496E12, 68732.33333333333], [1.67654508E12, 226741.83333333334], [1.6765449E12, 63943.0]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.67654502E12, 28796.166666666668], [1.67654514E12, 26481.816666666666], [1.67654496E12, 27186.083333333332], [1.67654508E12, 30498.916666666668], [1.6765449E12, 8286.716666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67654514E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 3785.983042181517, "minX": 1.6765449E12, "maxY": 7111.156323585264, "series": [{"data": [[1.67654502E12, 4022.8428801287137], [1.67654514E12, 4552.0031493307715], [1.67654496E12, 3785.983042181517], [1.67654508E12, 7111.156323585264], [1.6765449E12, 3854.0212468549075]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67654514E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 3785.9826161056617, "minX": 1.6765449E12, "maxY": 7111.155639954446, "series": [{"data": [[1.67654502E12, 4022.8425583266167], [1.67654514E12, 4551.999475111539], [1.67654496E12, 3785.9826161056617], [1.67654508E12, 7111.155639954446], [1.6765449E12, 3854.009505171926]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67654514E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.01924591024407317, "minX": 1.6765449E12, "maxY": 0.7525859658932051, "series": [{"data": [[1.67654502E12, 0.0343523732904265], [1.67654514E12, 0.01924591024407317], [1.67654496E12, 0.03544951001278214], [1.67654508E12, 0.027117356627421128], [1.6765449E12, 0.7525859658932051]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67654514E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 2989.0, "minX": 1.6765449E12, "maxY": 4810.0, "series": [{"data": [[1.67654502E12, 3054.0], [1.67654514E12, 3063.0], [1.67654496E12, 3348.0], [1.67654508E12, 3488.0], [1.6765449E12, 4810.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.67654502E12, 3018.0], [1.67654514E12, 3020.3], [1.67654496E12, 3017.0], [1.67654508E12, 3016.0], [1.6765449E12, 3015.0]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.67654502E12, 3032.0], [1.67654514E12, 3063.0], [1.67654496E12, 3031.0], [1.67654508E12, 3030.0], [1.6765449E12, 4618.279999999999]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.67654502E12, 3020.0], [1.67654514E12, 3025.5], [1.67654496E12, 3020.0], [1.67654508E12, 3019.0], [1.6765449E12, 3017.0]], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [[1.67654502E12, 2992.0], [1.67654514E12, 3000.0], [1.67654496E12, 2989.0], [1.67654508E12, 2990.0], [1.6765449E12, 2990.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.67654502E12, 3008.0], [1.67654514E12, 3008.0], [1.67654496E12, 3009.0], [1.67654508E12, 3009.0], [1.6765449E12, 3008.0]], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67654514E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 3001.0, "minX": 15.0, "maxY": 178855.0, "series": [{"data": [[16.0, 4583.5], [139.0, 4702.5], [147.0, 3006.0], [148.0, 3003.0], [151.0, 3006.0], [149.0, 3012.0], [145.0, 3001.0], [150.0, 3004.0], [146.0, 3007.0], [154.0, 3012.0], [157.0, 3006.0], [156.0, 3010.0], [153.0, 3010.0], [155.0, 3008.0], [152.0, 3005.0], [159.0, 3005.0], [158.0, 3006.0], [161.0, 3006.0], [162.0, 3005.0], [160.0, 3007.0], [163.0, 3007.0], [164.0, 3008.0], [167.0, 3004.0], [166.0, 3011.0], [165.0, 3007.0], [169.0, 3010.0], [168.0, 3009.0], [173.0, 3006.0], [172.0, 3015.0], [170.0, 3008.0], [171.0, 3010.0], [175.0, 3009.0], [180.0, 3009.0], [181.0, 3012.0], [182.0, 3012.0], [176.0, 3011.0], [184.0, 3006.0], [189.0, 3012.0], [185.0, 3009.0], [194.0, 3009.0], [195.0, 3005.0], [196.0, 3002.0], [197.0, 3011.0], [198.0, 3006.0], [199.0, 3012.0], [193.0, 3001.0], [206.0, 3007.0], [205.0, 3011.0], [202.0, 3010.0], [203.0, 3013.0], [204.0, 3008.0], [200.0, 3015.0], [201.0, 3017.0], [207.0, 3005.0], [212.0, 3003.0], [208.0, 3015.5], [216.0, 3010.0], [218.0, 3009.0], [219.0, 3013.0], [230.0, 3011.0], [228.0, 3002.0], [234.0, 3001.0], [239.0, 3003.0], [236.0, 3007.0], [238.0, 3016.5], [235.0, 3021.0], [244.0, 3010.0], [246.0, 3010.0], [240.0, 3006.0], [250.0, 3010.5], [254.0, 3010.0], [249.0, 3011.0], [252.0, 3009.0], [253.0, 3005.0], [259.0, 3009.0], [262.0, 3012.0], [260.0, 3006.0], [261.0, 3004.0], [263.0, 3001.0], [258.0, 3010.0], [256.0, 3015.0], [269.0, 3006.0], [268.0, 3009.0], [270.0, 3006.0], [271.0, 3013.0], [265.0, 3009.0], [266.0, 3007.0], [267.0, 3012.0], [273.0, 3012.0], [275.0, 3014.0], [274.0, 3013.0], [272.0, 3009.0], [277.0, 3015.0], [278.0, 3010.0], [279.0, 3003.0], [284.0, 3003.0], [285.0, 3010.0], [286.0, 3013.0], [281.0, 3008.5], [480.0, 3008.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[15.0, 4670.0], [139.0, 5141.0], [143.0, 5830.0], [147.0, 6778.5], [148.0, 8462.0], [151.0, 9365.0], [149.0, 11559.0], [145.0, 27750.0], [150.0, 51790.0], [146.0, 45680.5], [154.0, 70806.5], [157.0, 14907.0], [156.0, 20522.0], [153.0, 18833.0], [155.0, 72285.0], [152.0, 54555.0], [159.0, 63001.0], [158.0, 132297.5], [161.0, 32503.0], [162.0, 35331.0], [160.0, 144483.0], [163.0, 44185.5], [164.0, 125978.0], [166.0, 159628.0], [168.0, 50322.0], [171.0, 178855.0], [169.0, 164202.0], [173.0, 4786.0], [181.0, 4534.0], [182.0, 4511.0], [176.0, 170832.0], [184.0, 103963.0], [189.0, 109784.0], [185.0, 4557.0], [194.0, 121830.0], [195.0, 127792.0], [196.0, 134038.0], [197.0, 140072.0], [198.0, 146639.0], [199.0, 4447.0], [193.0, 4787.0], [206.0, 4404.0], [202.0, 155034.0], [203.0, 161617.0], [204.0, 4807.0], [205.0, 4747.0], [200.0, 4718.0], [201.0, 4444.0], [207.0, 4431.0], [212.0, 4347.0], [215.0, 4400.0], [208.0, 4433.0], [219.0, 4397.0], [230.0, 178414.5], [228.0, 4763.0], [227.0, 4403.0], [234.0, 7435.0], [239.0, 4846.0], [236.0, 4414.0], [238.0, 4868.0], [235.0, 4433.0], [244.0, 22212.0], [246.0, 19716.0], [240.0, 4831.0], [250.0, 25470.0], [254.0, 4886.0], [252.0, 4415.0], [253.0, 4388.0], [263.0, 40689.5], [259.0, 28418.0], [256.0, 4755.0], [258.0, 4380.0], [260.0, 31416.0], [261.0, 34363.0], [262.0, 37380.0], [269.0, 43129.0], [268.0, 46533.0], [270.0, 106290.5], [265.0, 70486.5], [271.0, 102753.0], [266.0, 4390.0], [267.0, 61381.5], [273.0, 79634.0], [275.0, 73572.0], [272.0, 76584.0], [277.0, 85153.5], [278.0, 112368.0], [279.0, 91660.0], [284.0, 93765.0], [285.0, 97106.0], [286.0, 100573.0], [281.0, 4403.0], [480.0, 119105.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 480.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 3001.0, "minX": 15.0, "maxY": 178855.0, "series": [{"data": [[16.0, 4582.0], [139.0, 4702.5], [147.0, 3006.0], [148.0, 3003.0], [151.0, 3006.0], [149.0, 3012.0], [145.0, 3001.0], [150.0, 3004.0], [146.0, 3007.0], [154.0, 3012.0], [157.0, 3006.0], [156.0, 3010.0], [153.0, 3010.0], [155.0, 3008.0], [152.0, 3005.0], [159.0, 3005.0], [158.0, 3006.0], [161.0, 3006.0], [162.0, 3005.0], [160.0, 3007.0], [163.0, 3007.0], [164.0, 3008.0], [167.0, 3004.0], [166.0, 3011.0], [165.0, 3007.0], [169.0, 3010.0], [168.0, 3009.0], [173.0, 3006.0], [172.0, 3015.0], [170.0, 3008.0], [171.0, 3010.0], [175.0, 3009.0], [180.0, 3009.0], [181.0, 3012.0], [182.0, 3012.0], [176.0, 3011.0], [184.0, 3006.0], [189.0, 3012.0], [185.0, 3009.0], [194.0, 3009.0], [195.0, 3005.0], [196.0, 3002.0], [197.0, 3011.0], [198.0, 3006.0], [199.0, 3012.0], [193.0, 3001.0], [206.0, 3007.0], [205.0, 3011.0], [202.0, 3010.0], [203.0, 3013.0], [204.0, 3008.0], [200.0, 3015.0], [201.0, 3017.0], [207.0, 3005.0], [212.0, 3003.0], [208.0, 3015.5], [216.0, 3010.0], [218.0, 3009.0], [219.0, 3013.0], [230.0, 3011.0], [228.0, 3002.0], [234.0, 3001.0], [239.0, 3003.0], [236.0, 3007.0], [238.0, 3016.5], [235.0, 3021.0], [244.0, 3010.0], [246.0, 3010.0], [240.0, 3006.0], [250.0, 3010.5], [254.0, 3010.0], [249.0, 3011.0], [252.0, 3009.0], [253.0, 3005.0], [259.0, 3009.0], [262.0, 3012.0], [260.0, 3006.0], [261.0, 3004.0], [263.0, 3001.0], [258.0, 3010.0], [256.0, 3015.0], [269.0, 3006.0], [268.0, 3009.0], [270.0, 3006.0], [271.0, 3013.0], [265.0, 3009.0], [266.0, 3007.0], [267.0, 3012.0], [273.0, 3012.0], [275.0, 3014.0], [274.0, 3013.0], [272.0, 3009.0], [277.0, 3015.0], [278.0, 3010.0], [279.0, 3003.0], [284.0, 3003.0], [285.0, 3010.0], [286.0, 3013.0], [281.0, 3008.5], [480.0, 3008.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[15.0, 4670.0], [139.0, 5141.0], [143.0, 5830.0], [147.0, 6778.5], [148.0, 8462.0], [151.0, 9365.0], [149.0, 11559.0], [145.0, 27750.0], [150.0, 51790.0], [146.0, 45680.5], [154.0, 70806.5], [157.0, 14907.0], [156.0, 20522.0], [153.0, 18833.0], [155.0, 72285.0], [152.0, 54555.0], [159.0, 63001.0], [158.0, 132297.5], [161.0, 32503.0], [162.0, 35331.0], [160.0, 144483.0], [163.0, 44185.5], [164.0, 125978.0], [166.0, 159628.0], [168.0, 50322.0], [171.0, 178855.0], [169.0, 164202.0], [173.0, 4786.0], [181.0, 4534.0], [182.0, 4511.0], [176.0, 170832.0], [184.0, 103963.0], [189.0, 109784.0], [185.0, 4557.0], [194.0, 121830.0], [195.0, 127792.0], [196.0, 134038.0], [197.0, 140072.0], [198.0, 146639.0], [199.0, 4447.0], [193.0, 4787.0], [206.0, 4404.0], [202.0, 155034.0], [203.0, 161617.0], [204.0, 4807.0], [205.0, 4747.0], [200.0, 4718.0], [201.0, 4444.0], [207.0, 4431.0], [212.0, 4347.0], [215.0, 4400.0], [208.0, 4433.0], [219.0, 4397.0], [230.0, 178414.5], [228.0, 4763.0], [227.0, 4403.0], [234.0, 7434.0], [239.0, 4846.0], [236.0, 4414.0], [238.0, 4868.0], [235.0, 4433.0], [244.0, 22212.0], [246.0, 19716.0], [240.0, 4831.0], [250.0, 25470.0], [254.0, 4886.0], [252.0, 4415.0], [253.0, 4388.0], [263.0, 40689.5], [259.0, 28418.0], [256.0, 4755.0], [258.0, 4380.0], [260.0, 31416.0], [261.0, 34363.0], [262.0, 37380.0], [269.0, 43129.0], [268.0, 46533.0], [270.0, 106290.5], [265.0, 70486.5], [271.0, 102753.0], [266.0, 4390.0], [267.0, 61381.5], [273.0, 79634.0], [275.0, 73572.0], [272.0, 76584.0], [277.0, 85153.5], [278.0, 112368.0], [279.0, 91660.0], [284.0, 93765.0], [285.0, 97106.0], [286.0, 100573.0], [281.0, 4403.0], [480.0, 119105.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 480.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 76.28333333333333, "minX": 1.6765449E12, "maxY": 219.41666666666666, "series": [{"data": [[1.67654502E12, 207.16666666666666], [1.67654514E12, 173.85], [1.67654496E12, 195.58333333333334], [1.67654508E12, 219.41666666666666], [1.6765449E12, 76.28333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67654514E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.9333333333333333, "minX": 1.6765449E12, "maxY": 205.1, "series": [{"data": [[1.67654502E12, 205.1], [1.67654514E12, 0.9333333333333333], [1.67654496E12, 192.53333333333333], [1.67654508E12, 183.63333333333333], [1.6765449E12, 49.3]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.67654508E12, 5.25]], "isOverall": false, "label": "502", "isController": false}, {"data": [[1.67654502E12, 2.066666666666667], [1.67654514E12, 189.58333333333334], [1.67654496E12, 3.05], [1.67654508E12, 30.533333333333335], [1.6765449E12, 10.316666666666666]], "isOverall": false, "label": "504", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67654514E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.9333333333333333, "minX": 1.6765449E12, "maxY": 205.1, "series": [{"data": [[1.67654502E12, 205.1], [1.67654514E12, 0.9333333333333333], [1.67654496E12, 192.53333333333333], [1.67654508E12, 183.63333333333333], [1.6765449E12, 49.3]], "isOverall": false, "label": "HTTP Request-success", "isController": false}, {"data": [[1.67654502E12, 2.066666666666667], [1.67654514E12, 189.58333333333334], [1.67654496E12, 3.05], [1.67654508E12, 35.78333333333333], [1.6765449E12, 10.316666666666666]], "isOverall": false, "label": "HTTP Request-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67654514E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.9333333333333333, "minX": 1.6765449E12, "maxY": 205.1, "series": [{"data": [[1.67654502E12, 205.1], [1.67654514E12, 0.9333333333333333], [1.67654496E12, 192.53333333333333], [1.67654508E12, 183.63333333333333], [1.6765449E12, 49.3]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.67654502E12, 2.066666666666667], [1.67654514E12, 189.58333333333334], [1.67654496E12, 3.05], [1.67654508E12, 35.78333333333333], [1.6765449E12, 10.316666666666666]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67654514E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

