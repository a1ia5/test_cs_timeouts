/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 2.0, "minX": 0.0, "maxY": 5986.0, "series": [{"data": [[0.0, 2.0], [0.1, 23.0], [0.2, 30.0], [0.3, 34.0], [0.4, 37.0], [0.5, 39.0], [0.6, 42.0], [0.7, 44.0], [0.8, 45.0], [0.9, 47.0], [1.0, 48.0], [1.1, 49.0], [1.2, 50.0], [1.3, 52.0], [1.4, 53.0], [1.5, 54.0], [1.6, 55.0], [1.7, 56.0], [1.8, 57.0], [1.9, 57.0], [2.0, 58.0], [2.1, 59.0], [2.2, 59.0], [2.3, 60.0], [2.4, 60.0], [2.5, 61.0], [2.6, 61.0], [2.7, 62.0], [2.8, 62.0], [2.9, 63.0], [3.0, 63.0], [3.1, 64.0], [3.2, 64.0], [3.3, 64.0], [3.4, 65.0], [3.5, 65.0], [3.6, 66.0], [3.7, 66.0], [3.8, 66.0], [3.9, 67.0], [4.0, 67.0], [4.1, 68.0], [4.2, 68.0], [4.3, 68.0], [4.4, 69.0], [4.5, 69.0], [4.6, 69.0], [4.7, 70.0], [4.8, 70.0], [4.9, 71.0], [5.0, 71.0], [5.1, 71.0], [5.2, 72.0], [5.3, 72.0], [5.4, 72.0], [5.5, 73.0], [5.6, 73.0], [5.7, 73.0], [5.8, 74.0], [5.9, 74.0], [6.0, 74.0], [6.1, 75.0], [6.2, 75.0], [6.3, 75.0], [6.4, 75.0], [6.5, 76.0], [6.6, 76.0], [6.7, 76.0], [6.8, 77.0], [6.9, 77.0], [7.0, 77.0], [7.1, 78.0], [7.2, 78.0], [7.3, 78.0], [7.4, 78.0], [7.5, 79.0], [7.6, 79.0], [7.7, 79.0], [7.8, 79.0], [7.9, 80.0], [8.0, 80.0], [8.1, 80.0], [8.2, 80.0], [8.3, 81.0], [8.4, 81.0], [8.5, 81.0], [8.6, 81.0], [8.7, 82.0], [8.8, 82.0], [8.9, 82.0], [9.0, 82.0], [9.1, 83.0], [9.2, 83.0], [9.3, 83.0], [9.4, 83.0], [9.5, 84.0], [9.6, 84.0], [9.7, 84.0], [9.8, 84.0], [9.9, 84.0], [10.0, 85.0], [10.1, 85.0], [10.2, 85.0], [10.3, 85.0], [10.4, 86.0], [10.5, 86.0], [10.6, 86.0], [10.7, 86.0], [10.8, 86.0], [10.9, 87.0], [11.0, 87.0], [11.1, 87.0], [11.2, 87.0], [11.3, 87.0], [11.4, 88.0], [11.5, 88.0], [11.6, 88.0], [11.7, 88.0], [11.8, 88.0], [11.9, 88.0], [12.0, 89.0], [12.1, 89.0], [12.2, 89.0], [12.3, 89.0], [12.4, 89.0], [12.5, 89.0], [12.6, 90.0], [12.7, 90.0], [12.8, 90.0], [12.9, 90.0], [13.0, 90.0], [13.1, 90.0], [13.2, 91.0], [13.3, 91.0], [13.4, 91.0], [13.5, 91.0], [13.6, 91.0], [13.7, 91.0], [13.8, 91.0], [13.9, 92.0], [14.0, 92.0], [14.1, 92.0], [14.2, 92.0], [14.3, 92.0], [14.4, 92.0], [14.5, 92.0], [14.6, 93.0], [14.7, 93.0], [14.8, 93.0], [14.9, 93.0], [15.0, 93.0], [15.1, 93.0], [15.2, 93.0], [15.3, 94.0], [15.4, 94.0], [15.5, 94.0], [15.6, 94.0], [15.7, 94.0], [15.8, 94.0], [15.9, 94.0], [16.0, 94.0], [16.1, 95.0], [16.2, 95.0], [16.3, 95.0], [16.4, 95.0], [16.5, 95.0], [16.6, 95.0], [16.7, 95.0], [16.8, 95.0], [16.9, 96.0], [17.0, 96.0], [17.1, 96.0], [17.2, 96.0], [17.3, 96.0], [17.4, 96.0], [17.5, 96.0], [17.6, 96.0], [17.7, 97.0], [17.8, 97.0], [17.9, 97.0], [18.0, 97.0], [18.1, 97.0], [18.2, 97.0], [18.3, 97.0], [18.4, 97.0], [18.5, 98.0], [18.6, 98.0], [18.7, 98.0], [18.8, 98.0], [18.9, 98.0], [19.0, 98.0], [19.1, 98.0], [19.2, 98.0], [19.3, 99.0], [19.4, 99.0], [19.5, 99.0], [19.6, 99.0], [19.7, 99.0], [19.8, 99.0], [19.9, 99.0], [20.0, 99.0], [20.1, 99.0], [20.2, 100.0], [20.3, 100.0], [20.4, 100.0], [20.5, 100.0], [20.6, 100.0], [20.7, 100.0], [20.8, 100.0], [20.9, 100.0], [21.0, 101.0], [21.1, 101.0], [21.2, 101.0], [21.3, 101.0], [21.4, 101.0], [21.5, 101.0], [21.6, 101.0], [21.7, 102.0], [21.8, 102.0], [21.9, 102.0], [22.0, 102.0], [22.1, 102.0], [22.2, 102.0], [22.3, 102.0], [22.4, 102.0], [22.5, 103.0], [22.6, 103.0], [22.7, 103.0], [22.8, 103.0], [22.9, 103.0], [23.0, 103.0], [23.1, 103.0], [23.2, 103.0], [23.3, 104.0], [23.4, 104.0], [23.5, 104.0], [23.6, 104.0], [23.7, 104.0], [23.8, 104.0], [23.9, 104.0], [24.0, 104.0], [24.1, 105.0], [24.2, 105.0], [24.3, 105.0], [24.4, 105.0], [24.5, 105.0], [24.6, 105.0], [24.7, 105.0], [24.8, 106.0], [24.9, 106.0], [25.0, 106.0], [25.1, 106.0], [25.2, 106.0], [25.3, 106.0], [25.4, 106.0], [25.5, 106.0], [25.6, 107.0], [25.7, 107.0], [25.8, 107.0], [25.9, 107.0], [26.0, 107.0], [26.1, 107.0], [26.2, 107.0], [26.3, 107.0], [26.4, 108.0], [26.5, 108.0], [26.6, 108.0], [26.7, 108.0], [26.8, 108.0], [26.9, 108.0], [27.0, 108.0], [27.1, 108.0], [27.2, 109.0], [27.3, 109.0], [27.4, 109.0], [27.5, 109.0], [27.6, 109.0], [27.7, 109.0], [27.8, 109.0], [27.9, 109.0], [28.0, 110.0], [28.1, 110.0], [28.2, 110.0], [28.3, 110.0], [28.4, 110.0], [28.5, 110.0], [28.6, 110.0], [28.7, 110.0], [28.8, 111.0], [28.9, 111.0], [29.0, 111.0], [29.1, 111.0], [29.2, 111.0], [29.3, 111.0], [29.4, 111.0], [29.5, 111.0], [29.6, 112.0], [29.7, 112.0], [29.8, 112.0], [29.9, 112.0], [30.0, 112.0], [30.1, 112.0], [30.2, 112.0], [30.3, 112.0], [30.4, 113.0], [30.5, 113.0], [30.6, 113.0], [30.7, 113.0], [30.8, 113.0], [30.9, 113.0], [31.0, 113.0], [31.1, 113.0], [31.2, 114.0], [31.3, 114.0], [31.4, 114.0], [31.5, 114.0], [31.6, 114.0], [31.7, 114.0], [31.8, 114.0], [31.9, 114.0], [32.0, 115.0], [32.1, 115.0], [32.2, 115.0], [32.3, 115.0], [32.4, 115.0], [32.5, 115.0], [32.6, 115.0], [32.7, 115.0], [32.8, 116.0], [32.9, 116.0], [33.0, 116.0], [33.1, 116.0], [33.2, 116.0], [33.3, 116.0], [33.4, 116.0], [33.5, 116.0], [33.6, 116.0], [33.7, 117.0], [33.8, 117.0], [33.9, 117.0], [34.0, 117.0], [34.1, 117.0], [34.2, 117.0], [34.3, 117.0], [34.4, 117.0], [34.5, 118.0], [34.6, 118.0], [34.7, 118.0], [34.8, 118.0], [34.9, 118.0], [35.0, 118.0], [35.1, 118.0], [35.2, 118.0], [35.3, 119.0], [35.4, 119.0], [35.5, 119.0], [35.6, 119.0], [35.7, 119.0], [35.8, 119.0], [35.9, 119.0], [36.0, 119.0], [36.1, 119.0], [36.2, 120.0], [36.3, 120.0], [36.4, 120.0], [36.5, 120.0], [36.6, 120.0], [36.7, 120.0], [36.8, 120.0], [36.9, 120.0], [37.0, 121.0], [37.1, 121.0], [37.2, 121.0], [37.3, 121.0], [37.4, 121.0], [37.5, 121.0], [37.6, 121.0], [37.7, 121.0], [37.8, 121.0], [37.9, 122.0], [38.0, 122.0], [38.1, 122.0], [38.2, 122.0], [38.3, 122.0], [38.4, 122.0], [38.5, 122.0], [38.6, 122.0], [38.7, 122.0], [38.8, 123.0], [38.9, 123.0], [39.0, 123.0], [39.1, 123.0], [39.2, 123.0], [39.3, 123.0], [39.4, 123.0], [39.5, 123.0], [39.6, 123.0], [39.7, 123.0], [39.8, 124.0], [39.9, 124.0], [40.0, 124.0], [40.1, 124.0], [40.2, 124.0], [40.3, 124.0], [40.4, 124.0], [40.5, 124.0], [40.6, 124.0], [40.7, 125.0], [40.8, 125.0], [40.9, 125.0], [41.0, 125.0], [41.1, 125.0], [41.2, 125.0], [41.3, 125.0], [41.4, 125.0], [41.5, 125.0], [41.6, 125.0], [41.7, 126.0], [41.8, 126.0], [41.9, 126.0], [42.0, 126.0], [42.1, 126.0], [42.2, 126.0], [42.3, 126.0], [42.4, 126.0], [42.5, 126.0], [42.6, 127.0], [42.7, 127.0], [42.8, 127.0], [42.9, 127.0], [43.0, 127.0], [43.1, 127.0], [43.2, 127.0], [43.3, 127.0], [43.4, 127.0], [43.5, 127.0], [43.6, 128.0], [43.7, 128.0], [43.8, 128.0], [43.9, 128.0], [44.0, 128.0], [44.1, 128.0], [44.2, 128.0], [44.3, 128.0], [44.4, 128.0], [44.5, 129.0], [44.6, 129.0], [44.7, 129.0], [44.8, 129.0], [44.9, 129.0], [45.0, 129.0], [45.1, 129.0], [45.2, 129.0], [45.3, 130.0], [45.4, 130.0], [45.5, 130.0], [45.6, 130.0], [45.7, 130.0], [45.8, 130.0], [45.9, 130.0], [46.0, 130.0], [46.1, 131.0], [46.2, 131.0], [46.3, 131.0], [46.4, 131.0], [46.5, 131.0], [46.6, 131.0], [46.7, 131.0], [46.8, 131.0], [46.9, 132.0], [47.0, 132.0], [47.1, 132.0], [47.2, 132.0], [47.3, 132.0], [47.4, 132.0], [47.5, 132.0], [47.6, 132.0], [47.7, 133.0], [47.8, 133.0], [47.9, 133.0], [48.0, 133.0], [48.1, 133.0], [48.2, 133.0], [48.3, 133.0], [48.4, 133.0], [48.5, 134.0], [48.6, 134.0], [48.7, 134.0], [48.8, 134.0], [48.9, 134.0], [49.0, 134.0], [49.1, 134.0], [49.2, 135.0], [49.3, 135.0], [49.4, 135.0], [49.5, 135.0], [49.6, 135.0], [49.7, 135.0], [49.8, 135.0], [49.9, 136.0], [50.0, 136.0], [50.1, 136.0], [50.2, 136.0], [50.3, 136.0], [50.4, 136.0], [50.5, 136.0], [50.6, 136.0], [50.7, 137.0], [50.8, 137.0], [50.9, 137.0], [51.0, 137.0], [51.1, 137.0], [51.2, 137.0], [51.3, 137.0], [51.4, 138.0], [51.5, 138.0], [51.6, 138.0], [51.7, 138.0], [51.8, 138.0], [51.9, 138.0], [52.0, 138.0], [52.1, 139.0], [52.2, 139.0], [52.3, 139.0], [52.4, 139.0], [52.5, 139.0], [52.6, 139.0], [52.7, 139.0], [52.8, 140.0], [52.9, 140.0], [53.0, 140.0], [53.1, 140.0], [53.2, 140.0], [53.3, 140.0], [53.4, 140.0], [53.5, 141.0], [53.6, 141.0], [53.7, 141.0], [53.8, 141.0], [53.9, 141.0], [54.0, 141.0], [54.1, 141.0], [54.2, 142.0], [54.3, 142.0], [54.4, 142.0], [54.5, 142.0], [54.6, 142.0], [54.7, 142.0], [54.8, 143.0], [54.9, 143.0], [55.0, 143.0], [55.1, 143.0], [55.2, 143.0], [55.3, 143.0], [55.4, 143.0], [55.5, 144.0], [55.6, 144.0], [55.7, 144.0], [55.8, 144.0], [55.9, 144.0], [56.0, 144.0], [56.1, 144.0], [56.2, 145.0], [56.3, 145.0], [56.4, 145.0], [56.5, 145.0], [56.6, 145.0], [56.7, 145.0], [56.8, 146.0], [56.9, 146.0], [57.0, 146.0], [57.1, 146.0], [57.2, 146.0], [57.3, 146.0], [57.4, 147.0], [57.5, 147.0], [57.6, 147.0], [57.7, 147.0], [57.8, 147.0], [57.9, 147.0], [58.0, 148.0], [58.1, 148.0], [58.2, 148.0], [58.3, 148.0], [58.4, 148.0], [58.5, 148.0], [58.6, 149.0], [58.7, 149.0], [58.8, 149.0], [58.9, 149.0], [59.0, 149.0], [59.1, 149.0], [59.2, 150.0], [59.3, 150.0], [59.4, 150.0], [59.5, 150.0], [59.6, 150.0], [59.7, 150.0], [59.8, 151.0], [59.9, 151.0], [60.0, 151.0], [60.1, 151.0], [60.2, 151.0], [60.3, 151.0], [60.4, 152.0], [60.5, 152.0], [60.6, 152.0], [60.7, 152.0], [60.8, 152.0], [60.9, 152.0], [61.0, 153.0], [61.1, 153.0], [61.2, 153.0], [61.3, 153.0], [61.4, 153.0], [61.5, 154.0], [61.6, 154.0], [61.7, 154.0], [61.8, 154.0], [61.9, 154.0], [62.0, 154.0], [62.1, 155.0], [62.2, 155.0], [62.3, 155.0], [62.4, 155.0], [62.5, 155.0], [62.6, 155.0], [62.7, 156.0], [62.8, 156.0], [62.9, 156.0], [63.0, 156.0], [63.1, 156.0], [63.2, 157.0], [63.3, 157.0], [63.4, 157.0], [63.5, 157.0], [63.6, 157.0], [63.7, 157.0], [63.8, 158.0], [63.9, 158.0], [64.0, 158.0], [64.1, 158.0], [64.2, 158.0], [64.3, 159.0], [64.4, 159.0], [64.5, 159.0], [64.6, 159.0], [64.7, 159.0], [64.8, 160.0], [64.9, 160.0], [65.0, 160.0], [65.1, 160.0], [65.2, 160.0], [65.3, 161.0], [65.4, 161.0], [65.5, 161.0], [65.6, 161.0], [65.7, 161.0], [65.8, 162.0], [65.9, 162.0], [66.0, 162.0], [66.1, 162.0], [66.2, 163.0], [66.3, 163.0], [66.4, 163.0], [66.5, 163.0], [66.6, 163.0], [66.7, 164.0], [66.8, 164.0], [66.9, 164.0], [67.0, 164.0], [67.1, 165.0], [67.2, 165.0], [67.3, 165.0], [67.4, 165.0], [67.5, 166.0], [67.6, 166.0], [67.7, 166.0], [67.8, 166.0], [67.9, 167.0], [68.0, 167.0], [68.1, 167.0], [68.2, 167.0], [68.3, 168.0], [68.4, 168.0], [68.5, 168.0], [68.6, 168.0], [68.7, 169.0], [68.8, 169.0], [68.9, 169.0], [69.0, 169.0], [69.1, 170.0], [69.2, 170.0], [69.3, 170.0], [69.4, 171.0], [69.5, 171.0], [69.6, 171.0], [69.7, 171.0], [69.8, 172.0], [69.9, 172.0], [70.0, 172.0], [70.1, 172.0], [70.2, 173.0], [70.3, 173.0], [70.4, 173.0], [70.5, 174.0], [70.6, 174.0], [70.7, 174.0], [70.8, 174.0], [70.9, 175.0], [71.0, 175.0], [71.1, 175.0], [71.2, 176.0], [71.3, 176.0], [71.4, 176.0], [71.5, 177.0], [71.6, 177.0], [71.7, 177.0], [71.8, 178.0], [71.9, 178.0], [72.0, 178.0], [72.1, 179.0], [72.2, 179.0], [72.3, 179.0], [72.4, 179.0], [72.5, 180.0], [72.6, 180.0], [72.7, 181.0], [72.8, 181.0], [72.9, 181.0], [73.0, 181.0], [73.1, 182.0], [73.2, 182.0], [73.3, 183.0], [73.4, 183.0], [73.5, 183.0], [73.6, 184.0], [73.7, 184.0], [73.8, 184.0], [73.9, 185.0], [74.0, 185.0], [74.1, 185.0], [74.2, 186.0], [74.3, 186.0], [74.4, 186.0], [74.5, 187.0], [74.6, 187.0], [74.7, 187.0], [74.8, 188.0], [74.9, 188.0], [75.0, 188.0], [75.1, 189.0], [75.2, 189.0], [75.3, 190.0], [75.4, 190.0], [75.5, 190.0], [75.6, 191.0], [75.7, 191.0], [75.8, 192.0], [75.9, 192.0], [76.0, 192.0], [76.1, 193.0], [76.2, 193.0], [76.3, 194.0], [76.4, 194.0], [76.5, 194.0], [76.6, 195.0], [76.7, 195.0], [76.8, 196.0], [76.9, 196.0], [77.0, 197.0], [77.1, 197.0], [77.2, 198.0], [77.3, 198.0], [77.4, 199.0], [77.5, 199.0], [77.6, 200.0], [77.7, 200.0], [77.8, 201.0], [77.9, 201.0], [78.0, 202.0], [78.1, 202.0], [78.2, 203.0], [78.3, 203.0], [78.4, 204.0], [78.5, 204.0], [78.6, 205.0], [78.7, 205.0], [78.8, 206.0], [78.9, 206.0], [79.0, 207.0], [79.1, 208.0], [79.2, 208.0], [79.3, 209.0], [79.4, 210.0], [79.5, 210.0], [79.6, 211.0], [79.7, 212.0], [79.8, 212.0], [79.9, 213.0], [80.0, 214.0], [80.1, 214.0], [80.2, 215.0], [80.3, 216.0], [80.4, 216.0], [80.5, 217.0], [80.6, 218.0], [80.7, 218.0], [80.8, 219.0], [80.9, 220.0], [81.0, 220.0], [81.1, 221.0], [81.2, 222.0], [81.3, 223.0], [81.4, 223.0], [81.5, 224.0], [81.6, 225.0], [81.7, 226.0], [81.8, 227.0], [81.9, 228.0], [82.0, 229.0], [82.1, 229.0], [82.2, 230.0], [82.3, 231.0], [82.4, 232.0], [82.5, 233.0], [82.6, 234.0], [82.7, 235.0], [82.8, 236.0], [82.9, 237.0], [83.0, 238.0], [83.1, 239.0], [83.2, 240.0], [83.3, 241.0], [83.4, 243.0], [83.5, 244.0], [83.6, 245.0], [83.7, 246.0], [83.8, 248.0], [83.9, 249.0], [84.0, 250.0], [84.1, 252.0], [84.2, 253.0], [84.3, 254.0], [84.4, 256.0], [84.5, 257.0], [84.6, 259.0], [84.7, 260.0], [84.8, 262.0], [84.9, 264.0], [85.0, 266.0], [85.1, 268.0], [85.2, 269.0], [85.3, 271.0], [85.4, 273.0], [85.5, 275.0], [85.6, 277.0], [85.7, 280.0], [85.8, 282.0], [85.9, 284.0], [86.0, 286.0], [86.1, 289.0], [86.2, 292.0], [86.3, 295.0], [86.4, 298.0], [86.5, 301.0], [86.6, 305.0], [86.7, 309.0], [86.8, 313.0], [86.9, 317.0], [87.0, 322.0], [87.1, 327.0], [87.2, 332.0], [87.3, 338.0], [87.4, 346.0], [87.5, 354.0], [87.6, 365.0], [87.7, 377.0], [87.8, 395.0], [87.9, 429.0], [88.0, 518.0], [88.1, 1547.0], [88.2, 2007.0], [88.3, 2060.0], [88.4, 2087.0], [88.5, 2113.0], [88.6, 2141.0], [88.7, 2170.0], [88.8, 2232.0], [88.9, 2369.0], [89.0, 2723.0], [89.1, 3012.0], [89.2, 3034.0], [89.3, 3061.0], [89.4, 3086.0], [89.5, 3102.0], [89.6, 3114.0], [89.7, 3123.0], [89.8, 3131.0], [89.9, 3138.0], [90.0, 3145.0], [90.1, 3154.0], [90.2, 3163.0], [90.3, 3172.0], [90.4, 3179.0], [90.5, 3187.0], [90.6, 3196.0], [90.7, 3205.0], [90.8, 3213.0], [90.9, 3223.0], [91.0, 3233.0], [91.1, 3241.0], [91.2, 3249.0], [91.3, 3256.0], [91.4, 3263.0], [91.5, 3270.0], [91.6, 3277.0], [91.7, 3283.0], [91.8, 3288.0], [91.9, 3294.0], [92.0, 3300.0], [92.1, 3305.0], [92.2, 3311.0], [92.3, 3316.0], [92.4, 3321.0], [92.5, 3326.0], [92.6, 3331.0], [92.7, 3336.0], [92.8, 3340.0], [92.9, 3345.0], [93.0, 3349.0], [93.1, 3354.0], [93.2, 3358.0], [93.3, 3362.0], [93.4, 3367.0], [93.5, 3371.0], [93.6, 3375.0], [93.7, 3379.0], [93.8, 3384.0], [93.9, 3388.0], [94.0, 3392.0], [94.1, 3396.0], [94.2, 3400.0], [94.3, 3403.0], [94.4, 3407.0], [94.5, 3411.0], [94.6, 3415.0], [94.7, 3418.0], [94.8, 3422.0], [94.9, 3426.0], [95.0, 3429.0], [95.1, 3433.0], [95.2, 3437.0], [95.3, 3441.0], [95.4, 3446.0], [95.5, 3450.0], [95.6, 3454.0], [95.7, 3458.0], [95.8, 3462.0], [95.9, 3466.0], [96.0, 3470.0], [96.1, 3475.0], [96.2, 3479.0], [96.3, 3483.0], [96.4, 3487.0], [96.5, 3492.0], [96.6, 3496.0], [96.7, 3501.0], [96.8, 3505.0], [96.9, 3510.0], [97.0, 3515.0], [97.1, 3519.0], [97.2, 3524.0], [97.3, 3529.0], [97.4, 3534.0], [97.5, 3539.0], [97.6, 3544.0], [97.7, 3550.0], [97.8, 3556.0], [97.9, 3561.0], [98.0, 3568.0], [98.1, 3574.0], [98.2, 3581.0], [98.3, 3589.0], [98.4, 3596.0], [98.5, 3606.0], [98.6, 3617.0], [98.7, 3630.0], [98.8, 3643.0], [98.9, 3657.0], [99.0, 3678.0], [99.1, 3697.0], [99.2, 3719.0], [99.3, 3746.0], [99.4, 3784.0], [99.5, 3826.0], [99.6, 3874.0], [99.7, 3933.0], [99.8, 4037.0], [99.9, 4198.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "maxY": 956856.0, "series": [{"data": [[0.0, 334863.0], [600.0, 212.0], [700.0, 511.0], [800.0, 157.0], [900.0, 25.0], [1000.0, 184.0], [1100.0, 49.0], [1500.0, 342.0], [1600.0, 67.0], [1700.0, 231.0], [1800.0, 21.0], [1900.0, 879.0], [2000.0, 4405.0], [2100.0, 5131.0], [2300.0, 697.0], [2200.0, 1814.0], [2400.0, 528.0], [2500.0, 446.0], [2600.0, 312.0], [2700.0, 493.0], [2800.0, 568.0], [2900.0, 211.0], [3000.0, 6928.0], [3100.0, 19225.0], [3200.0, 22644.0], [3300.0, 36632.0], [3400.0, 41164.0], [3500.0, 29419.0], [3600.0, 11269.0], [3700.0, 5368.0], [3800.0, 3611.0], [3900.0, 2047.0], [4000.0, 1258.0], [4300.0, 434.0], [4100.0, 829.0], [4200.0, 374.0], [4500.0, 305.0], [4600.0, 134.0], [4400.0, 262.0], [4700.0, 102.0], [4800.0, 19.0], [4900.0, 9.0], [5000.0, 3.0], [5100.0, 12.0], [5500.0, 1.0], [5400.0, 1.0], [5900.0, 1.0], [100.0, 956856.0], [200.0, 147688.0], [300.0, 22857.0], [400.0, 2760.0], [500.0, 703.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 5900.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 1835.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 1465030.0, "series": [{"data": [[0.0, 1465030.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 1835.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 189933.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 8263.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 1000.0, "minX": 1.6763832E12, "maxY": 1000.0, "series": [{"data": [[1.67638398E12, 1000.0], [1.67638392E12, 1000.0], [1.67638362E12, 1000.0], [1.67638332E12, 1000.0], [1.6763832E12, 1000.0], [1.67638326E12, 1000.0], [1.67638386E12, 1000.0], [1.67638356E12, 1000.0], [1.6763838E12, 1000.0], [1.67638344E12, 1000.0], [1.6763835E12, 1000.0], [1.67638404E12, 1000.0], [1.67638338E12, 1000.0], [1.67638374E12, 1000.0], [1.67638368E12, 1000.0]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638404E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 518.3200501363158, "minX": 1000.0, "maxY": 518.3200501363158, "series": [{"data": [[1000.0, 518.3200501363158]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[1000.0, 518.3200501363158]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 1000.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 124546.3, "minX": 1.6763832E12, "maxY": 708053.5666666667, "series": [{"data": [[1.67638398E12, 487953.26666666666], [1.67638392E12, 621619.5], [1.67638362E12, 454355.3], [1.67638332E12, 493904.9666666667], [1.6763832E12, 608270.6666666666], [1.67638326E12, 435226.73333333334], [1.67638386E12, 540383.1], [1.67638356E12, 708053.5666666667], [1.6763838E12, 465190.73333333334], [1.67638344E12, 676869.9666666667], [1.6763835E12, 648694.8], [1.67638404E12, 274955.5], [1.67638338E12, 613934.5], [1.67638374E12, 638454.3333333334], [1.67638368E12, 509223.9]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.67638398E12, 213620.56666666668], [1.67638392E12, 290230.6], [1.67638362E12, 190507.8], [1.67638332E12, 238859.46666666667], [1.6763832E12, 266671.1666666667], [1.67638326E12, 197692.43333333332], [1.67638386E12, 258157.7], [1.67638356E12, 295246.6666666667], [1.6763838E12, 217852.73333333334], [1.67638344E12, 316324.86666666664], [1.6763835E12, 298732.9], [1.67638404E12, 124546.3], [1.67638338E12, 294317.6], [1.67638374E12, 275767.5333333333], [1.67638368E12, 240107.9]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638404E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 423.0925599062367, "minX": 1.6763832E12, "maxY": 705.3003329347447, "series": [{"data": [[1.67638398E12, 623.4670416409718], [1.67638392E12, 463.55967496190885], [1.67638362E12, 705.3003329347447], [1.67638332E12, 556.744343256783], [1.6763832E12, 474.6897952347013], [1.67638326E12, 681.3668816864069], [1.67638386E12, 519.4872267351768], [1.67638356E12, 451.8954614220822], [1.6763838E12, 615.5970208927117], [1.67638344E12, 423.0925599062367], [1.6763835E12, 448.8062140683811], [1.67638404E12, 478.08643104344867], [1.67638338E12, 455.45289261216385], [1.67638374E12, 486.53497789080706], [1.67638368E12, 557.1379579763931]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638404E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 423.0922421948948, "minX": 1.6763832E12, "maxY": 705.2999695200574, "series": [{"data": [[1.67638398E12, 623.4667907287968], [1.67638392E12, 463.55943641596264], [1.67638362E12, 705.2999695200574], [1.67638332E12, 556.7440814570988], [1.6763832E12, 474.52197144172544], [1.67638326E12, 681.3666105581907], [1.67638386E12, 519.4869412507646], [1.67638356E12, 451.8952269288976], [1.6763838E12, 615.5966928423558], [1.67638344E12, 423.0922421948948], [1.6763835E12, 448.80591502754737], [1.67638404E12, 478.0861620671717], [1.67638338E12, 455.4526270260453], [1.67638374E12, 486.53440288958325], [1.67638368E12, 557.1376696338048]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638404E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.0025572148363989912, "minX": 1.6763832E12, "maxY": 0.9360244545873246, "series": [{"data": [[1.67638398E12, 0.003930957334476302], [1.67638392E12, 0.002731735844991279], [1.67638362E12, 0.004126515204801806], [1.67638332E12, 0.003328596005684798], [1.6763832E12, 0.9360244545873246], [1.67638326E12, 0.004371942746754933], [1.67638386E12, 0.00288079728011213], [1.67638356E12, 0.0026475037821482566], [1.6763838E12, 0.0037725790908904114], [1.67638344E12, 0.0030429686948417944], [1.6763835E12, 0.002893219996860058], [1.67638404E12, 0.005289866767084501], [1.67638338E12, 0.0025572148363989912], [1.67638374E12, 0.0032151476376358224], [1.67638368E12, 0.003376398694087171]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638404E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 2.0, "minX": 1.6763832E12, "maxY": 4857.0, "series": [{"data": [[1.67638398E12, 3865.0], [1.67638392E12, 3943.0], [1.67638362E12, 4128.0], [1.67638332E12, 3908.0], [1.6763832E12, 4857.0], [1.67638326E12, 3920.0], [1.67638386E12, 4019.0], [1.67638356E12, 4284.0], [1.6763838E12, 3860.0], [1.67638344E12, 3979.0], [1.6763835E12, 4169.0], [1.67638404E12, 3882.0], [1.67638338E12, 4014.0], [1.67638374E12, 4232.0], [1.67638368E12, 3947.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.67638398E12, 3164.9000000000015], [1.67638392E12, 326.8000000000029], [1.67638362E12, 3139.0], [1.67638332E12, 2183.0], [1.6763832E12, 320.0], [1.67638326E12, 3204.0], [1.67638386E12, 3180.9000000000015], [1.67638356E12, 335.0], [1.6763838E12, 3137.0], [1.67638344E12, 295.0], [1.6763835E12, 289.0], [1.67638404E12, 396.90000000000146], [1.67638338E12, 2059.0], [1.67638374E12, 370.0], [1.67638368E12, 2425.9000000000015]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.67638398E12, 3532.0], [1.67638392E12, 3555.0], [1.67638362E12, 3647.0], [1.67638332E12, 3523.0], [1.6763832E12, 3647.9900000000016], [1.67638326E12, 3625.0], [1.67638386E12, 3675.9600000000064], [1.67638356E12, 3807.980000000003], [1.6763838E12, 3474.0], [1.67638344E12, 3678.9900000000016], [1.6763835E12, 3534.980000000003], [1.67638404E12, 3582.0], [1.67638338E12, 3580.0], [1.67638374E12, 3963.9900000000016], [1.67638368E12, 3540.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.67638398E12, 3355.0], [1.67638392E12, 3406.0], [1.67638362E12, 3462.0], [1.67638332E12, 3310.0], [1.6763832E12, 3466.0], [1.67638326E12, 3418.0], [1.67638386E12, 3389.0], [1.67638356E12, 3483.0], [1.6763838E12, 3340.0], [1.67638344E12, 3395.0], [1.6763835E12, 3369.0], [1.67638404E12, 3422.0], [1.67638338E12, 3422.9500000000007], [1.67638374E12, 3506.0], [1.67638368E12, 3312.0]], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [[1.67638398E12, 2.0], [1.67638392E12, 4.0], [1.67638362E12, 8.0], [1.67638332E12, 2.0], [1.6763832E12, 3.0], [1.67638326E12, 4.0], [1.67638386E12, 2.0], [1.67638356E12, 7.0], [1.6763838E12, 6.0], [1.67638344E12, 19.0], [1.6763835E12, 9.0], [1.67638404E12, 3.0], [1.67638338E12, 10.0], [1.67638374E12, 9.0], [1.67638368E12, 4.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.67638398E12, 143.0], [1.67638392E12, 131.0], [1.67638362E12, 148.0], [1.67638332E12, 121.0], [1.6763832E12, 141.0], [1.67638326E12, 156.0], [1.67638386E12, 128.0], [1.67638356E12, 139.0], [1.6763838E12, 128.0], [1.67638344E12, 135.0], [1.6763835E12, 129.0], [1.67638404E12, 123.0], [1.67638338E12, 125.0], [1.67638374E12, 128.0], [1.67638368E12, 124.0]], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638404E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 80.0, "minX": 4.0, "maxY": 5986.0, "series": [{"data": [[4.0, 3124.0], [5.0, 3171.0], [34.0, 3218.0], [43.0, 3198.5], [48.0, 3132.5], [60.0, 3195.0], [63.0, 2176.0], [75.0, 3030.0], [72.0, 3158.0], [76.0, 3147.5], [78.0, 3160.0], [82.0, 3150.0], [89.0, 3041.5], [90.0, 3145.0], [93.0, 3184.0], [99.0, 3135.5], [110.0, 3131.0], [119.0, 3131.0], [121.0, 3146.0], [127.0, 3136.0], [124.0, 3243.0], [130.0, 3234.0], [131.0, 3138.0], [140.0, 3201.0], [146.0, 3156.0], [150.0, 3266.5], [155.0, 3365.0], [158.0, 3198.0], [159.0, 3141.0], [169.0, 3027.0], [180.0, 3143.0], [189.0, 3189.0], [185.0, 3333.0], [191.0, 3189.0], [184.0, 3180.0], [193.0, 3143.0], [192.0, 3144.0], [195.0, 3126.0], [194.0, 3203.5], [201.0, 3180.0], [207.0, 3139.0], [206.0, 3280.5], [212.0, 3135.0], [210.0, 3224.0], [220.0, 3007.0], [221.0, 3228.0], [218.0, 3157.0], [219.0, 3178.0], [236.0, 3144.0], [234.0, 3181.0], [232.0, 3176.0], [239.0, 3128.0], [233.0, 3183.0], [242.0, 3150.0], [246.0, 3143.0], [264.0, 3098.0], [266.0, 3172.0], [265.0, 3127.0], [284.0, 3113.0], [278.0, 3180.0], [286.0, 3131.0], [281.0, 3138.0], [299.0, 3116.0], [301.0, 3204.0], [290.0, 3145.0], [318.0, 3264.5], [326.0, 2541.0], [333.0, 3081.0], [331.0, 3195.0], [320.0, 3226.0], [346.0, 3190.0], [354.0, 2194.0], [363.0, 3136.0], [383.0, 3023.0], [371.0, 3146.0], [413.0, 3062.0], [412.0, 3274.0], [403.0, 3359.0], [409.0, 3121.0], [408.0, 3195.5], [443.0, 241.0], [448.0, 3121.0], [471.0, 3044.0], [483.0, 3103.0], [484.0, 3142.5], [491.0, 213.0], [510.0, 3164.5], [521.0, 3045.0], [516.0, 3138.0], [512.0, 2660.0], [562.0, 2082.0], [561.0, 3108.0], [573.0, 170.0], [592.0, 3134.0], [587.0, 3132.0], [612.0, 3126.0], [631.0, 156.0], [620.0, 3156.0], [654.0, 153.0], [691.0, 191.0], [698.0, 2052.0], [692.0, 3155.0], [709.0, 246.0], [720.0, 184.0], [717.0, 2111.0], [723.0, 129.0], [764.0, 183.0], [776.0, 303.0], [827.0, 153.0], [828.0, 358.5], [848.0, 2162.0], [889.0, 156.0], [870.0, 240.0], [880.0, 200.0], [893.0, 2396.0], [908.0, 359.0], [915.0, 176.0], [936.0, 255.0], [946.0, 2075.0], [982.0, 2109.5], [990.0, 124.0], [965.0, 109.0], [985.0, 181.0], [992.0, 126.0], [1022.0, 405.0], [998.0, 2093.0], [1081.0, 116.0], [1037.0, 281.0], [1055.0, 169.0], [1073.0, 119.0], [1068.0, 150.0], [1042.0, 250.0], [1128.0, 306.0], [1134.0, 632.5], [1088.0, 156.0], [1136.0, 322.0], [1144.0, 162.0], [1165.0, 284.0], [1169.0, 144.5], [1207.0, 196.0], [1266.0, 269.0], [1260.0, 156.0], [1225.0, 124.0], [1275.0, 215.0], [1236.0, 257.0], [1270.0, 194.0], [1245.0, 252.0], [1227.0, 121.0], [1229.0, 149.0], [1339.0, 128.0], [1296.0, 184.0], [1407.0, 119.0], [1378.0, 123.0], [1376.0, 164.0], [1388.0, 233.0], [1400.0, 224.0], [1382.0, 196.0], [1429.0, 170.0], [1433.0, 208.0], [1409.0, 195.0], [1425.0, 183.0], [1520.0, 252.0], [1526.0, 165.5], [1501.0, 207.0], [1498.0, 366.0], [1516.0, 204.0], [1524.0, 111.0], [1522.0, 180.0], [1533.0, 173.0], [1495.0, 177.0], [1503.0, 158.0], [1482.0, 186.0], [1475.0, 169.0], [1552.0, 162.5], [1560.0, 159.0], [1558.0, 185.0], [1549.0, 160.0], [1556.0, 178.0], [1605.0, 194.0], [1661.0, 182.0], [1640.0, 134.0], [1603.0, 184.0], [1633.0, 205.0], [1650.0, 192.0], [1643.0, 175.0], [1638.0, 185.0], [1667.0, 219.0], [1699.0, 171.0], [1702.0, 150.0], [1703.0, 167.0], [1689.0, 209.0], [1707.0, 183.0], [1693.0, 149.0], [1709.0, 200.0], [1683.0, 167.0], [1685.0, 188.0], [1682.0, 165.0], [1680.0, 152.0], [1694.0, 179.0], [1697.0, 193.0], [1676.0, 169.0], [1717.0, 149.0], [1748.0, 156.0], [1734.0, 204.0], [1783.0, 161.0], [1752.0, 191.0], [1777.0, 162.0], [1728.0, 179.0], [1849.0, 168.0], [1845.0, 216.0], [1813.0, 149.0], [1838.0, 193.0], [1811.0, 179.0], [1809.0, 140.0], [1839.0, 143.0], [1847.0, 175.0], [1848.0, 136.0], [1824.0, 140.0], [1855.0, 145.0], [1851.0, 156.0], [1854.0, 168.0], [1833.0, 194.0], [1831.0, 129.0], [1837.0, 181.0], [1795.0, 155.0], [1793.0, 152.0], [1841.0, 134.0], [1865.0, 169.0], [1880.0, 152.0], [1895.0, 163.0], [1901.0, 159.0], [1903.0, 129.0], [1902.0, 135.0], [1879.0, 175.0], [1872.0, 156.0], [1883.0, 162.0], [1862.0, 143.0], [1860.0, 166.0], [1856.0, 157.0], [1864.0, 163.0], [1863.0, 159.0], [1912.0, 169.0], [1894.0, 167.0], [1918.0, 169.0], [1917.0, 161.0], [1915.0, 151.0], [1870.0, 134.0], [1909.0, 142.0], [1906.0, 171.5], [1867.0, 160.0], [1869.0, 166.0], [1927.0, 176.0], [1921.0, 156.0], [1923.0, 134.0], [1925.0, 152.0], [1926.0, 116.0], [1956.0, 151.0], [1920.0, 158.0], [1964.0, 157.0], [1958.0, 176.0], [1960.0, 145.0], [1937.0, 149.0], [1930.0, 146.0], [1931.0, 130.0], [1972.0, 148.0], [1973.0, 104.0], [1975.0, 137.0], [1940.0, 157.0], [1942.0, 174.0], [1944.0, 153.0], [1948.0, 112.0], [1980.0, 154.0], [1952.0, 119.0], [1979.0, 149.0], [1987.0, 150.0], [2001.0, 167.0], [2002.0, 115.0], [2035.0, 152.0], [2007.0, 178.0], [2011.0, 132.0], [1985.0, 153.0], [2016.0, 136.0], [2046.0, 117.0], [2044.0, 148.0], [2037.0, 143.0], [1992.0, 152.0], [2005.0, 165.0], [1994.0, 171.0], [1996.0, 184.0], [2027.0, 114.0], [2024.0, 143.0], [2017.0, 125.0], [2021.0, 151.0], [2029.0, 149.0], [2048.0, 161.0], [2126.0, 157.0], [2096.0, 150.0], [2092.0, 143.0], [2094.0, 117.0], [2090.0, 151.0], [2088.0, 161.0], [2106.0, 147.0], [2110.0, 148.0], [2100.0, 123.5], [2098.0, 146.0], [2138.0, 118.5], [2136.0, 160.0], [2122.0, 144.0], [2118.0, 129.0], [2114.0, 127.0], [2112.0, 129.0], [2162.0, 145.0], [2074.0, 161.0], [2072.0, 145.0], [2070.0, 135.0], [2144.0, 151.0], [2160.0, 135.0], [2064.0, 158.0], [2062.0, 137.0], [2066.0, 125.0], [2132.0, 133.0], [2130.0, 143.0], [2178.0, 144.0], [2300.0, 138.0], [2226.0, 142.0], [2282.0, 135.0], [2298.0, 109.0], [2272.0, 134.0], [2280.0, 131.0], [2194.0, 137.0], [2190.0, 98.0], [2186.0, 141.0], [2208.0, 142.0], [2212.0, 149.5], [2218.0, 139.0], [2210.0, 148.0], [2302.0, 123.0], [2240.0, 125.0], [2252.0, 146.0], [2242.0, 136.0], [2256.0, 127.0], [2254.0, 137.0], [2260.0, 126.0], [2268.0, 138.5], [2264.0, 135.0], [2270.0, 144.0], [2222.0, 136.0], [2220.0, 156.0], [2228.0, 158.0], [2230.0, 148.0], [2234.0, 149.0], [2236.0, 129.0], [2238.0, 125.0], [2400.0, 124.0], [2374.0, 141.0], [2372.0, 135.0], [2378.0, 143.0], [2376.0, 129.0], [2386.0, 137.0], [2380.0, 118.0], [2398.0, 124.0], [2394.0, 123.0], [2306.0, 147.0], [2368.0, 144.0], [2402.0, 140.0], [2308.0, 134.0], [2314.0, 130.0], [2326.0, 124.0], [2322.0, 136.0], [2336.0, 137.0], [2416.0, 134.0], [2414.0, 129.0], [2424.0, 126.0], [2420.0, 127.0], [2412.0, 126.0], [2406.0, 129.0], [2404.0, 128.0], [2342.0, 162.0], [2348.0, 152.0], [2304.0, 133.0], [2362.0, 143.0], [2360.0, 140.0], [2356.0, 132.0], [2352.0, 153.0], [2344.0, 141.0], [2346.0, 133.0], [2450.0, 117.0], [2492.0, 166.0], [2480.0, 146.0], [2490.0, 122.0], [2558.0, 128.0], [2556.0, 118.0], [2550.0, 131.0], [2542.0, 120.0], [2540.0, 116.5], [2498.0, 137.0], [2524.0, 128.0], [2518.0, 124.0], [2526.0, 101.0], [2510.0, 127.0], [2516.0, 121.0], [2504.0, 110.0], [2500.0, 120.0], [2496.0, 135.0], [2472.0, 124.0], [2476.0, 109.0], [2478.0, 129.0], [2470.0, 114.0], [2494.0, 139.0], [2432.0, 125.0], [2440.0, 138.0], [2442.0, 138.0], [2536.0, 127.0], [2538.0, 119.0], [2448.0, 127.0], [2446.0, 121.0], [2528.0, 124.0], [2462.0, 146.0], [2452.0, 112.0], [2454.0, 117.0], [2458.0, 125.0], [2530.0, 126.0], [2532.0, 133.0], [2534.0, 108.0], [2568.0, 125.0], [2574.0, 118.0], [2560.0, 134.0], [2622.0, 124.0], [2566.0, 121.0], [2564.0, 122.0], [2614.0, 131.0], [2616.0, 125.0], [2618.0, 124.0], [2612.0, 124.0], [2610.0, 132.0], [2576.0, 133.0], [2652.0, 130.0], [2648.0, 121.0], [2646.0, 125.0], [2578.0, 138.0], [2580.0, 119.0], [2584.0, 128.0], [2666.0, 109.0], [2594.0, 125.0], [2592.0, 117.0], [2606.0, 118.0], [2602.0, 128.0], [2604.0, 109.0], [2624.0, 117.0], [2630.0, 105.0], [2632.0, 110.0], [2682.0, 106.0], [2670.0, 120.0], [2678.0, 127.0], [2674.0, 117.0], [2688.0, 120.0], [2700.0, 142.0], [2692.0, 115.0], [2744.0, 121.0], [2802.0, 118.0], [2766.0, 109.0], [2720.0, 116.0], [2718.0, 110.0], [2788.0, 106.0], [2732.0, 121.0], [2730.0, 114.0], [2722.0, 103.0], [2708.0, 128.0], [2712.0, 103.0], [2714.0, 118.0], [2770.0, 114.0], [2866.0, 115.0], [2838.0, 112.0], [2894.0, 101.5], [2918.0, 120.0], [2880.0, 126.0], [2854.0, 107.0], [2932.0, 83.0], [2816.0, 102.0], [2972.0, 115.0], [3002.0, 107.0], [3090.0, 106.0], [2159.0, 133.0], [2127.0, 154.0], [2073.0, 158.0], [2115.0, 136.0], [2125.0, 123.0], [2129.0, 155.0], [2089.0, 146.0], [2087.0, 173.0], [2081.0, 169.0], [2085.0, 155.5], [2143.0, 138.5], [2139.0, 177.0], [2133.0, 140.0], [2121.0, 107.0], [2057.0, 127.0], [2049.0, 146.0], [2107.0, 124.0], [2099.0, 149.0], [2097.0, 125.0], [2109.0, 132.0], [2145.0, 129.0], [2153.0, 140.0], [2155.0, 151.0], [2157.0, 133.0], [2173.0, 126.0], [2175.0, 147.0], [2167.0, 141.0], [2171.0, 137.0], [2075.0, 151.0], [2069.0, 159.0], [2071.0, 144.0], [2065.0, 144.0], [2061.0, 146.0], [2059.0, 137.0], [2201.0, 146.0], [2205.0, 103.0], [2207.0, 129.0], [2199.0, 178.5], [2181.0, 143.0], [2193.0, 136.0], [2191.0, 136.0], [2185.0, 152.0], [2217.0, 141.0], [2211.0, 128.0], [2209.0, 128.0], [2277.0, 124.0], [2273.0, 147.0], [2283.0, 131.0], [2293.0, 129.0], [2289.0, 104.0], [2285.0, 103.0], [2287.0, 127.0], [2299.0, 141.0], [2241.0, 133.0], [2303.0, 114.0], [2281.0, 137.0], [2279.0, 127.0], [2247.0, 139.0], [2257.0, 150.0], [2225.0, 137.0], [2177.0, 140.0], [2235.0, 155.0], [2229.0, 123.0], [2227.0, 119.0], [2231.0, 143.0], [2333.0, 119.0], [2421.0, 132.0], [2373.0, 136.0], [2381.0, 136.0], [2375.0, 137.0], [2377.0, 138.0], [2383.0, 124.0], [2385.0, 132.0], [2387.0, 127.0], [2393.0, 132.0], [2391.0, 125.0], [2395.0, 130.0], [2397.0, 135.0], [2399.0, 121.0], [2315.0, 155.0], [2313.0, 124.0], [2317.0, 133.0], [2319.0, 135.0], [2323.0, 146.0], [2329.0, 128.0], [2335.0, 141.0], [2427.0, 146.0], [2431.0, 111.0], [2425.0, 128.0], [2403.0, 126.0], [2407.0, 129.0], [2409.0, 135.0], [2413.0, 115.0], [2419.0, 130.0], [2365.0, 128.0], [2363.0, 134.0], [2359.0, 137.0], [2361.0, 128.0], [2357.0, 133.0], [2367.0, 151.0], [2353.0, 148.0], [2349.0, 138.0], [2347.0, 130.0], [2351.0, 124.0], [2343.0, 130.0], [2345.0, 145.0], [2341.0, 138.0], [2339.0, 139.0], [2337.0, 132.0], [2433.0, 121.0], [2447.0, 144.0], [2537.0, 119.0], [2545.0, 130.0], [2467.0, 122.0], [2469.0, 137.0], [2471.0, 115.0], [2465.0, 130.0], [2483.0, 143.0], [2487.0, 139.0], [2485.0, 135.0], [2481.0, 134.0], [2549.0, 133.0], [2557.0, 124.0], [2551.0, 132.0], [2521.0, 127.0], [2515.0, 121.0], [2513.0, 135.0], [2523.0, 123.0], [2527.0, 131.0], [2507.0, 124.0], [2505.0, 123.0], [2475.0, 126.0], [2477.0, 134.0], [2479.0, 128.0], [2495.0, 124.0], [2489.0, 117.0], [2437.0, 141.0], [2439.0, 117.0], [2441.0, 128.0], [2445.0, 133.0], [2443.0, 123.0], [2451.0, 133.0], [2455.0, 136.0], [2459.0, 122.0], [2461.0, 99.0], [2533.0, 138.0], [2577.0, 132.0], [2669.0, 119.0], [2607.0, 136.0], [2605.0, 114.0], [2621.0, 110.0], [2563.0, 125.0], [2611.0, 110.0], [2613.0, 122.0], [2615.0, 111.0], [2579.0, 137.0], [2663.0, 124.0], [2643.0, 110.0], [2641.0, 118.0], [2653.0, 124.0], [2645.0, 111.0], [2637.0, 116.0], [2629.0, 124.0], [2631.0, 118.0], [2597.0, 118.0], [2603.0, 125.0], [2627.0, 124.0], [2679.0, 125.0], [2685.0, 112.0], [2675.0, 125.0], [2719.0, 117.0], [2707.0, 126.0], [2757.0, 125.0], [2809.0, 110.0], [2793.0, 97.0], [2785.0, 116.0], [2789.0, 116.0], [2739.0, 121.0], [2723.0, 113.0], [2725.0, 117.0], [2729.0, 107.5], [2733.0, 111.0], [2731.0, 101.5], [2713.0, 121.0], [2709.0, 104.0], [2777.0, 122.0], [2769.0, 105.0], [2915.0, 111.0], [2845.0, 116.0], [2875.0, 110.0], [2827.0, 128.0], [2931.0, 117.0], [2881.0, 114.0], [2939.0, 109.5], [2859.0, 100.0], [2905.0, 97.0], [3011.0, 110.0], [2991.0, 99.0], [3035.0, 111.0], [3033.0, 80.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[4.0, 3681.0], [34.0, 3534.0], [43.0, 5986.0], [82.0, 4174.0], [89.0, 3706.0], [99.0, 3559.0], [131.0, 3589.0], [146.0, 3628.0], [220.0, 3604.0], [218.0, 3561.0], [239.0, 3616.0], [278.0, 3571.5], [281.0, 3531.0], [299.0, 4340.0], [318.0, 3746.0], [333.0, 3545.0], [383.0, 4112.5], [413.0, 3601.0], [412.0, 3538.0], [403.0, 4065.0], [409.0, 3711.0], [443.0, 4320.5], [483.0, 3640.5], [491.0, 4143.0], [516.0, 3735.0], [562.0, 3556.5], [561.0, 3677.0], [654.0, 3724.0], [723.0, 3534.0], [828.0, 3963.5], [889.0, 3544.5], [893.0, 3625.0], [908.0, 4774.0], [936.0, 4043.0], [965.0, 4083.0], [985.0, 4197.0], [992.0, 3650.0], [1022.0, 4229.0], [1081.0, 3726.0], [1055.0, 4131.0], [1128.0, 4093.0], [1134.0, 4239.0], [1136.0, 3919.0], [1169.0, 3730.0], [1260.0, 3961.0], [1266.0, 3959.0], [1270.0, 3589.0], [1229.0, 3720.5], [1388.0, 4305.5], [1382.0, 4141.5], [1433.0, 4396.5], [1526.0, 4568.5], [1520.0, 5526.0], [1524.0, 3698.0], [1560.0, 4376.0], [1549.0, 3746.5], [1640.0, 3954.0], [1603.0, 3952.5], [1633.0, 4426.0], [1643.0, 3994.0], [1699.0, 3895.0], [1693.0, 3715.0], [1694.0, 4426.0], [1676.0, 3663.0], [1682.0, 3923.0], [1683.0, 3825.0], [1685.0, 3594.0], [1734.0, 3783.0], [1752.0, 4396.0], [1728.0, 3743.5], [1845.0, 4003.5], [1813.0, 4080.0], [1838.0, 4120.0], [1855.0, 3888.0], [1837.0, 3883.0], [1841.0, 3706.0], [1824.0, 4058.0], [1912.0, 4033.0], [1880.0, 3695.0], [1894.0, 3927.0], [1903.0, 4461.0], [1902.0, 4310.0], [1917.0, 3860.0], [1867.0, 4011.0], [1909.0, 3899.0], [1915.0, 3884.5], [1863.0, 3805.5], [1862.0, 4990.0], [1956.0, 3844.0], [1940.0, 4001.5], [1926.0, 3917.0], [1923.0, 3965.0], [1973.0, 3877.0], [1948.0, 3957.5], [1975.0, 4503.0], [2001.0, 3965.0], [2035.0, 4068.0], [1992.0, 3962.0], [2005.0, 3975.5], [2046.0, 3852.5], [2002.0, 3737.0], [2017.0, 3685.0], [2021.0, 3905.0], [2145.0, 4653.5], [2048.0, 4246.0], [2115.0, 4069.0], [2132.0, 3714.0], [2122.0, 3813.0], [2118.0, 3867.0], [2133.0, 3785.0], [2075.0, 3942.0], [2072.0, 3876.0], [2070.0, 3824.0], [2071.0, 3910.0], [2144.0, 3591.0], [2069.0, 3975.0], [2106.0, 3581.5], [2094.0, 4088.5], [2097.0, 3842.0], [2107.0, 3857.0], [2085.0, 4005.0], [2090.0, 3992.0], [2157.0, 4054.0], [2153.0, 3808.0], [2155.0, 3909.0], [2162.0, 3837.0], [2171.0, 3750.5], [2159.0, 3778.5], [2160.0, 3719.5], [2066.0, 3808.0], [2062.0, 3704.0], [2061.0, 3692.0], [2065.0, 3693.0], [2199.0, 3770.0], [2178.0, 4518.5], [2236.0, 3853.5], [2238.0, 3611.0], [2177.0, 4355.0], [2181.0, 3747.5], [2186.0, 3725.0], [2193.0, 3715.0], [2190.0, 4026.5], [2205.0, 4163.0], [2273.0, 3778.0], [2207.0, 3868.0], [2226.0, 3799.0], [2231.0, 3890.0], [2227.0, 3614.0], [2225.0, 3710.0], [2210.0, 4475.0], [2211.0, 3748.5], [2217.0, 3768.0], [2222.0, 4015.0], [2277.0, 5416.0], [2235.0, 3732.0], [2257.0, 3909.0], [2256.0, 3763.0], [2268.0, 4606.0], [2264.0, 4152.0], [2302.0, 3704.0], [2303.0, 3916.0], [2300.0, 3780.0], [2289.0, 3736.0], [2298.0, 3708.0], [2282.0, 4057.5], [2280.0, 3657.0], [2285.0, 3921.5], [2240.0, 3875.5], [2400.0, 3963.0], [2368.0, 3815.0], [2431.0, 3633.5], [2372.0, 3883.5], [2336.0, 3825.5], [2403.0, 3815.0], [2406.0, 3737.0], [2412.0, 3799.5], [2413.0, 3580.0], [2424.0, 3733.5], [2420.0, 4076.0], [2414.0, 3810.0], [2409.0, 3952.5], [2342.0, 3581.5], [2306.0, 3838.0], [2365.0, 3848.0], [2304.0, 3694.5], [2362.0, 4659.0], [2360.0, 3882.0], [2357.0, 3736.5], [2344.0, 3723.5], [2346.0, 3825.0], [2352.0, 3816.5], [2317.0, 3897.0], [2314.0, 3722.0], [2315.0, 3813.0], [2333.0, 4055.5], [2326.0, 3638.0], [2322.0, 3879.0], [2337.0, 3967.0], [2425.0, 3930.0], [2373.0, 3965.0], [2386.0, 3770.0], [2385.0, 3850.0], [2383.0, 3953.0], [2381.0, 3930.0], [2376.0, 3745.0], [2374.0, 3720.0], [2380.0, 3834.0], [2387.0, 3988.0], [2395.0, 3912.0], [2394.0, 3961.0], [2391.0, 3725.5], [2397.0, 3681.0], [2398.0, 3850.0], [2399.0, 3818.0], [2450.0, 3821.0], [2441.0, 3872.0], [2537.0, 3892.0], [2494.0, 3873.0], [2487.0, 3798.0], [2490.0, 3988.0], [2465.0, 3833.5], [2479.0, 3976.0], [2470.0, 3813.0], [2476.0, 3809.0], [2469.0, 3635.0], [2485.0, 3862.0], [2437.0, 4470.0], [2439.0, 3847.0], [2433.0, 3806.0], [2495.0, 3575.0], [2447.0, 3802.5], [2442.0, 3874.0], [2550.0, 3827.0], [2551.0, 3769.0], [2496.0, 3699.0], [2521.0, 3713.0], [2516.0, 3749.0], [2523.0, 3813.0], [2527.0, 3738.0], [2505.0, 3883.0], [2513.0, 3910.5], [2507.0, 3974.0], [2515.0, 3853.0], [2530.0, 4149.0], [2536.0, 3747.0], [2533.0, 3613.0], [2532.0, 3859.0], [2451.0, 3817.0], [2452.0, 3682.0], [2528.0, 3694.0], [2454.0, 4375.0], [2614.0, 3717.0], [2682.0, 3768.0], [2675.0, 3719.0], [2568.0, 3869.0], [2580.0, 3748.5], [2574.0, 3770.0], [2563.0, 4431.5], [2618.0, 3706.0], [2615.0, 4028.0], [2560.0, 3785.5], [2663.0, 3703.5], [2594.0, 3739.0], [2592.0, 4016.0], [2602.0, 3860.5], [2605.0, 3835.5], [2606.0, 4112.0], [2624.0, 3757.0], [2629.0, 3750.5], [2627.0, 3788.0], [2610.0, 3994.0], [2611.0, 3756.0], [2729.0, 3817.0], [2720.0, 3884.0], [2718.0, 3849.0], [2770.0, 4028.0], [2732.0, 3704.0], [2766.0, 3655.0], [2793.0, 3853.0], [2722.0, 3841.0], [2725.0, 3702.5], [2714.0, 3851.5], [2731.0, 3906.0], [2827.0, 4345.0], [2866.0, 3808.0], [2931.0, 3851.0], [2875.0, 4342.0], [2915.0, 3872.0], [2854.0, 3678.0], [2939.0, 3713.0], [2905.0, 3742.0], [2859.0, 3830.5], [2932.0, 3726.5], [3011.0, 4077.5], [2972.0, 3924.0], [2991.0, 3739.0], [3033.0, 3789.0], [3090.0, 3724.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 3090.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 80.0, "minX": 4.0, "maxY": 5986.0, "series": [{"data": [[4.0, 3124.0], [5.0, 3171.0], [34.0, 3218.0], [43.0, 3198.5], [48.0, 3132.5], [60.0, 3195.0], [63.0, 2176.0], [75.0, 3030.0], [72.0, 3158.0], [76.0, 3147.5], [78.0, 3160.0], [82.0, 3150.0], [89.0, 3041.5], [90.0, 3145.0], [93.0, 3184.0], [99.0, 3135.5], [110.0, 3131.0], [119.0, 3131.0], [121.0, 3146.0], [127.0, 3136.0], [124.0, 3243.0], [130.0, 3234.0], [131.0, 3138.0], [140.0, 3201.0], [146.0, 3156.0], [150.0, 3266.5], [155.0, 3365.0], [158.0, 3198.0], [159.0, 3141.0], [169.0, 3027.0], [180.0, 3143.0], [189.0, 3189.0], [185.0, 3333.0], [191.0, 3189.0], [184.0, 3180.0], [193.0, 3143.0], [192.0, 3144.0], [195.0, 3126.0], [194.0, 3203.5], [201.0, 3180.0], [207.0, 3139.0], [206.0, 3280.5], [212.0, 3135.0], [210.0, 3224.0], [220.0, 3007.0], [221.0, 3228.0], [218.0, 3157.0], [219.0, 3178.0], [236.0, 3144.0], [234.0, 3181.0], [232.0, 3176.0], [239.0, 3128.0], [233.0, 3183.0], [242.0, 3150.0], [246.0, 3143.0], [264.0, 3098.0], [266.0, 3172.0], [265.0, 3127.0], [284.0, 3113.0], [278.0, 3180.0], [286.0, 3131.0], [281.0, 3138.0], [299.0, 3116.0], [301.0, 3204.0], [290.0, 3145.0], [318.0, 3264.5], [326.0, 2464.0], [333.0, 3081.0], [331.0, 3195.0], [320.0, 3226.0], [346.0, 3190.0], [354.0, 2194.0], [363.0, 3136.0], [383.0, 3023.0], [371.0, 3146.0], [413.0, 3062.0], [412.0, 3274.0], [403.0, 3359.0], [409.0, 3121.0], [408.0, 3195.5], [443.0, 241.0], [448.0, 3121.0], [471.0, 3044.0], [483.0, 3103.0], [484.0, 3142.5], [491.0, 213.0], [510.0, 3164.5], [521.0, 3045.0], [516.0, 3138.0], [512.0, 2660.0], [562.0, 2082.0], [561.0, 3108.0], [573.0, 170.0], [592.0, 3134.0], [587.0, 3132.0], [612.0, 3126.0], [631.0, 156.0], [620.0, 3156.0], [654.0, 153.0], [691.0, 191.0], [698.0, 2052.0], [692.0, 3155.0], [709.0, 246.0], [720.0, 184.0], [717.0, 2111.0], [723.0, 129.0], [764.0, 183.0], [776.0, 303.0], [827.0, 153.0], [828.0, 358.5], [848.0, 2162.0], [889.0, 156.0], [870.0, 240.0], [880.0, 200.0], [893.0, 2396.0], [908.0, 359.0], [915.0, 176.0], [936.0, 255.0], [946.0, 2075.0], [982.0, 2109.5], [990.0, 124.0], [965.0, 109.0], [985.0, 181.0], [992.0, 126.0], [1022.0, 405.0], [998.0, 2093.0], [1081.0, 116.0], [1037.0, 281.0], [1055.0, 169.0], [1073.0, 119.0], [1068.0, 150.0], [1042.0, 250.0], [1128.0, 306.0], [1134.0, 632.5], [1088.0, 156.0], [1136.0, 322.0], [1144.0, 162.0], [1165.0, 284.0], [1169.0, 144.5], [1207.0, 196.0], [1266.0, 269.0], [1260.0, 156.0], [1225.0, 124.0], [1275.0, 215.0], [1236.0, 257.0], [1270.0, 194.0], [1245.0, 252.0], [1227.0, 121.0], [1229.0, 149.0], [1339.0, 128.0], [1296.0, 184.0], [1407.0, 119.0], [1378.0, 123.0], [1376.0, 164.0], [1388.0, 233.0], [1400.0, 224.0], [1382.0, 196.0], [1429.0, 170.0], [1433.0, 208.0], [1409.0, 195.0], [1425.0, 183.0], [1520.0, 252.0], [1526.0, 165.5], [1501.0, 207.0], [1498.0, 366.0], [1516.0, 204.0], [1524.0, 111.0], [1522.0, 180.0], [1533.0, 173.0], [1495.0, 177.0], [1503.0, 158.0], [1482.0, 186.0], [1475.0, 169.0], [1552.0, 162.5], [1560.0, 159.0], [1558.0, 185.0], [1549.0, 160.0], [1556.0, 178.0], [1605.0, 194.0], [1661.0, 182.0], [1640.0, 134.0], [1603.0, 184.0], [1633.0, 205.0], [1650.0, 192.0], [1643.0, 175.0], [1638.0, 185.0], [1667.0, 219.0], [1699.0, 171.0], [1702.0, 150.0], [1703.0, 167.0], [1689.0, 209.0], [1707.0, 183.0], [1693.0, 149.0], [1709.0, 200.0], [1683.0, 167.0], [1685.0, 188.0], [1682.0, 165.0], [1680.0, 152.0], [1694.0, 179.0], [1697.0, 193.0], [1676.0, 169.0], [1717.0, 149.0], [1748.0, 156.0], [1734.0, 204.0], [1783.0, 161.0], [1752.0, 191.0], [1777.0, 162.0], [1728.0, 179.0], [1849.0, 168.0], [1845.0, 216.0], [1813.0, 149.0], [1838.0, 193.0], [1811.0, 179.0], [1809.0, 140.0], [1839.0, 143.0], [1847.0, 175.0], [1848.0, 136.0], [1824.0, 140.0], [1855.0, 145.0], [1851.0, 156.0], [1854.0, 168.0], [1833.0, 194.0], [1831.0, 129.0], [1837.0, 181.0], [1795.0, 155.0], [1793.0, 152.0], [1841.0, 134.0], [1865.0, 169.0], [1880.0, 152.0], [1895.0, 163.0], [1901.0, 159.0], [1903.0, 129.0], [1902.0, 135.0], [1879.0, 175.0], [1872.0, 156.0], [1883.0, 162.0], [1862.0, 143.0], [1860.0, 166.0], [1856.0, 157.0], [1864.0, 163.0], [1863.0, 159.0], [1912.0, 169.0], [1894.0, 167.0], [1918.0, 169.0], [1917.0, 161.0], [1915.0, 151.0], [1870.0, 134.0], [1909.0, 142.0], [1906.0, 171.5], [1867.0, 160.0], [1869.0, 166.0], [1927.0, 176.0], [1921.0, 156.0], [1923.0, 134.0], [1925.0, 152.0], [1926.0, 116.0], [1956.0, 151.0], [1920.0, 158.0], [1964.0, 157.0], [1958.0, 176.0], [1960.0, 145.0], [1937.0, 149.0], [1930.0, 146.0], [1931.0, 130.0], [1972.0, 148.0], [1973.0, 104.0], [1975.0, 137.0], [1940.0, 157.0], [1942.0, 174.0], [1944.0, 153.0], [1948.0, 112.0], [1980.0, 154.0], [1952.0, 119.0], [1979.0, 149.0], [1987.0, 150.0], [2001.0, 167.0], [2002.0, 115.0], [2035.0, 152.0], [2007.0, 178.0], [2011.0, 132.0], [1985.0, 153.0], [2016.0, 136.0], [2046.0, 117.0], [2044.0, 148.0], [2037.0, 143.0], [1992.0, 152.0], [2005.0, 165.0], [1994.0, 171.0], [1996.0, 184.0], [2027.0, 114.0], [2024.0, 143.0], [2017.0, 125.0], [2021.0, 151.0], [2029.0, 149.0], [2048.0, 161.0], [2126.0, 157.0], [2096.0, 150.0], [2092.0, 143.0], [2094.0, 117.0], [2090.0, 151.0], [2088.0, 161.0], [2106.0, 147.0], [2110.0, 148.0], [2100.0, 123.5], [2098.0, 146.0], [2138.0, 118.5], [2136.0, 160.0], [2122.0, 144.0], [2118.0, 129.0], [2114.0, 127.0], [2112.0, 129.0], [2162.0, 145.0], [2074.0, 161.0], [2072.0, 145.0], [2070.0, 135.0], [2144.0, 151.0], [2160.0, 135.0], [2064.0, 158.0], [2062.0, 137.0], [2066.0, 125.0], [2132.0, 133.0], [2130.0, 143.0], [2178.0, 144.0], [2300.0, 138.0], [2226.0, 142.0], [2282.0, 135.0], [2298.0, 109.0], [2272.0, 134.0], [2280.0, 131.0], [2194.0, 137.0], [2190.0, 98.0], [2186.0, 141.0], [2208.0, 142.0], [2212.0, 149.5], [2218.0, 139.0], [2210.0, 148.0], [2302.0, 123.0], [2240.0, 125.0], [2252.0, 146.0], [2242.0, 136.0], [2256.0, 127.0], [2254.0, 137.0], [2260.0, 126.0], [2268.0, 138.5], [2264.0, 135.0], [2270.0, 144.0], [2222.0, 136.0], [2220.0, 156.0], [2228.0, 158.0], [2230.0, 148.0], [2234.0, 149.0], [2236.0, 129.0], [2238.0, 125.0], [2400.0, 124.0], [2374.0, 141.0], [2372.0, 135.0], [2378.0, 143.0], [2376.0, 129.0], [2386.0, 137.0], [2380.0, 118.0], [2398.0, 124.0], [2394.0, 123.0], [2306.0, 147.0], [2368.0, 144.0], [2402.0, 140.0], [2308.0, 134.0], [2314.0, 130.0], [2326.0, 124.0], [2322.0, 136.0], [2336.0, 137.0], [2416.0, 134.0], [2414.0, 129.0], [2424.0, 126.0], [2420.0, 127.0], [2412.0, 126.0], [2406.0, 129.0], [2404.0, 128.0], [2342.0, 162.0], [2348.0, 152.0], [2304.0, 133.0], [2362.0, 143.0], [2360.0, 140.0], [2356.0, 132.0], [2352.0, 153.0], [2344.0, 141.0], [2346.0, 133.0], [2450.0, 117.0], [2492.0, 166.0], [2480.0, 146.0], [2490.0, 122.0], [2558.0, 128.0], [2556.0, 118.0], [2550.0, 131.0], [2542.0, 120.0], [2540.0, 116.5], [2498.0, 137.0], [2524.0, 128.0], [2518.0, 124.0], [2526.0, 101.0], [2510.0, 127.0], [2516.0, 121.0], [2504.0, 110.0], [2500.0, 120.0], [2496.0, 135.0], [2472.0, 124.0], [2476.0, 109.0], [2478.0, 129.0], [2470.0, 114.0], [2494.0, 139.0], [2432.0, 125.0], [2440.0, 138.0], [2442.0, 138.0], [2536.0, 127.0], [2538.0, 119.0], [2448.0, 127.0], [2446.0, 121.0], [2528.0, 124.0], [2462.0, 146.0], [2452.0, 112.0], [2454.0, 117.0], [2458.0, 125.0], [2530.0, 126.0], [2532.0, 133.0], [2534.0, 108.0], [2568.0, 125.0], [2574.0, 118.0], [2560.0, 134.0], [2622.0, 124.0], [2566.0, 121.0], [2564.0, 122.0], [2614.0, 131.0], [2616.0, 125.0], [2618.0, 124.0], [2612.0, 124.0], [2610.0, 132.0], [2576.0, 133.0], [2652.0, 130.0], [2648.0, 121.0], [2646.0, 125.0], [2578.0, 138.0], [2580.0, 119.0], [2584.0, 128.0], [2666.0, 109.0], [2594.0, 125.0], [2592.0, 117.0], [2606.0, 118.0], [2602.0, 128.0], [2604.0, 109.0], [2624.0, 117.0], [2630.0, 105.0], [2632.0, 110.0], [2682.0, 106.0], [2670.0, 120.0], [2678.0, 127.0], [2674.0, 117.0], [2688.0, 120.0], [2700.0, 142.0], [2692.0, 115.0], [2744.0, 121.0], [2802.0, 118.0], [2766.0, 109.0], [2720.0, 116.0], [2718.0, 110.0], [2788.0, 106.0], [2732.0, 121.0], [2730.0, 114.0], [2722.0, 103.0], [2708.0, 128.0], [2712.0, 103.0], [2714.0, 118.0], [2770.0, 114.0], [2866.0, 115.0], [2838.0, 112.0], [2894.0, 101.5], [2918.0, 120.0], [2880.0, 126.0], [2854.0, 107.0], [2932.0, 83.0], [2816.0, 102.0], [2972.0, 115.0], [3002.0, 107.0], [3090.0, 106.0], [2159.0, 133.0], [2127.0, 154.0], [2073.0, 158.0], [2115.0, 136.0], [2125.0, 123.0], [2129.0, 155.0], [2089.0, 146.0], [2087.0, 173.0], [2081.0, 169.0], [2085.0, 155.5], [2143.0, 138.5], [2139.0, 177.0], [2133.0, 140.0], [2121.0, 107.0], [2057.0, 127.0], [2049.0, 146.0], [2107.0, 124.0], [2099.0, 149.0], [2097.0, 125.0], [2109.0, 132.0], [2145.0, 129.0], [2153.0, 140.0], [2155.0, 151.0], [2157.0, 133.0], [2173.0, 126.0], [2175.0, 147.0], [2167.0, 141.0], [2171.0, 137.0], [2075.0, 151.0], [2069.0, 159.0], [2071.0, 144.0], [2065.0, 144.0], [2061.0, 146.0], [2059.0, 137.0], [2201.0, 146.0], [2205.0, 103.0], [2207.0, 129.0], [2199.0, 178.5], [2181.0, 143.0], [2193.0, 136.0], [2191.0, 136.0], [2185.0, 152.0], [2217.0, 141.0], [2211.0, 128.0], [2209.0, 128.0], [2277.0, 124.0], [2273.0, 147.0], [2283.0, 131.0], [2293.0, 129.0], [2289.0, 104.0], [2285.0, 103.0], [2287.0, 127.0], [2299.0, 141.0], [2241.0, 133.0], [2303.0, 114.0], [2281.0, 137.0], [2279.0, 127.0], [2247.0, 139.0], [2257.0, 150.0], [2225.0, 137.0], [2177.0, 140.0], [2235.0, 155.0], [2229.0, 123.0], [2227.0, 119.0], [2231.0, 143.0], [2333.0, 119.0], [2421.0, 132.0], [2373.0, 136.0], [2381.0, 136.0], [2375.0, 137.0], [2377.0, 138.0], [2383.0, 124.0], [2385.0, 132.0], [2387.0, 127.0], [2393.0, 132.0], [2391.0, 125.0], [2395.0, 130.0], [2397.0, 135.0], [2399.0, 121.0], [2315.0, 155.0], [2313.0, 124.0], [2317.0, 133.0], [2319.0, 135.0], [2323.0, 146.0], [2329.0, 128.0], [2335.0, 141.0], [2427.0, 146.0], [2431.0, 111.0], [2425.0, 128.0], [2403.0, 126.0], [2407.0, 129.0], [2409.0, 135.0], [2413.0, 115.0], [2419.0, 130.0], [2365.0, 128.0], [2363.0, 134.0], [2359.0, 137.0], [2361.0, 128.0], [2357.0, 133.0], [2367.0, 151.0], [2353.0, 148.0], [2349.0, 138.0], [2347.0, 130.0], [2351.0, 124.0], [2343.0, 130.0], [2345.0, 145.0], [2341.0, 138.0], [2339.0, 139.0], [2337.0, 132.0], [2433.0, 121.0], [2447.0, 144.0], [2537.0, 119.0], [2545.0, 130.0], [2467.0, 122.0], [2469.0, 137.0], [2471.0, 115.0], [2465.0, 130.0], [2483.0, 143.0], [2487.0, 139.0], [2485.0, 135.0], [2481.0, 134.0], [2549.0, 133.0], [2557.0, 124.0], [2551.0, 132.0], [2521.0, 127.0], [2515.0, 121.0], [2513.0, 135.0], [2523.0, 123.0], [2527.0, 131.0], [2507.0, 124.0], [2505.0, 123.0], [2475.0, 126.0], [2477.0, 134.0], [2479.0, 128.0], [2495.0, 124.0], [2489.0, 117.0], [2437.0, 141.0], [2439.0, 117.0], [2441.0, 128.0], [2445.0, 133.0], [2443.0, 123.0], [2451.0, 133.0], [2455.0, 136.0], [2459.0, 122.0], [2461.0, 99.0], [2533.0, 138.0], [2577.0, 132.0], [2669.0, 119.0], [2607.0, 136.0], [2605.0, 114.0], [2621.0, 110.0], [2563.0, 125.0], [2611.0, 110.0], [2613.0, 122.0], [2615.0, 111.0], [2579.0, 137.0], [2663.0, 124.0], [2643.0, 110.0], [2641.0, 118.0], [2653.0, 124.0], [2645.0, 111.0], [2637.0, 116.0], [2629.0, 124.0], [2631.0, 118.0], [2597.0, 118.0], [2603.0, 125.0], [2627.0, 124.0], [2679.0, 125.0], [2685.0, 112.0], [2675.0, 125.0], [2719.0, 117.0], [2707.0, 126.0], [2757.0, 125.0], [2809.0, 110.0], [2793.0, 97.0], [2785.0, 116.0], [2789.0, 116.0], [2739.0, 121.0], [2723.0, 113.0], [2725.0, 117.0], [2729.0, 107.5], [2733.0, 111.0], [2731.0, 101.5], [2713.0, 121.0], [2709.0, 104.0], [2777.0, 122.0], [2769.0, 105.0], [2915.0, 111.0], [2845.0, 116.0], [2875.0, 110.0], [2827.0, 128.0], [2931.0, 117.0], [2881.0, 114.0], [2939.0, 109.5], [2859.0, 100.0], [2905.0, 97.0], [3011.0, 110.0], [2991.0, 99.0], [3035.0, 111.0], [3033.0, 80.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[4.0, 3681.0], [34.0, 3534.0], [43.0, 5986.0], [82.0, 4174.0], [89.0, 3706.0], [99.0, 3559.0], [131.0, 3589.0], [146.0, 3628.0], [220.0, 3604.0], [218.0, 3561.0], [239.0, 3616.0], [278.0, 3571.5], [281.0, 3531.0], [299.0, 4340.0], [318.0, 3746.0], [333.0, 3545.0], [383.0, 4112.5], [413.0, 3601.0], [412.0, 3538.0], [403.0, 4065.0], [409.0, 3711.0], [443.0, 4320.5], [483.0, 3640.0], [491.0, 4143.0], [516.0, 3735.0], [562.0, 3556.5], [561.0, 3677.0], [654.0, 3724.0], [723.0, 3534.0], [828.0, 3963.5], [889.0, 3544.5], [893.0, 3625.0], [908.0, 4774.0], [936.0, 4043.0], [965.0, 4083.0], [985.0, 4197.0], [992.0, 3650.0], [1022.0, 4229.0], [1081.0, 3726.0], [1055.0, 4131.0], [1128.0, 4093.0], [1134.0, 4239.0], [1136.0, 3919.0], [1169.0, 3730.0], [1260.0, 3961.0], [1266.0, 3959.0], [1270.0, 3589.0], [1229.0, 3720.5], [1388.0, 4305.5], [1382.0, 4141.5], [1433.0, 4396.5], [1526.0, 4568.5], [1520.0, 5526.0], [1524.0, 3698.0], [1560.0, 4376.0], [1549.0, 3746.5], [1640.0, 3954.0], [1603.0, 3952.5], [1633.0, 4426.0], [1643.0, 3994.0], [1699.0, 3895.0], [1693.0, 3715.0], [1694.0, 4426.0], [1676.0, 3663.0], [1682.0, 3923.0], [1683.0, 3825.0], [1685.0, 3594.0], [1734.0, 3783.0], [1752.0, 4396.0], [1728.0, 3743.5], [1845.0, 4003.5], [1813.0, 4080.0], [1838.0, 4120.0], [1855.0, 3888.0], [1837.0, 3883.0], [1841.0, 3706.0], [1824.0, 4058.0], [1912.0, 4033.0], [1880.0, 3695.0], [1894.0, 3927.0], [1903.0, 4461.0], [1902.0, 4310.0], [1917.0, 3860.0], [1867.0, 4011.0], [1909.0, 3899.0], [1915.0, 3884.5], [1863.0, 3805.5], [1862.0, 4990.0], [1956.0, 3844.0], [1940.0, 4001.5], [1926.0, 3917.0], [1923.0, 3965.0], [1973.0, 3877.0], [1948.0, 3957.5], [1975.0, 4503.0], [2001.0, 3965.0], [2035.0, 4068.0], [1992.0, 3962.0], [2005.0, 3975.5], [2046.0, 3852.5], [2002.0, 3737.0], [2017.0, 3685.0], [2021.0, 3905.0], [2145.0, 4653.5], [2048.0, 4246.0], [2115.0, 4069.0], [2132.0, 3714.0], [2122.0, 3813.0], [2118.0, 3867.0], [2133.0, 3785.0], [2075.0, 3942.0], [2072.0, 3876.0], [2070.0, 3824.0], [2071.0, 3910.0], [2144.0, 3591.0], [2069.0, 3975.0], [2106.0, 3581.5], [2094.0, 4088.5], [2097.0, 3842.0], [2107.0, 3857.0], [2085.0, 4005.0], [2090.0, 3992.0], [2157.0, 4054.0], [2153.0, 3808.0], [2155.0, 3909.0], [2162.0, 3837.0], [2171.0, 3750.5], [2159.0, 3778.5], [2160.0, 3719.5], [2066.0, 3808.0], [2062.0, 3704.0], [2061.0, 3692.0], [2065.0, 3693.0], [2199.0, 3770.0], [2178.0, 4518.5], [2236.0, 3853.5], [2238.0, 3611.0], [2177.0, 4355.0], [2181.0, 3747.5], [2186.0, 3725.0], [2193.0, 3715.0], [2190.0, 4026.5], [2205.0, 4163.0], [2273.0, 3778.0], [2207.0, 3868.0], [2226.0, 3799.0], [2231.0, 3890.0], [2227.0, 3614.0], [2225.0, 3710.0], [2210.0, 4475.0], [2211.0, 3748.5], [2217.0, 3768.0], [2222.0, 4015.0], [2277.0, 5416.0], [2235.0, 3732.0], [2257.0, 3909.0], [2256.0, 3763.0], [2268.0, 4606.0], [2264.0, 4152.0], [2302.0, 3704.0], [2303.0, 3916.0], [2300.0, 3780.0], [2289.0, 3736.0], [2298.0, 3708.0], [2282.0, 4057.5], [2280.0, 3657.0], [2285.0, 3921.5], [2240.0, 3875.5], [2400.0, 3963.0], [2368.0, 3815.0], [2431.0, 3633.5], [2372.0, 3883.5], [2336.0, 3825.5], [2403.0, 3815.0], [2406.0, 3737.0], [2412.0, 3799.5], [2413.0, 3580.0], [2424.0, 3733.5], [2420.0, 4076.0], [2414.0, 3810.0], [2409.0, 3952.5], [2342.0, 3581.5], [2306.0, 3838.0], [2365.0, 3848.0], [2304.0, 3694.5], [2362.0, 4659.0], [2360.0, 3882.0], [2357.0, 3736.5], [2344.0, 3723.5], [2346.0, 3825.0], [2352.0, 3816.5], [2317.0, 3897.0], [2314.0, 3722.0], [2315.0, 3813.0], [2333.0, 4055.5], [2326.0, 3638.0], [2322.0, 3879.0], [2337.0, 3967.0], [2425.0, 3930.0], [2373.0, 3965.0], [2386.0, 3770.0], [2385.0, 3850.0], [2383.0, 3953.0], [2381.0, 3930.0], [2376.0, 3745.0], [2374.0, 3720.0], [2380.0, 3834.0], [2387.0, 3988.0], [2395.0, 3912.0], [2394.0, 3961.0], [2391.0, 3725.5], [2397.0, 3681.0], [2398.0, 3850.0], [2399.0, 3818.0], [2450.0, 3821.0], [2441.0, 3872.0], [2537.0, 3892.0], [2494.0, 3873.0], [2487.0, 3798.0], [2490.0, 3988.0], [2465.0, 3833.5], [2479.0, 3976.0], [2470.0, 3813.0], [2476.0, 3809.0], [2469.0, 3635.0], [2485.0, 3862.0], [2437.0, 4470.0], [2439.0, 3847.0], [2433.0, 3806.0], [2495.0, 3575.0], [2447.0, 3802.0], [2442.0, 3874.0], [2550.0, 3827.0], [2551.0, 3769.0], [2496.0, 3699.0], [2521.0, 3713.0], [2516.0, 3749.0], [2523.0, 3813.0], [2527.0, 3738.0], [2505.0, 3883.0], [2513.0, 3910.5], [2507.0, 3974.0], [2515.0, 3853.0], [2530.0, 4149.0], [2536.0, 3747.0], [2533.0, 3613.0], [2532.0, 3859.0], [2451.0, 3817.0], [2452.0, 3682.0], [2528.0, 3694.0], [2454.0, 4375.0], [2614.0, 3717.0], [2682.0, 3768.0], [2675.0, 3719.0], [2568.0, 3869.0], [2580.0, 3748.5], [2574.0, 3770.0], [2563.0, 4431.5], [2618.0, 3706.0], [2615.0, 4028.0], [2560.0, 3785.5], [2663.0, 3703.5], [2594.0, 3739.0], [2592.0, 4016.0], [2602.0, 3860.5], [2605.0, 3835.5], [2606.0, 4112.0], [2624.0, 3757.0], [2629.0, 3750.5], [2627.0, 3788.0], [2610.0, 3994.0], [2611.0, 3756.0], [2729.0, 3817.0], [2720.0, 3884.0], [2718.0, 3849.0], [2770.0, 4028.0], [2732.0, 3704.0], [2766.0, 3655.0], [2793.0, 3853.0], [2722.0, 3841.0], [2725.0, 3702.5], [2714.0, 3851.5], [2731.0, 3906.0], [2827.0, 4345.0], [2866.0, 3808.0], [2931.0, 3851.0], [2875.0, 4342.0], [2915.0, 3872.0], [2854.0, 3678.0], [2939.0, 3713.0], [2905.0, 3742.0], [2859.0, 3830.5], [2932.0, 3726.5], [3011.0, 4077.5], [2972.0, 3924.0], [2991.0, 3739.0], [3033.0, 3789.0], [3090.0, 3724.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 3090.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 912.7833333333333, "minX": 1.6763832E12, "maxY": 2360.633333333333, "series": [{"data": [[1.67638398E12, 1594.1833333333334], [1.67638392E12, 2165.9166666666665], [1.67638362E12, 1421.7], [1.67638332E12, 1782.5333333333333], [1.6763832E12, 2006.75], [1.67638326E12, 1475.3166666666666], [1.67638386E12, 1926.5333333333333], [1.67638356E12, 2203.35], [1.6763838E12, 1625.7666666666667], [1.67638344E12, 2360.633333333333], [1.6763835E12, 2229.3333333333335], [1.67638404E12, 912.7833333333333], [1.67638338E12, 2196.4], [1.67638374E12, 2057.983333333333], [1.67638368E12, 1791.8333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638404E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.9833333333333334, "minX": 1.6763832E12, "maxY": 2353.016666666667, "series": [{"data": [[1.67638398E12, 1582.2833333333333], [1.67638392E12, 2158.7833333333333], [1.67638362E12, 1406.8833333333334], [1.67638332E12, 1780.55], [1.6763832E12, 1975.4166666666667], [1.67638326E12, 1467.8833333333334], [1.67638386E12, 1922.9666666666667], [1.67638356E12, 2179.516666666667], [1.6763838E12, 1620.7333333333333], [1.67638344E12, 2353.016666666667], [1.6763835E12, 2220.0833333333335], [1.67638404E12, 924.6], [1.67638338E12, 2192.7833333333333], [1.67638374E12, 2040.7333333333333], [1.67638368E12, 1787.0666666666666]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.67638398E12, 11.9], [1.67638392E12, 7.116666666666666], [1.67638362E12, 14.816666666666666], [1.67638332E12, 1.9833333333333334], [1.6763832E12, 14.666666666666666], [1.67638326E12, 7.433333333333334], [1.67638386E12, 3.5833333333333335], [1.67638356E12, 23.816666666666666], [1.6763838E12, 5.033333333333333], [1.67638344E12, 7.616666666666666], [1.6763835E12, 9.266666666666667], [1.67638404E12, 4.85], [1.67638338E12, 3.6166666666666667], [1.67638374E12, 17.233333333333334], [1.67638368E12, 4.783333333333333]], "isOverall": false, "label": "504", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.67638404E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.9833333333333334, "minX": 1.6763832E12, "maxY": 2353.016666666667, "series": [{"data": [[1.67638398E12, 1582.2833333333333], [1.67638392E12, 2158.7833333333333], [1.67638362E12, 1406.8833333333334], [1.67638332E12, 1780.55], [1.6763832E12, 1975.4166666666667], [1.67638326E12, 1467.8833333333334], [1.67638386E12, 1922.9666666666667], [1.67638356E12, 2179.516666666667], [1.6763838E12, 1620.7333333333333], [1.67638344E12, 2353.016666666667], [1.6763835E12, 2220.0833333333335], [1.67638404E12, 924.6], [1.67638338E12, 2192.7833333333333], [1.67638374E12, 2040.7333333333333], [1.67638368E12, 1787.0666666666666]], "isOverall": false, "label": "HTTP Request-success", "isController": false}, {"data": [[1.67638398E12, 11.9], [1.67638392E12, 7.116666666666666], [1.67638362E12, 14.816666666666666], [1.67638332E12, 1.9833333333333334], [1.6763832E12, 14.666666666666666], [1.67638326E12, 7.433333333333334], [1.67638386E12, 3.5833333333333335], [1.67638356E12, 23.816666666666666], [1.6763838E12, 5.033333333333333], [1.67638344E12, 7.616666666666666], [1.6763835E12, 9.266666666666667], [1.67638404E12, 4.85], [1.67638338E12, 3.6166666666666667], [1.67638374E12, 17.233333333333334], [1.67638368E12, 4.783333333333333]], "isOverall": false, "label": "HTTP Request-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638404E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.9833333333333334, "minX": 1.6763832E12, "maxY": 2353.016666666667, "series": [{"data": [[1.67638398E12, 1582.2833333333333], [1.67638392E12, 2158.7833333333333], [1.67638362E12, 1406.8833333333334], [1.67638332E12, 1780.55], [1.6763832E12, 1975.4166666666667], [1.67638326E12, 1467.8833333333334], [1.67638386E12, 1922.9666666666667], [1.67638356E12, 2179.516666666667], [1.6763838E12, 1620.7333333333333], [1.67638344E12, 2353.016666666667], [1.6763835E12, 2220.0833333333335], [1.67638404E12, 924.6], [1.67638338E12, 2192.7833333333333], [1.67638374E12, 2040.7333333333333], [1.67638368E12, 1787.0666666666666]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.67638398E12, 11.9], [1.67638392E12, 7.116666666666666], [1.67638362E12, 14.816666666666666], [1.67638332E12, 1.9833333333333334], [1.6763832E12, 14.666666666666666], [1.67638326E12, 7.433333333333334], [1.67638386E12, 3.5833333333333335], [1.67638356E12, 23.816666666666666], [1.6763838E12, 5.033333333333333], [1.67638344E12, 7.616666666666666], [1.6763835E12, 9.266666666666667], [1.67638404E12, 4.85], [1.67638338E12, 3.6166666666666667], [1.67638374E12, 17.233333333333334], [1.67638368E12, 4.783333333333333]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.67638404E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 10800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

