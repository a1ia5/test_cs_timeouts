﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Threading;
using System.Diagnostics;
using System.Data.SqlClient;


namespace WebPages
{
    public class TestSqlTokenHandler : HttpTaskAsyncHandler
    {
        private static readonly TimeSpan _timeout = TimeSpan.FromMilliseconds(3000);
        private static readonly TimeSpan _acceptableLag = TimeSpan.FromMilliseconds(400);
        private static readonly Stopwatch _watch = Stopwatch.StartNew();
        
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var startTime = _watch.Elapsed;
            try
            {
                using (var con = new SqlConnection(WebConfig.CONNECTION_STRING))
                using (var cts = new CancellationTokenSource(_timeout))
                using (var cmd = new SqlCommand("WAITFOR DELAY '00:00:10'", con))
                {
                    await con.OpenAsync(cts.Token);
                    await cmd.ExecuteNonQueryAsync(cts.Token);
                }
            }
            catch (Exception) { }
            finally
            {
                if (_watch.Elapsed - startTime  > _timeout + _acceptableLag)
                {
                    context.Response.StatusCode = 504;
                }
                else
                {
                    context.Response.StatusCode = 200;
                }
            }
        }

        public override bool IsReusable => false;
    }
}
