﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using System.Net.Http;
using System.Threading;


namespace WebPages
{
    public class TestHttpTokenHandler : HttpTaskAsyncHandler
    {
        private static readonly TimeSpan _timeout = TimeSpan.FromMilliseconds(1000);
        private static readonly TimeSpan _acceptableLag = TimeSpan.FromMilliseconds(400);
        private static readonly Stopwatch _watch = Stopwatch.StartNew();
        
        private static readonly HttpClient _httpClient = new HttpClient
        {
            BaseAddress = WebConfig.SLOW_HTTP_SERVER,
        };
        
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var startTime = _watch.Elapsed;
            try
            {
                using (var cts = new CancellationTokenSource(_timeout))
                {
                    var response = await _httpClient.GetAsync("/", cts.Token);
                    await response.Content.ReadAsStreamAsync();
                }
            }
            catch (Exception) { }
            finally
            {
                if (_watch.Elapsed - startTime  > _timeout + _acceptableLag)
                {
                    context.Response.StatusCode = 504;
                }
                else
                {
                    context.Response.StatusCode = 200;
                }
            }
        }

        public override bool IsReusable => false;
    }
}
