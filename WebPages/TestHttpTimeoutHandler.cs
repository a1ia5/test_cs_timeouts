﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using System.Net.Http;


namespace WebPages
{
    public class TestHttpTimeoutHandler : HttpTaskAsyncHandler
    {
        private static readonly TimeSpan _timeout = TimeSpan.FromMilliseconds(1000);
        private static readonly TimeSpan _acceptableLag = TimeSpan.FromMilliseconds(400);
        private static readonly Stopwatch _watch = Stopwatch.StartNew();
        
        private static readonly HttpClient _httpClient = new HttpClient
        {
            Timeout = _timeout,
            BaseAddress = WebConfig.SLOW_HTTP_SERVER
        };
        
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var startTime = _watch.Elapsed;
            try
            {
                var response = await _httpClient.GetAsync("/");
                await response.Content.ReadAsStreamAsync();
            }
            catch (Exception) { }
            finally
            {
                if (_watch.Elapsed - startTime  > _timeout + _acceptableLag)
                {
                    context.Response.StatusCode = 504;
                }
                else
                {
                    context.Response.StatusCode = 200;
                }
            }
        }

        public override bool IsReusable => false;
    }
}
