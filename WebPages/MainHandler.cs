﻿using System;
using System.Web;


namespace WebPages
{
    public class MainHandler: IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string currentDT = DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff");
            context.Response.Write($"OK: {currentDT}");
            context.Response.StatusCode = 200;
        }
        public bool IsReusable => false;
    }
}
