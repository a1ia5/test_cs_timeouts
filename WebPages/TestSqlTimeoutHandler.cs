﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;
using System.Threading;
using System.Data.SqlClient;


namespace WebPages
{
    public class TestSqlTimeoutHandler : HttpTaskAsyncHandler
    {
        private const int _commandTimeout = 4;
        private static readonly TimeSpan _connectionTimeout = TimeSpan.FromMilliseconds(3000);
        private static readonly TimeSpan _acceptableLag = TimeSpan.FromMilliseconds(400);
        private static readonly Stopwatch _watch = Stopwatch.StartNew();

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var connectionStartTime = _watch.Elapsed;
            var commandStartTime = TimeSpan.Zero;
            try
            {
                using (var con = new SqlConnection(WebConfig.CONNECTION_STRING))
                using (var cts = new CancellationTokenSource(_connectionTimeout))
                using (var cmd = new SqlCommand("WAITFOR DELAY '00:00:10'", con))
                {
                    await con.OpenAsync(cts.Token);
                    cmd.CommandTimeout = _commandTimeout;
                    commandStartTime = _watch.Elapsed;
                    await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception) { }
            finally
            {
                var stoptime = _watch.Elapsed;
                if (stoptime - connectionStartTime  > _connectionTimeout + TimeSpan.FromSeconds(_commandTimeout) + _acceptableLag ||
                    commandStartTime != TimeSpan.Zero && stoptime - commandStartTime > TimeSpan.FromSeconds(_commandTimeout) + _acceptableLag)
                {
                    context.Response.StatusCode = 504;
                }
                else
                {
                    context.Response.StatusCode = 200;
                }
            }
        }

        public override bool IsReusable => false;
    }
}
